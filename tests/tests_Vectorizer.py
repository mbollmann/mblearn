from hypothesis import given, strategies as st
from mblearn.serialization import dump, load
from mblearn.utils import Vectorizer

def test_empty_length():
    v = Vectorizer(unk_symbol=None, pad_symbol=None, padding=0)
    assert len(v) == 0
    v = Vectorizer(unk_symbol="<UNK>", pad_symbol=None, padding=0)
    assert len(v) == 1
    v = Vectorizer(unk_symbol=None, pad_symbol="<PAD>", padding=10)
    assert len(v) == 1
    v = Vectorizer(unk_symbol="<UNK>", pad_symbol="<PAD>", padding=10)
    assert len(v) == 2

@given(st.sets(elements=st.text()))
def test_text_items(testset):
    v = Vectorizer(unk_symbol=None, pad_symbol=None, padding=0)
    v.update(testset)
    assert len(testset) == len(v)
    for elem in testset:
        assert elem in v
    assert len(testset) == len(v.map_to_index(testset))

@given(st.sets(elements=st.text()))
def test_ctor_vs_update(testset):
    v1 = Vectorizer()
    v1.update(testset)
    v2 = Vectorizer(items=testset)
    assert v1 == v2

@given(items=st.sets(elements=st.text(), min_size=1),
       length=st.integers(min_value=1, max_value=1000),
       choice=st.choices())
def test_bidirectional_mapping(items, length, choice):
    v = Vectorizer(items=items, unk_symbol=None)
    items = tuple(items)
    sequence = [choice(items) for _ in range(length)]
    assert v.map_to_items(v.map_to_index(sequence)) == sequence

@given(st.sets(elements=st.characters(blacklist_categories=["Cc", "Cf", "Co", "Cs"])))
def test_serialization(testset):
    v = Vectorizer(unk_symbol=None, pad_symbol=None, padding=0)
    v.update(testset)
    assert v == load(dump(v, compress=False))
    assert v == load(dump(v, compress=True))

def test_pad_symbol():
    """Tests that padding symbol always has index zero. (Relevant for Keras'
    embedding layer, which can mask zeros.)"""
    v = Vectorizer(unk_symbol=None, pad_symbol="<PAD>", padding=5)
    assert v['<PAD>'] == 0
    v = Vectorizer(unk_symbol="<UNK>", pad_symbol="<PAD>", padding=5)
    assert v['<PAD>'] == 0
    v = Vectorizer(unk_symbol="<UNK>", pad_symbol="<PAD>", padding=5)
    v.update(("Foo", "bar", "baz", "banana"))
    assert v['<PAD>'] == 0

@given(st.integers(min_value=4, max_value=1000))
def test_padding(length):
    items = ('foo', 'bar')
    sequence = ('foo', 'foo', 'baz', 'bar')
    v1 = Vectorizer(items=items, unk_symbol="<UNK>")
    assert len(v1.map_to_index(sequence)) == len(sequence)
    padded_indices = v1.map_to_index(sequence, padding=length)
    assert len(padded_indices) == length
    assert all(x == 0 for x in padded_indices[len(sequence):])
    v2 = Vectorizer(items=items, unk_symbol="<UNK>", padding=length)
    assert len(v2.map_to_index(sequence)) == length

@given(st.integers(min_value=1, max_value=11))
def test_cutoff(length):
    items = ('foo', 'bar')
    sequence = ('foo', 'foo', 'baz', 'bar') * 3
    v1 = Vectorizer(items=items, unk_symbol="<UNK>")
    assert len(v1.map_to_index(sequence)) == len(sequence)
    padded_indices = v1.map_to_index(sequence, padding=length)
    assert len(padded_indices) == length
    v2 = Vectorizer(items=items, unk_symbol="<UNK>", padding=length)
    assert len(v2.map_to_index(sequence)) == length
