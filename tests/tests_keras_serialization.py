from keras.models import Sequential
from keras.layers.recurrent import LSTM

from mblearn.serialization import dump, load, keras

def test_lstm():
    model = Sequential([
        LSTM(20, input_shape=(2, 2), return_sequences=True),
        LSTM(20)
    ])
    serialization = dump(model)
    clone = load(serialization)
    assert len(clone.layers) == len(model.layers)
    for m, c in zip(model.layers, clone.layers):
        assert type(m) == type(c)
        assert m.input_shape == c.input_shape
        assert m.output_shape == c.output_shape
