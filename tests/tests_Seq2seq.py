import pytest
import numpy as np
from keras.layers import Activation, Dense, Embedding, LSTM, Masking
from keras.layers.wrappers import TimeDistributed
from mblearn.models.seq2seq import Seq2seq
from mblearn.models.decoders import GreedySequentialDecoder
#from mblearn.serialization import dump, load, keras

x = [[[0,0,0,1,0,0,0,0,0,0],
      [0,0,0,0,1,0,0,0,0,0],
      [0,0,0,0,0,1,0,0,0,0]],
     [[0,0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,1,0,0,0,0],
      [0,0,0,0,1,0,0,0,0,0]],
     [[0,0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,1,0,0,0],
      [0,0,0,0,0,1,0,0,0,0]],
     [[0,0,0,0,0,1,0,0,0,0],
      [0,0,0,1,0,0,0,0,0,0],
      [0,0,0,1,0,0,0,0,0,0]],
     [[0,0,0,0,1,0,0,0,0,0],
      [0,0,0,0,0,0,1,0,0,0],
      [0,0,0,1,0,0,0,0,0,0]]]
x_embed = np.argmax(x, axis=2)

y = [[[0,0,0,0,0,0,0,0,0,1],
      [0,1,0,0,0,0,0,0,0,0],
      [0,0,1,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,1,0]],
     [[0,0,1,0,0,0,0,0,0,0],
      [0,1,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,1,0],
      [0,0,0,0,0,0,0,0,0,0]],
     [[0,0,1,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,1,0],
      [0,0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0,0]],
     [[0,0,1,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0,1],
      [0,0,0,0,0,0,0,0,0,1],
      [0,0,0,0,0,0,0,0,1,0]],
     [[0,1,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0,1],
      [0,0,0,0,0,0,0,0,1,0],
      [0,0,0,0,0,0,0,0,0,0]]]
y_classy = np.argmax(y, axis=2)
y_sparse = np.expand_dims(y_classy, -1)

mask_symbol  = [0,0,0,0,0,0,0,0,0,0]
start_symbol = [0,0,0,0,0,0,0,1,0,0]
end_symbol   = [0,0,0,0,0,0,0,0,1,0]

mask_symbol_sparse = [0]
start_symbol_embed = np.argmax(start_symbol)
end_symbol_sparse  = [np.argmax(end_symbol)]

verbosity = 0

def test_lstm_model():
    model = Seq2seq((3, 4), start_symbol, end_symbol, mask_symbol)
    model.add(Masking(mask_value=0., input_shape=(7, 10)))
    model.add(LSTM(16, return_sequences=True))
    model.add(TimeDistributed(Dense(10)))
    model.add(Activation('softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='sgd')
    history = model.fit(x, y, nb_epoch=150, verbose=verbosity)
    loss_0 = history.history['loss'][0]
    loss_e = history.history['loss'][-1]
    assert loss_0 > loss_e, 'Loss at the end is lower than at the start'
    assert (loss_0 - loss_e) > 0.2, 'Loss at the end is considerably lower'
    assert loss_e < 2.2, 'Loss at the end is below 2.2'
    model.decoder = GreedySequentialDecoder()
    preds = model.predict_classes(np.asarray(x), verbose=verbosity)
    assert preds.shape == y_classy.shape

def test_lstm_model_with_embedding():
    model = Seq2seq((3, 4), start_symbol_embed, end_symbol_sparse, mask_symbol_sparse)
    model.add(Embedding(10, 5, input_length=7, mask_zero=True))
    model.add(LSTM(5, return_sequences=True))
    model.add(TimeDistributed(Dense(10)))
    model.add(Activation('softmax'))
    model.compile(loss='sparse_categorical_crossentropy', optimizer='sgd')
    history = model.fit(x_embed, y_sparse, nb_epoch=150, verbose=verbosity)
    loss_0 = history.history['loss'][0]
    loss_e = history.history['loss'][-1]
    assert loss_0 > loss_e, 'Loss at the end is lower than at the start'
    assert (loss_0 - loss_e) > 0.2, 'Loss at the end is considerably lower'
    assert loss_e < 2.4, 'Loss at the end is below 2.4'
    model.decoder = GreedySequentialDecoder()
    preds = model.predict_classes(np.asarray(x_embed), verbose=verbosity)
    assert preds.shape == y_classy.shape


if __name__ == '__main__':
    verbosity = 2
    print("Testing LSTM model with onehot vectors...")
    test_lstm_model()
    print("Testing LSTM model with embeddings...")
    test_lstm_model_with_embedding()
