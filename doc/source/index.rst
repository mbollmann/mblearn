.. mblearn documentation master file, created by
   sphinx-quickstart on Wed May 18 12:38:48 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mblearn's documentation!
===================================

Contents:

.. toctree::
   :glob:
   :maxdepth: 4

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

