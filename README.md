# mblearn

mblearn is a Python 3.x package for all my personal machine learning (and
particularly deep learning) needs.  It builds on existing packages such as
[Keras][] and extends them with additional functionality that I happen to need
in one or more of my projects.

Useful features will be documented here ... later.

## Example usage for spelling normalization

All scripts provide a `-h`/`--help` switch with detailed options.

### Encoder/decoder models

In the `scripts/` directory, run

```
./norm_advanced_seq2seq.py train B1.train -e 30 -r 0.3 -x my_model --eval B1.test
```

to train an encoder/decoder model on `B1.train` for 30 epochs with dropout of
0.3, evaluate on `B1.test` after training, and save the model to `my_model.*`
files.

`B1.train`/`B1.test` should be files containing one word pair per line,
separated by a tab.

To train a model with only one recurrent layer, a bidirectional encoder, and attention mechanism, that additionally evaluates on `B1.test` after each epoch, you should use:

```
./norm_advanced_seq2seq.py train B1.train -d 1 --bidir --att -x my_model --validate B1.test
```

Evaluate a saved model using greedy decoding or beam search:

```
./norm_advanced_seq2seq.py eval B1.test -x my_model -d greedy
./norm_advanced_seq2seq.py eval B1.test -x my_model -d beam --width 5
```

Calculate attention weights and output them as CSV or as graphical plots:

```
./norm_advanced_seq2seq.py pratt B1.test -x my_model --csv my_weights.csv
./norm_advanced_seq2seq.py pratt B1.test -x my_model --plot my_plots
```

### Multi-task learning

Perform multi-task learning using exactly one main and one auxiliary task:

```
./norm_mtl_seq2seq.py train B1.train -x my_model --aux aux.train --share-embeddings --share-encoder --share-decoder
```

The saved model can be evaluated/analysed with `norm_advanced_seq2seq.py` as
above.


## Contact

Marcel Bollmann <marcel@bollmann.me>


[Keras]: http://keras.io/
