#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import numpy as np
import random
import sys

from norm_common import get_data_from_file, train_model, \
     save_model, load_model, save_history, \
     add_common_train_params, add_common_eval_params

from norm_advanced_seq2seq import Wacc, append_end_symbol, \
     convert_to_xy, count_correct, evaluate_model

def build_model(args, enc_vec, dec_vec, enc_len, dec_len):
    from keras.layers import Input, Embedding, Dense, TimeDistributed, Activation
    from keras.optimizers import Adam
    from mblearn.models.seq2seq import Seq2seqModel
    from mblearn.models.wrappers import CategoricalModelWrapper

    from encdec import build_encoder_layers, build_decoder_layers, encode, decode

    ### inputs
    main_enc_input = Input(shape=(enc_len,), dtype='int32', name='main_enc_input')
    aux_enc_input  = Input(shape=(enc_len,), dtype='int32', name='aux_enc_input')
    main_dec_input = Input(shape=(dec_len,), dtype='int32', name='main_dec_input')
    aux_dec_input  = Input(shape=(dec_len,), dtype='int32', name='aux_dec_input')

    if args.share_embeddings and args.share_encoder:
        embed = Embedding(len(enc_vec), args.embedding, mask_zero=True)
        main_enc_embed = embed(main_enc_input)
        aux_enc_embed  = embed(aux_enc_input)
    else:
        main_enc_embed = Embedding(len(enc_vec), args.embedding, mask_zero=True)(main_enc_input)
        aux_enc_embed  = Embedding(len(enc_vec), args.embedding, mask_zero=True)(aux_enc_input)

    if args.share_embeddings and args.share_decoder:
        embed = Embedding(len(dec_vec), args.embedding, mask_zero=True)
        main_dec_embed = embed(main_dec_input)
        aux_dec_embed  = embed(aux_dec_input)
    else:
        main_dec_embed = Embedding(len(dec_vec), args.embedding, mask_zero=True)(main_dec_input)
        aux_dec_embed  = Embedding(len(dec_vec), args.embedding, mask_zero=True)(aux_dec_input)

    ### build encoder
    if args.share_encoder:
        encoder = build_encoder_layers(args.hidden, bidirectional=args.bidir,
                                       depth=args.depth, dropout_W=args.dropout,
                                       return_sequences=args.att)
        main_encoder, aux_encoder = encoder, encoder
    else:
        main_encoder = build_encoder_layers(args.hidden, bidirectional=args.bidir,
                                            depth=args.depth, dropout_W=args.dropout,
                                            return_sequences=args.att)
        aux_encoder  = build_encoder_layers(args.hidden, bidirectional=args.bidir,
                                            depth=args.depth, dropout_W=args.dropout,
                                            return_sequences=args.att)

    ### build decoder
    with_attention = 1 if args.att else 0
    if args.share_decoder:
        decoder = build_decoder_layers(args.hidden, depth=args.depth,
                                       dropout_W=args.dropout,
                                       with_attention=with_attention)
        main_decoder, aux_decoder = decoder, decoder
    else:
        main_decoder = build_decoder_layers(args.hidden, depth=args.depth,
                                            dropout_W=args.dropout,
                                            with_attention=with_attention)
        aux_decoder  = build_decoder_layers(args.hidden, depth=args.depth,
                                            dropout_W=args.dropout,
                                            with_attention=with_attention)

    ### combine & predict
    main_enc_layer, main_enc_hidden = encode(main_enc_embed, main_encoder)
    aux_enc_layer, aux_enc_hidden   = encode(aux_enc_embed, aux_encoder)
    main_dec_layer = decode(main_dec_embed, main_decoder,
                            hidden_states=(main_enc_hidden if not args.att else None),
                            attend=([main_enc_layer] if args.att else None))
    aux_dec_layer  = decode(aux_dec_embed, aux_decoder,
                            hidden_states=(aux_enc_hidden if not args.att else None),
                            attend=([main_enc_layer] if args.att else None))

    if args.share_softmax:
        dense = TimeDistributed(Dense(len(dec_vec)))
        main_dec_output = dense(main_dec_layer)
        aux_dec_output  = dense(aux_dec_layer)
    else:
        main_dec_output = TimeDistributed(Dense(len(dec_vec)))(main_dec_layer)
        aux_dec_output  = TimeDistributed(Dense(len(dec_vec)))(aux_dec_layer)

    main_dec_output = Activation('softmax', name='main_dec_output')(main_dec_output)
    aux_dec_output  = Activation('softmax', name='aux_dec_output')(aux_dec_output)

    ### build model
    symbols = dict(
        start_symbol=dec_vec['<START>'],
        end_symbol=[dec_vec['<END>']],
        mask_symbol=[0]
    )
    train_model = Seq2seqModel(
        input=[main_enc_input, aux_enc_input, main_dec_input, aux_dec_input],
        output=[main_dec_output, aux_dec_output],
        decoder_input=[2,3], decoder_output=[0,1],
        **symbols
    )
    eval_model = Seq2seqModel(
        input=[main_enc_input, main_dec_input],
        output=[main_dec_output],
        decoder_input=1, decoder_output=0,
        **symbols
    )
    for model in (train_model, eval_model):
        model.compile(
            loss="sparse_categorical_crossentropy",
            optimizer=Adam(lr=0.003),
            metrics=[Wacc]
        )
    train_wrapper = CategoricalModelWrapper(
        model=train_model,
        input_vectorizer=[enc_vec, enc_vec, None, None],
        output_vectorizer=[dec_vec, dec_vec]
    )
    eval_wrapper = CategoricalModelWrapper(
        model=eval_model,
        input_vectorizer=[enc_vec, None],
        output_vectorizer=[dec_vec]
    )
    return (train_wrapper, eval_wrapper)

def main_aux_shuffler(main_X, main_y, aux_X, aux_y, fn, batch_size):
    def cycle_random(data):
        indices = list(range(len(data)))
        while True:
            random.shuffle(indices)
            for idx in indices:
                yield data[idx]

    count = 0
    main_len = len(main_X)
    main_gen = cycle_random(list(zip(main_X, main_y)))
    aux_gen  = cycle_random(list(zip(aux_X, aux_y)))
    while True:
        current_batch_len = min(batch_size, main_len - count)
        main_bx, main_by = zip(*[next(main_gen) for _ in range(current_batch_len)])
        aux_bx, aux_by   = zip(*[next(aux_gen) for _ in range(current_batch_len)])
        batch_inputs  = [main_bx, aux_bx, None, None]
        batch_outputs = [main_by, aux_by]
        count = (count + current_batch_len) % main_len
        if args.sw < 1.0:
            yield (batch_inputs, batch_outputs,
                   [np.asarray([1.0] * current_batch_len),
                    np.asarray([float(args.sw)] * current_batch_len)])
        else:
            yield (batch_inputs, batch_outputs)

def train(args):
    from mblearn.utils import Vectorizer
    from mblearn.models.decoders import GreedyDecoder

    main_data = get_data_from_file(args.infile, cutoff=(args.max_input, args.max_output))
    aux_data = get_data_from_file(args.aux, cutoff=(args.max_input, args.max_output))
    enc_vectorizer = Vectorizer(unk_symbol="<UNK>")
    enc_vectorizer.update(main_data.characters(0))
    enc_vectorizer.update(aux_data.characters(0))
    dec_vectorizer = Vectorizer(unk_symbol="<UNK>")
    dec_vectorizer.update(main_data.characters(1))
    dec_vectorizer.update(aux_data.characters(1))
    dec_vectorizer.update(('<START>', '<END>'))

    (train_wrapper, eval_wrapper) = build_model(
        args, enc_vectorizer, dec_vectorizer,
        args.max_input, args.max_output
    )
    eval_wrapper.model.decoder = GreedyDecoder()
    main_X, main_y = convert_to_xy(main_data)
    aux_X, aux_y = convert_to_xy(aux_data)
    params = {}
    if args.validation_split > 0.0:
        raise NotImplementedError("cannot use validation_split with MTL")
    elif args.validate:
        val_X, val_y = convert_to_xy(get_data_from_file(args.validate))
        params = {'validation_data': ([val_X, val_X, None, None], [val_y, val_y])}
    try:
        history = train_wrapper.fit_generator(
            main_aux_shuffler(
                main_X, main_y, aux_X, aux_y,
                train_wrapper.model._make_decoder_input,
                args.batch),
            len(main_X), # samples_per_epoch
            args.epochs, # nb_epochs
            verbose=args.verbose,
            **params
        )
    except KeyboardInterrupt:
        history = None

    if args.prefix:
        save_model(eval_wrapper, args.prefix)
        save_history(history, args.prefix)

    if args.eval is not None:
        for evalfile in args.eval:
            evaluate_model(eval_wrapper, args, infile=evalfile)


if __name__ == "__main__":
    description = "Normalization using neural network architectures."
    epilog = ""
    parser = argparse.ArgumentParser(
        description=description,
        epilog=epilog,
        fromfile_prefix_chars='@'
        )
    subparsers = parser.add_subparsers(title="available commands")

    p_train = subparsers.add_parser('train')
    p_train.set_defaults(func=train,
                         help="Train a model")
    add_common_train_params(p_train)
    # p_train.add_argument('-u', '--recurrent-unit',
    #                      dest="recurrent_unit",
    #                      choices=('LSTM','GRU'),
    #                      default='LSTM',
    #                      help='Recurrent unit to use (default: %(default)s)')
    p_train.add_argument('--aux',
                         metavar='INPUT',
                         type=argparse.FileType('r', encoding="UTF-8"),
                         required=True,
                         help='Auxiliary task to train on')
    p_train.add_argument('--att',
                         action='store_true',
                         default=False,
                         help='Use attentional decoders')
    p_train.add_argument('--bidir',
                         action='store_true',
                         default=False,
                         help='Use bi-directional encoders')
    p_train.add_argument('--share-embeddings',
                         action='store_true',
                         default=False,
                         help='Share input/output embeddings for main/auxiliary task')
    p_train.add_argument('--share-decoder',
                         action='store_true',
                         default=False,
                         help='Share decoder for main/auxiliary task')
    p_train.add_argument('--share-encoder',
                         action='store_true',
                         default=False,
                         help='Share encoder for main/auxiliary task')
    p_train.add_argument('--share-softmax',
                         action='store_true',
                         default=False,
                         help='Share final prediction layer for main/auxiliary task')
    p_train.add_argument('--sw',
                         type=float,
                         default=1.0,
                         help='Sample weight for the auxiliary task (default: %(default)f)')

    args = parser.parse_args()
    if args.seed > 0:
        np.random.seed(args.seed)
    args.func(args)
