
from camel import Camel
from pprint import pprint
from scripts.norm_common import load_model
from mblearn.layers import *
from mblearn.models.decoders import *
from mblearn.models.seq2seq import *
from mblearn.models.wrappers import *
from mblearn.utils import Vectorizer
from keras import backend as K
import argparse
import theano
import theano.tensor as T
import sys

camel = Camel([])

def get_layer(model, names):
    for lname in names:
        layer = model.get_layer(name=lname)
        if layer is not None:
            break
    else:
        raise Exception("No layer found: " + ', '.join(names))
    return layer

def main(prefix, args):
    wrapper = load_model(prefix)
    model = wrapper.model
    model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer="adam",
    )
    model.decoder = GreedyDecoder()

    # extract some model inputs/outputs
    enc_in = get_layer(model, ("encoder_input", "main_enc_input")).input
    dec_in = get_layer(model, ("decoder_input", "main_dec_input")).input
    dec_out = get_layer(model, ("decoder_output", "main_dec_output")).output

    enc_embed = model.get_layer(name="embedding_1")
    dec_embed = model.get_layer(name="embedding_2")
    td_out = model.get_layer(name="timedistributed_1").output
    # this is highly model-specific...
    enc_lstm_in = get_layer(model, ("hiddenstatelstm_1", "bidirectionalhidden_1")).input
    dec_lstm_in = get_layer(model, ("hiddenstatelstm_2", "attentionlstm_1")).input[0]

    # this only makes sense if `y` has the actual predictions for `x` -- could run a
    # normal decoding step to generate `y`, but for this toy example, I've made sure
    # these are the actual predictions for this model
    x, y = [], []
    for line in args.words:
        line = line.strip().split("\t")
        x.append(line[0])
        y.append(line[1])
    #x = ["anshelme", "vnd"]
    #y = ["anselm", "und"]
    num_timesteps = 14

    # do what wrapper/seq2seq-model usually does for us
    # -- note: if len(y) == 1, this crashes for some unfathomable reason, so I've
    # used two example word pairs...
    (x_v, _), y_out = wrapper.transform_inputs([x, None], [y])
    y_v = model._make_decoder_input(y_out)

    max_class = T.argmax(td_out, axis=-1)

    max_out = T.max(td_out, axis=-1)
    #max_sum = max_out.sum()

    saliency_per_timestep = []
    for i in range(num_timesteps):
        max_sum = max_out[:, i].sum()
        saliency_enc = theano.grad(max_sum, wrt=enc_lstm_in)
        saliency_dec = theano.grad(max_sum, wrt=dec_lstm_in)
        saliency = T.concatenate([saliency_enc, saliency_dec], axis=1)
        #saliency = T.jacobian(max_out.flatten(), enc_lstm_in)
        saliency = saliency.norm(2, axis=-1)
        #saliency = saliency.sum(axis=-1, keepdims=True)
        saliency_per_timestep.append(saliency)

    saliency = T.concatenate(saliency_per_timestep, axis=1).reshape((-1, num_timesteps, num_timesteps * 2))

    fx = theano.function([enc_in, dec_in, K.learning_phase()],
                         [saliency, max_class],
                         #[max_class],
                         allow_input_downcast=True)
    grad = fx(x_v, y_v, False)

    #pred_class = grad[1][:, 0]
    #pprint(pred_class)
    #try:
    #    pprint(grad[0][:, pred_class])
    #except Exception as e:
    #    pprint(e)

    gradients = []
    pprint(grad[0].shape)
    for i, grads in enumerate(grad[0]):
        gradients.append({
            'x': x[i],
            'y': y[i],
            'grads': grads.tolist()
        })

    args.save.write(camel.dump(gradients))


if __name__ == "__main__":
    description = "Compute saliency of timesteps."
    epilog = ""
    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument('model',
                        metavar='PREFIX',
                        type=str,
                        help='Prefix (or .yaml file) for loading the model')
    parser.add_argument('-s', '--save',
                        metavar='OUTPUT',
                        type=argparse.FileType('w', encoding="UTF-8"),
                        default=sys.stdout,
                        help='Output gradients to this file (default: STDOUT)')
    parser.add_argument('-w', '--words',
                        metavar='words',
                        type=argparse.FileType('r', encoding="UTF-8"),
                        required=True,
                        help='Read word pairs from this file')

    args = parser.parse_args()

    prefix = args.model
    if prefix.endswith(".yaml"):
        prefix = prefix[:-5]

    main(prefix, args)
