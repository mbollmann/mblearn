#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import sys
import pycrfsuite

## I can't get crfsuite to work with utf-8 characters and python3. ugly hack:
## convert data to stringified unicode code points...
def unicode_to_ascii(word):
    return '/'.join(str(ord(c)) for c in word)

def ascii_to_unicode(ascii):
    return ''.join([chr(int(c)) for c in ascii.split("/")])

def featurize(word, pos):
    l = len(word)
    features = [
        'bias',
        'char={}'.format(word[pos][0]),
        'char_n-1={}'.format(word[pos-1][0] if pos > 0 else '__START-1__'),
        'char_n-2={}'.format(word[pos-2][0] if pos > 1 else '__START-2__'),
        'char_n+1={}'.format(word[pos+1][0] if pos < l-1 else '__END+1__'),
        'char_n+2={}'.format(word[pos+2][0] if pos < l-2 else '__END+2__')
    ]
    return features

def parse_data(blob):
    from mblearn.data import AnnotatedTextData
    data = AnnotatedTextData(blob, cols=2)
    X, y = [], []
    # assume data is one character per line, so iterate over "sentences" to get
    # the words
    for sent in data.sentences("all"):
        sent = [(unicode_to_ascii(e[0]), unicode_to_ascii(e[1])) for e in sent]
        X.append([featurize(sent, i) for i in range(len(sent))])
        y.append([item[1] for item in sent])
    return (X, y)

def train(args):
    (X_train, y_train) = parse_data(args.infile.read())
    if args.train_after > 0:
        (X_train, y_train) = (X_train[args.train_after:], y_train[args.train_after:])
    trainer = pycrfsuite.Trainer(algorithm=args.algorithm, verbose=(args.verbose > 0))
    for X, y in zip(X_train, y_train):
        trainer.append(X, y)
    params = {
        #'c1': 1.0,
        #'c2': 1e-3,
        'feature.possible_states': True,
        'feature.possible_transitions': True
    }
    if args.iters > 0:
        params['max_iterations'] = args.iters
    trainer.set_params(params)
    trainer.train('{}.crfsuite'.format(args.prefix))

def evaluate(args):
    (X_test, y_test) = parse_data(args.infile.read())
    if args.eval_before > 0:
        (X_test, y_test) = (X_test[:args.eval_before], y_test[:args.eval_before])
    tagger = pycrfsuite.Tagger()
    tagger.open('{}.crfsuite'.format(args.prefix))

    predictions = [tagger.tag(X) for X in X_test]
    correct = [p == y for p, y in zip(predictions, y_test)]
    print("Word-level accuracy: {0}/{1} = {2:.4f}".format(
        sum(correct), len(correct), sum(correct) / len(correct)
    ))


if __name__ == "__main__":
    description = "Normalization using conditional random fields."
    epilog = ""
    parser = argparse.ArgumentParser(
        description=description,
        epilog=epilog,
        fromfile_prefix_chars='@'
        )
    subparsers = parser.add_subparsers(title="available commands")

    sparser = subparsers.add_parser('train')
    sparser.set_defaults(func=train,
                         help="Train a model")
    sparser.add_argument('infile',
                         metavar='INPUT',
                         type=argparse.FileType('r', encoding="UTF-8"),
                         default=sys.stdin,
                         help='File to train on (default: STDIN)')
    sparser.add_argument('-a', '--algorithm',
                         choices=('lbfgs','l2sgd','ap','pa','arow'),
                         default='lbfgs',
                         help='Training algorithm (default: %(default)s)')
    sparser.add_argument('-i', '--iters',
                         metavar='N',
                         type=int,
                         default=-1,
                         help='Maximum number of iterations (default: None)')
    sparser.add_argument('-x', '--prefix',
                         metavar='STR',
                         type=str,
                         required=True,
                         help='Prefix for saving the model (required)')
    sparser.add_argument('-V', '--verbose',
                         choices=(0,1),
                         type=int,
                         default=1,
                         help='Output verbosity')
    sparser.add_argument('--train-after',
                         metavar='INDEX',
                         type=int,
                         default=0,
                         help='Only train on samples starting from INDEX')

    sparser = subparsers.add_parser('eval')
    sparser.set_defaults(func=evaluate,
                         help="Evaluate a model")
    sparser.add_argument('infile',
                         metavar='INPUT',
                         type=argparse.FileType('r', encoding="UTF-8"),
                         default=sys.stdin,
                         help='File to train on (default: STDIN)')
    sparser.add_argument('-x', '--prefix',
                         metavar='STR',
                         type=str,
                         required=True,
                         help='Prefix for loading the model (required)')
    sparser.add_argument('--eval-before',
                         metavar='INDEX',
                         type=int,
                         default=0,
                         help='Only evaluate on samples before INDEX')

    args = parser.parse_args()
    args.func(args)
