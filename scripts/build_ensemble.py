#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import numpy as np
import random
import sys

from norm_common import save_model

class ModelMismatch(Exception):
    pass

def load_model(prefix):
    from mblearn.serialization import load
    print("Loading model '{}'...".format(prefix), file=sys.stderr)
    with open(prefix + '.yaml', 'r') as f:
        model = load(f.read())
    model.model.load_weights(prefix + '.h5')
    return model

def load_and_check_models(prefixes):
    from mblearn.models.seq2seq import Seq2seqModel
    wrappers = [load_model(prf) for prf in prefixes]
    for attr in ('input_names', 'output_names',
                 'input_shape', 'output_shape',
                 'input_padding'):
        values = [getattr(w, attr) for w in wrappers]
        if not all(values[0] == value for value in values):
            raise ModelMismatch("Models do not appear to be equivalent: '{}' differs".format(attr))
    for attr in ('input_layers', 'output_layers'):
        values = [len(getattr(w.model, attr)) for w in wrappers]
        if not all(values[0] == value for value in values):
            raise ModelMismatch("Models do not appear to be equivalent: 'model.{}' differs in length".format(attr))

    if not all(len(w.outputs) == 1 for w in wrappers):
        raise ModelMismatch("Models have more than one output layer -- currently unsupported".format(attr))

    for w in wrappers:
        assert isinstance(w.model, Seq2seqModel), "Model is not a seq2seq model"
    for attr in ('decoder_input', 'decoder_output',
                 'start_symbol', 'end_symbol', 'mask_symbol'):
        values = [getattr(w.model, attr) for w in wrappers]
        if not all(values[0] == value for value in values):
            raise ModelMismatch("Seq2seqModels do not appear to be equivalent: '{}' differs".format(attr))

    assert all(isinstance(w.input_vectorizer, list) for w in wrappers)
    assert all(isinstance(w.output_vectorizer, list) for w in wrappers)
    input_vecs = [w.input_vectorizer for w in wrappers]
    input_vecs = list(zip(*input_vecs))
    output_vecs = [w.output_vectorizer for w in wrappers]
    output_vecs = list(zip(*output_vecs))

    for vecs in input_vecs + output_vecs:
        if not all(vecs[0] == v for v in vecs):
            raise ModelMismatch("Vectorizers do not match")

    return wrappers

def build_ensemble(args):
    from keras.layers import Input
    from mblearn.layers import HiddenStateLSTM, MaskedMerge, merge, TimeCutoff
    from mblearn.models.seq2seq import Seq2seq, Seq2seqModel
    from mblearn.models.decoders import GreedyDecoder, BeamSearchDecoder
    from mblearn.models.filters import DictionaryFilter
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer

    # Load all models and check if they can be merged
    try:
        wrappers = load_and_check_models(args.prefixes)
    except ModelMismatch as e:
        print(str(err), file=sys.stderr)
        exit(1)

    # Hook them up
    print("Hooking up the models...", file=sys.stderr)
    model = wrappers[0].model
    inputs = [Input(shape=wrappers[0].input_shape[i][1:], dtype=in_.dtype, name=in_.name) for i, in_ in enumerate(wrappers[0].inputs)]
    models = []
    for i, w in enumerate(wrappers):
        sys.stderr.write("{:d} ".format(i+1))
        sys.stderr.flush()
        # model names need to be unique
        w.model.name = '{:s}_sub{:d}'.format(w.model.name, i)
        models.append(w.model(inputs))
    merged = merge(models, mode='ave')
    sys.stderr.write("\n")

    merged_model = Seq2seqModel(
        input=inputs, output=[merged],
        decoder_input=model.decoder_input,
        decoder_output=model.decoder_output,
        start_symbol=model.start_symbol,
        end_symbol=model.end_symbol,
        mask_symbol=model.mask_symbol
    )
    merged_wrapper = CategoricalModelWrapper(
        model=merged_model,
        input_vectorizer=wrappers[0].input_vectorizer,
        input_padding=wrappers[0].input_padding,
        output_vectorizer=wrappers[0].output_vectorizer,
        embedding_at=wrappers[0].embedding_inputs
    )

    save_model(merged_wrapper, args.out, skip_weights=False)


if __name__ == "__main__":
    description = "Build an ensemble out of individually trained models."
    epilog = ""
    parser = argparse.ArgumentParser(
        description=description,
        epilog=epilog
        )
    parser.add_argument('prefixes',
                        metavar='PREFIX',
                        type=str,
                        nargs='+',
                        help='Prefixes of the individual models')
    parser.add_argument('-o', '--out',
                        metavar='ENSEMBLE',
                        type=str,
                        required=True,
                        help='Prefix for saving the ensemble model')

    args = parser.parse_args()
    build_ensemble(args)
