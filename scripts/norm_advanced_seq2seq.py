#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import numpy as np
import random
import sys

from boltons.iterutils import split as bsplit
WORD_SEP = '÷'

from norm_common import get_data_from_file, train_model, \
     save_model, load_model, save_history, \
     add_common_train_params, add_common_eval_params, \
     add_model_and_learning_params, add_system_params, \
     filter_unknowns
from norm_vocab import Vocab

def Wacc(*args):
    from mblearn.metrics import sparse_ca_per_sequence_drop_mask
    return sparse_ca_per_sequence_drop_mask(*args)

def append_end_symbol(wordlist, symbol='<END>'):
    return tuple([c for c in word] + [symbol] for word in wordlist)

def convert_to_xy(data, batch_size=None,
                  word_separator=WORD_SEP,
                  prepend_X=None,
                  for_decoder_input=False,
                  sentence_input=False):
    def pad(word):
        return [word_separator] + [c for c in word] + [word_separator]
    def prepend(word):
        if prepend_X is not None:
            return [prepend_X] + word
        return word
    if not sentence_input:
        X = [prepend(pad(w)) for w in data.words(0)]
        Y = [pad(w) for w in data.words(1)]
    else:
        X = [prepend(pad(word_separator.join(words))) for words in data.sentences(0)]
        Y = [pad(word_separator.join(words)) for words in data.sentences(1)]
    if for_decoder_input:
        y = tuple([for_decoder_input] + [c for c in word] for word in Y)
    else:
        y = append_end_symbol(Y)
    if batch_size is not None:
        batch_cutoff = len(X) - (len(X) % batch_size)
        X, y = X[:batch_cutoff], y[:batch_cutoff]
    return (X, y)

def count_correct(preds, truths):
    return sum(p == t for p, t in zip(preds, truths))

def count_correct_charwise(preds, truths):
    correct, total = 0, 0
    for pred, truth in zip(preds, truths):
        correct += sum(p == t for (p, t) in zip(pred, truth))
        total   += max((len(pred), len(truth)))
    return correct, total

def is_masking_encoder(type_):
    if type_ in ("CNN", "Dense"):
        return False
    return True

def build_model(args, enc_vec, dec_vec, enc_len, dec_len):
    from keras.layers import Input, Embedding, Dense, TimeDistributed, Activation
    from keras.optimizers import Adam, SGD
    from mblearn.layers import merge
    from mblearn.models.seq2seq import Seq2seqModel
    from mblearn.models.wrappers import CategoricalModelWrapper
    from encdec import encdec2

    ### build inputs
    enc_input = Input(shape=(enc_len,), dtype='int32', name='encoder_input')
    enc_embed = Embedding(len(enc_vec), args.embedding, mask_zero=is_masking_encoder(args.encoder))(enc_input)
    dec_input = Input(shape=(dec_len,), dtype='int32', name='decoder_input')
    dec_embed_layer = Embedding(len(dec_vec), args.embedding, mask_zero=True)
    dec_embed = dec_embed_layer(dec_input)

    ### build encoder/decoder
    if hasattr(args, 'ensembles') and args.ensembles > 1:
        dec_layers = []
        for _ in range(args.ensembles):
            dec_layer = encdec2(args, enc_embed, dec_embed)
            dec_layers.append(dec_layer)
        dec_layer = merge(dec_layers, mode='ave')
    else:
        dec_layer = encdec2(args, enc_embed, dec_embed)

    ### prediction layer
    dec_layer = TimeDistributed(Dense(len(dec_vec)))(dec_layer)
    #from mblearn.layers.dense import EmbeddingTranspose
    #dec_layer = TimeDistributed(EmbeddingTranspose(dec_embed_layer))(dec_layer)

    if False: # doesn't work :(
        from mblearn.layers import ChainCRF
        dec_output = dec_layer #Activation('softmax')(dec_layer)
        crf = ChainCRF(name='decoder_output')
        loss = crf.sparse_loss
        dec_output = crf(dec_output)
    else:
        loss = "sparse_categorical_crossentropy"
        dec_output = Activation('softmax', name='decoder_output')(dec_layer)

    ### build model
    model = Seq2seqModel(
        input=[enc_input, dec_input], output=[dec_output],
        decoder_input=1, decoder_output=0,
        start_symbol=dec_vec['<START>'],
        end_symbol=[dec_vec['<END>']],
        mask_symbol=[0]
    )
    if args.optimizer == "adam":
        optimizer = Adam(lr=args.learning_rate, clipnorm=1.)
    elif args.optimizer == "sgd":
        optimizer = SGD(lr=args.learning_rate, momentum=args.momentum,
                        decay=args.decay, nesterov=(args.momentum > 0),
                        clipnorm=1.)
    else:
        optimizer = args.optimizer # doesn't use learning rate or anything...
    model.compile(
        loss=loss,
        optimizer=optimizer,
        metrics={'decoder_output': Wacc}
    )
    wrapper = CategoricalModelWrapper(
        model=model,
        input_vectorizer=[enc_vec, None],
        input_padding=['right', 'right'],
        output_vectorizer=[dec_vec]
    )
    return wrapper

def build_mtl_model(args, enc_vec, dec_vec):
    from keras.layers import Input, Embedding, Dense, TimeDistributed, Activation
    from keras.optimizers import Adam, SGD
    from mblearn.layers import merge
    from mblearn.models.seq2seq import Seq2seqModel
    from mblearn.models.wrappers import CategoricalModelWrapper
    from encdec import make_recurrent_encoder, make_recurrent_decoder

    ### build inputs
    enc_inputs = [Input(shape=(args.max_input + 2,), dtype='int32', name='encoder_{}_input'.format(i)) \
                  for i in range(len(args.infiles))]
    dec_inputs = [Input(shape=(args.max_output + 2,), dtype='int32', name='decoder_{}_input'.format(i)) \
                  for i in range(len(args.infiles))]

    ### embedding layers
    layer = Embedding(len(enc_vec), args.embedding, mask_zero=is_masking_encoder(args.encoder))
    enc_embeds = [layer(input_) for input_ in enc_inputs]
    layer = Embedding(len(dec_vec), args.embedding, mask_zero=True)
    dec_embeds = [layer(input_) for input_ in dec_inputs]

    ### shared encoder/decoder
    enc_tensors, enc_hiddens, _ = make_recurrent_encoder(args, enc_embeds)
    dec_tensors = make_recurrent_decoder(args, dec_embeds, enc_tensors, enc_hiddens)

    ### prediction layers
    dec_outputs = []
    for i, tensor in enumerate(dec_tensors):
        output_ = TimeDistributed(Dense(len(dec_vec)))(tensor)
        output_ = Activation('softmax', name='dout_{}'.format(i))(output_)
        dec_outputs.append(output_)

    ### build models
    symbols = dict(
        start_symbol=dec_vec['<START>'],
        end_symbol=[dec_vec['<END>']],
        mask_symbol=[0]
        )
    full_model = Seq2seqModel(
        input=(enc_inputs + dec_inputs),
        output=dec_outputs,
        decoder_input=list(range(len(enc_inputs), len(enc_inputs) + len(dec_inputs))),
        decoder_output=list(range(len(dec_outputs))),
        **symbols
    )
    #from keras.utils.visualize_util import plot
    #plot(full_model, to_file="{}.png".format(args.prefix[0]))
    part_models = []
    for (e_i, d_i, d_o) in zip(enc_inputs, dec_inputs, dec_outputs):
        part_models.append(Seq2seqModel(
            input=[e_i, d_i], output=[d_o],
            decoder_input=1, decoder_output=0,
            **symbols)
            )

    if args.optimizer == "adam":
        optimizer = Adam(lr=args.learning_rate, clipnorm=1.)
    elif args.optimizer == "sgd":
        optimizer = SGD(lr=args.learning_rate, momentum=args.momentum,
                        decay=args.decay, nesterov=(args.momentum > 0),
                        clipnorm=1.)
    else:
        optimizer = args.optimizer # doesn't use learning rate or anything...
    for model in part_models + [full_model]:
        model.compile(
            loss="sparse_categorical_crossentropy",
            optimizer=optimizer,
            metrics=[Wacc]
            )
    full_wrapper = CategoricalModelWrapper(
        model=full_model,
        input_vectorizer=([enc_vec] * len(enc_inputs) + [None] * len(dec_inputs)),
        input_padding=['right'] * (len(enc_inputs)+len(dec_inputs)),
        output_vectorizer=([dec_vec] * len(dec_outputs))
    )
    part_wrappers = []
    for model in part_models:
        part_wrappers.append(CategoricalModelWrapper(
            model=model,
            input_vectorizer=[enc_vec, None],
            input_padding=['right', 'right'],
            output_vectorizer=[dec_vec]
            ))

    return full_wrapper, part_wrappers

def evaluate_model(wrapper, args, infile=None):
    def print_accuracy(name, preds, truth):
        Cc, Ct = count_correct_charwise(preds, truth)
        print(name)
        print("  Char-level accuracy:  {0}/{1} = {2}".format(Cc, Ct, Cc/Ct))
        correct = count_correct(preds, truth)
        print("  Word-level accuracy:  {0}/{1} = {2}".format(correct, len(preds),
                                                             correct / len(preds)))

    if infile is None:
        infile = args.infile
    sys.stderr.write("Evaluating on: {0}\n".format(infile.name))
    data = get_data_from_file(infile)
    sys.stderr.write("Found {} words.\n".format(data.word_count))
    prepend_X = '<Task:{}>'.format(args.task) if args.task else None
    if prepend_X and prepend_X not in wrapper.input_vectorizer[0]:
        raise Exception("This model does not contain the task: {}".format(args.task))
    elif prepend_X is None and any(v.startswith('<Task:') for v in wrapper.input_vectorizer[0]):
        sys.stderr.write("CAUTION: model seems to use task embeddings, but none was selected!\n")
    X, y = convert_to_xy(data, prepend_X=prepend_X, sentence_input=args.sentence_input)
    preds = wrapper.predict_classes([X, None], batch_size=args.batch, verbose=args.verbose)[0]
    if hasattr(args, 'print') and args.print:
        if args.sentence_input:
            for p_sent in preds:
                assert p_sent[-1] == "<END>",  "Unexpected format: {}".format(''.join(p))
                #assert p_sent[-2] == WORD_SEP, "Unexpected format: {}".format(''.join(p))
                range_end = -2 if p_sent[-2] == WORD_SEP else -1
                if p_sent[-2] != WORD_SEP:
                    print("## WARN: Unexpected format: {}".format(''.join(p_sent)), file=sys.stderr)
                for p in bsplit(p_sent[1:range_end], sep=WORD_SEP):
                    if p:
                        print(''.join(p))
                    else:
                        # prevent writing an empty line even though it's not the
                        # end of the sentence -- print WORD_SEP as placeholder
                        print(WORD_SEP)
                print('')
        else:
            sentence_boundaries = set()
            offset = -1
            for s in data.sentences(0):
                offset += len(s)
                sentence_boundaries.add(offset)
            for i, p in enumerate(preds):
                assert p[-1] == "<END>",  "Unexpected format: {}".format(''.join(p))
                #assert p[-2] == WORD_SEP, "Unexpected format: {}".format(''.join(p))
                range_end = -2 if p[-2] == WORD_SEP else -1
                if p[-2] != WORD_SEP:
                    print("## WARN: Unexpected format: {}".format(''.join(p)), file=sys.stderr)
                print(''.join(p[1:range_end]))
                if i in sentence_boundaries:
                    print('')
    else:
        if args.sentence_input:
            print(("Caution: There is no trivial way to convert sentence output back\n"
                   "         to word output, so 'word accuracy' will actually mean\n"
                   "         'sentence accuracy' here."), file=sys.stderr)
        print_accuracy("Seq2seq", preds, y)
        traindata = None
        if args.command == "train":
            args.infile.seek(0)
            traindata = get_data_from_file(args.infile)
        elif args.trainfile:
            traindata = get_data_from_file(args.trainfile)
        if traindata is not None:
            train_X, train_y = convert_to_xy(traindata, prepend_X=prepend_X, sentence_input=args.sentence_input)
            vocab = Vocab(train_X, train_y)
            # Greedy mapper
            preds_map = vocab.predict_with_map(X, preds, min_count=0, max_ambiguity=1.0)
            print_accuracy("Mapper (greedy: c>=0, a<=1) + Seq2seq", preds_map, y)
            # Semi-greedy mapper
            preds_map = vocab.predict_with_map(X, preds, min_count=0, max_ambiguity=0.75)
            print_accuracy("Mapper (semi-g: c>=0, a<=0.75) + Seq2seq", preds_map, y)
            # Strict mapper
            preds_map = vocab.predict_with_map(X, preds, min_count=0, max_ambiguity=0.0)
            print_accuracy("Mapper (strict: c>=0, a==0) + Seq2seq", preds_map, y)

def train(args):
    from mblearn.utils import Vectorizer
    from mblearn.models.decoders import GreedyDecoder
    from keras.callbacks import EarlyStopping, ModelCheckpoint
    from mblearn.callbacks import ClearLine

    data = get_data_from_file(args.infile, cutoff=(args.max_input, args.max_output))
    enc_vectorizer = Vectorizer(unk_symbol="<UNK>")
    enc_vectorizer.update(data.characters(0))
    enc_vectorizer.update((WORD_SEP,))
    dec_vectorizer = Vectorizer(unk_symbol="<UNK>")
    dec_vectorizer.update(data.characters(1))
    dec_vectorizer.update(('<START>', '<END>',WORD_SEP))

    if args.pretrain:
        pretrain_data = get_data_from_file(args.pretrain,
                                           cutoff=(args.max_input, args.max_output))
        enc_vectorizer.update(pretrain_data.characters(0))
        dec_vectorizer.update(pretrain_data.characters(1))

    wrapper = build_model(
        args, enc_vectorizer, dec_vectorizer,
        args.max_input + 2, args.max_output + 2
    )
    params = {'validation_split': 0.0}

    if args.pretrain:
        pre_X, pre_y = convert_to_xy(pretrain_data, sentence_input=args.sentence_input)
        epochs, args.epochs = args.epochs, args.pretrain_epochs
        train_model(wrapper, [pre_X, None], [pre_y], args, **params)
        args.epochs = epochs

    X, y = convert_to_xy(data, sentence_input=args.sentence_input)
    if args.validate:
        v_data = get_data_from_file(args.validate)
        X_v, y_v = convert_to_xy(v_data, sentence_input=args.sentence_input)
        params['validation_data'] = ([X_v, None], [y_v])
    else:
        params['validation_split'] = args.validation_split

    if args.early_stopping:
        callbacks = [ClearLine()] if args.verbose else []
        min_delta = 0.001
        # patience between [1, 10], depending on training set size
        patience = max(min(round(10000 * (1 / len(X))), 10), 1)
        callbacks.append(EarlyStopping(monitor='val_loss', min_delta=min_delta,
                                       patience=patience, mode='min',
                                       verbose=args.verbose))
        if args.prefix:
            filepath = args.prefix + '.h5'
            callbacks.append(ModelCheckpoint(filepath, monitor='val_Wacc',
                                             mode='max', period=1,
                                             save_best_only=True,
                                             save_weights_only=True,
                                             verbose=args.verbose))
        params['callbacks'] = callbacks

    history = train_model(wrapper, [X, None], [y], args, **params)

    if args.prefix:
        save_model(wrapper, args.prefix, skip_weights=args.early_stopping)
        save_history(history, args.prefix)

    if args.eval is not None:
        wrapper.model.decoder = GreedyDecoder()
        for evalfile in args.eval:
            evaluate_model(wrapper, args, infile=evalfile)

def mtl_shuffler(train_Xy, samples_per_epoch, batch_size):
    def cycle_random(data):
        indices = list(range(len(data)))
        while True:
            random.shuffle(indices)
            for idx in indices:
                yield data[idx]

    batch_pos = 0
    generators = [cycle_random(list(zip(*Xy))) for Xy in train_Xy]
    while True:
        batch_len = min(batch_size, samples_per_epoch - batch_pos)
        batch_Xy = [list(zip(*[next(gen) for _ in range(batch_len)])) for gen in generators]
        batch_inputs = [Xy[0] for Xy in batch_Xy] + [None] * len(generators)
        batch_outputs = [Xy[1] for Xy in batch_Xy]
        batch_pos += batch_len
        batch_pos %= samples_per_epoch
        yield (batch_inputs, batch_outputs)

def mtl_flat_shuffler(train_Xy, samples_per_epoch, batch_size):
    mtl_spe, mtl_bs = samples_per_epoch // len(train_Xy), batch_size // len(train_Xy)
    for (batch_inputs, batch_outputs) in mtl_shuffler(train_Xy, mtl_spe, mtl_bs):
        flat_inputs  = [x for y in zip(*batch_inputs[:len(train_Xy)]) for x in y]
        flat_outputs = [x for y in zip(*batch_outputs) for x in y]
        yield ([flat_inputs, None], [flat_outputs])

def train_mtl(args):
    from mblearn.utils import Vectorizer
    from mblearn.models.decoders import GreedyDecoder
    from keras.callbacks import EarlyStopping, ModelCheckpoint
    from mblearn.callbacks import ClearLine, MultiModelCheckpoint, MultiEarlyStopping

    filenames = []
    datasets = []
    enc_vectorizer = Vectorizer(unk_symbol="<UNK>")
    dec_vectorizer = Vectorizer(unk_symbol="<UNK>")
    dec_vectorizer.update(('<START>', '<END>'))

    for infile in args.infiles:
        filenames.append(infile.name)
        data = get_data_from_file(infile, cutoff=(args.max_input, args.max_output))
        datasets.append(data)

    if args.with_task_embeddings:
        train_Xy = [convert_to_xy(data, prepend_X='<Task:{}>'.format(task), sentence_input=args.sentence_input) \
                    for data, task in zip(datasets, args.with_task_embeddings)]
    else:
        train_Xy = [convert_to_xy(data, sentence_input=args.sentence_input) for data in datasets]

    enc_vectorizer.update(c for Xy in train_Xy for w in Xy[0] for c in w)
    dec_vectorizer.update(c for Xy in train_Xy for w in Xy[1] for c in w)

    if args.with_task_embeddings:
        if len(args.with_task_embeddings) != len(datasets):
            raise Exception(
                "Number of task names must match number of input files ({} != {})"\
                .format(len(args.with_task_embeddings), len(datasets))
                )
        train_wrapper = build_model(
            args, enc_vectorizer, dec_vectorizer,
            args.max_input + 3, args.max_output + 2
        )
    else:
        train_wrapper, part_wrappers = build_mtl_model(
            args, enc_vectorizer, dec_vectorizer
        )
    params = {}

    if args.validate:
        validate_X, validate_y = [], []
        if len(args.validate) != len(datasets):
            raise Exception(
                "Number of validation files must match number of input files ({} != {})"\
                .format(len(args.validate), len(datasets))
                )
        for j, infile in enumerate(args.validate):
            data = get_data_from_file(infile)
            if args.with_task_embeddings:
                task = args.with_task_embeddings[j]
                v_X, v_y = convert_to_xy(data, prepend_X='<Task:{}>'.format(task), sentence_input=args.sentence_input)
                validate_X += v_X
                validate_y += v_y
            else:
                v_X, v_y = convert_to_xy(data, sentence_input=args.sentence_input)
                validate_X.append(v_X)
                validate_y.append(v_y)
        if args.with_task_embeddings:
            validate_X = [validate_X]
            validate_y = [validate_y]
        val_sizes = [len(X) for X in validate_X]
        if not all(size == min(val_sizes) for size in val_sizes):
            sys.stderr.write("Validation files have different lengths; "
                             "only using first {} samples from each file\n".format(min(val_sizes)))
            validate_X = [X[:min(val_sizes)] for X in validate_X]
            validate_y = [y[:min(val_sizes)] for y in validate_y]
        params['validation_data'] = (
            (validate_X + [None] * len(validate_X)),
            validate_y
            )

    if not args.samples_per_epoch:
        args.samples_per_epoch = len(train_Xy[0][0])
    if args.with_task_embeddings:
        args.samples_per_epoch *= len(datasets)
        args.batch *= len(datasets)

    if args.prefix and not args.with_task_embeddings:
        if len(args.prefix) < len(part_wrappers):
            prefixes = ['_'.join((args.prefix[0], str(i))) for i in range(len(part_wrappers))]
        else:
            prefixes = args.prefix[:len(part_wrappers)]

    if args.early_stopping:
        callbacks = [ClearLine()] if args.verbose else []
        min_delta = 0.001
        # patience between [1, 10], depending on training set size
        patience = max(min(round(30000 * (1 / args.samples_per_epoch)), 10), 1)
        if args.with_task_embeddings:
            callbacks.append(EarlyStopping(monitor='val_loss', min_delta=min_delta,
                                           patience=patience, mode='min',
                                           verbose=args.verbose))
            if args.prefix:
                filepath = args.prefix[0] + '.h5'
                callbacks.append(ModelCheckpoint(filepath, monitor='val_Wacc',
                                                 mode='max', period=1,
                                                 save_best_only=True,
                                                 save_weights_only=True,
                                                 verbose=args.verbose))
        else:
            monitor = ['val_dout_{}_Wacc'.format(i) for i in range(len(part_wrappers))]
            callbacks.append(MultiEarlyStopping(monitor=monitor, combine_mode='all',
                                                min_delta=min_delta,
                                                patience=patience, mode='max',
                                                verbose=args.verbose))
            if args.prefix:
                callbacks.append(MultiModelCheckpoint(part_wrappers, [p + ".h5" for p in prefixes],
                                                      monitor='val_dout_{}_Wacc',
                                                      mode='max', period=1,
                                                      save_best_only=True,
                                                      save_weights_only=True,
                                                      verbose=args.verbose))
        params['callbacks'] = callbacks

    try:
        shuffler = mtl_flat_shuffler if args.with_task_embeddings else mtl_shuffler
        history = train_wrapper.fit_generator(
            shuffler(
                train_Xy,
                args.samples_per_epoch,
                args.batch),
            args.samples_per_epoch,
            args.epochs,
            verbose=args.verbose,
            **params
            )
    except KeyboardInterrupt:
        history = []

    if args.prefix:
        if args.with_task_embeddings:
            save_model(train_wrapper, args.prefix[0], skip_weights=args.early_stopping)
            save_history(history, args.prefix[0])
        else:
            for wrapper, prefix in zip(part_wrappers, prefixes):
                save_model(wrapper, prefix, skip_weights=args.early_stopping)
                save_history(history, prefix)


def evaluate(args):
    from mblearn.layers import HiddenStateLSTM, MaskedMerge, merge, TimeCutoff
    from mblearn.models.seq2seq import Seq2seq
    from mblearn.models.decoders import GreedyDecoder, BeamSearchDecoder
    from mblearn.models.filters import DictionaryFilter
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer

    wrapper = load_model(args.prefix)
    wrapper.model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer="adam",
        metrics={'decoder_output': Wacc}
    )
    if args.decoder == 'greedy':
        wrapper.model.decoder = GreedyDecoder()
    elif args.decoder == 'beam':
        wrapper.model.decoder = BeamSearchDecoder(beam_width=args.width)
    if args.filter:
        words = [w.strip() for w in args.filter.readlines()]
        vectorizer = wrapper.output_vectorizer[wrapper.model.decoder_output[0]]
        eos_idx = wrapper.model.end_symbol[0]
        sys.stderr.write("Building dictionary...")
        dictfilt = DictionaryFilter(words, vectorizer, eos_idx)
        sys.stderr.write(" done.\n")
        wrapper.model.decoder.append_filter(dictfilt)

    evaluate_model(wrapper, args)

def proba(args):
    args.verbose = 0
    from mblearn.layers import HiddenStateLSTM, MaskedMerge, merge, TimeCutoff
    from mblearn.models import CopyDecoder
    from mblearn.models.seq2seq import Seq2seq
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer

    wrapper = load_model(args.prefix)
    wrapper.model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer="adam",
        metrics={'decoder_output': Wacc}
    )
    wrapper.model.decoder = CopyDecoder()
    ### hard-coded ... only works with one specific type of model
    wrapper.input_vectorizer[1] = wrapper.output_vectorizer[0]

    data = get_data_from_file(args.infile)
    if args.predictions:
        from mblearn.data import AnnotatedTextData
        preds = AnnotatedTextData(args.predictions.read())
        tokens = ['\t'.join(tok) for tok in zip(data.words(0), preds.words(-1))]
        data = AnnotatedTextData(tokens, cols=2)
        del preds

    prepend_X = '<Task:{}>'.format(args.task) if args.task else None
    if prepend_X and prepend_X not in wrapper.input_vectorizer[0]:
        raise Exception("This model does not contain the task: {}".format(args.task))
    elif prepend_X is None and any(v.startswith('<Task:') for v in wrapper.input_vectorizer[0]):
        sys.stderr.write("CAUTION: model seems to use task embeddings, but none was selected!\n")
    X, y = convert_to_xy(data, prepend_X=prepend_X, sentence_input=args.sentence_input)

    preds = wrapper.predict_proba([X, y], batch_size=args.batch, verbose=args.verbose)[0]
    for word in preds:
        best = [max(elem, key=lambda k: k[1]) for elem in word]
        for i, elem in enumerate(best):
            if elem[0] == "<END>":
                break
        best = best[:i+1]
        pred = ''.join(e[0] for e in best[1:-2])
        prob = sum(e[1] for e in best) / len(best)
        print("{}\t{}".format(pred, prob))


def embed(args):
    from keras import backend as K
    from keras.models import Model
    from mblearn.layers import AttentionLSTM, HiddenStateLSTM, MaskedMerge, merge, TimeCutoff, BidirectionalHidden
    from mblearn.models.seq2seq import Seq2seq
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer
    from mblearn.serialization import dump, load

    from pprint import pprint

    wrapper = load_model(args.prefix)
    model = wrapper.model

    if not args.words:
        embedding = model.get_layer(name="embedding_1")
        embeds, *_ = embedding.get_weights()
        for n in range(embeds.shape[0]):
            char = wrapper.input_vectorizer[0].get_item(n)
            weights = [str(w) for w in embeds[n]]
            print("\t".join((char, ",".join(weights))))

    else:
        model.compile(loss="sparse_categorical_crossentropy", optimizer="adam")
        enc_in  = model.get_layer(name="encoder_input")
        if enc_in is None:
            enc_in = model.get_layer(name="main_enc_input")
        enc_in = enc_in.input
        enc_out = model.get_layer(name="hiddenstatelstm_1").output
        get_output = K.function([enc_in, K.learning_phase()], enc_out)
        X, _ = wrapper.transform_inputs([list(args.words), None], None)
        embeds, *_ = get_output([X[0], False])
        for n in range(embeds.shape[0]):
            word = str(args.words[n])
            weights = [str(w) for w in embeds[n]]
            print("\t".join((word, ",".join(weights))))


def pratt(args):
    from keras import backend as K
    from keras.models import Model
    from mblearn.layers import AttentionLSTM, AttentionGRU, \
                               HiddenStateLSTM, MaskedMerge, merge, \
                               TimeCutoff, BidirectionalHidden
    from mblearn.models.seq2seq import Seq2seq
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer
    from mblearn.serialization import dump, load

    if args.plot:
        import matplotlib
        matplotlib.use('Agg')
        import seaborn as sns

    wrapper = load_model(args.prefix)
    for n, layer in enumerate(wrapper.model.layers):
        if isinstance(layer, (AttentionLSTM, AttentionGRU)):
            attn_idx = n
            break
    else:
        print(wrapper.model.layers)
        print("Couldn't find attentional layer!")
        exit(1)

    wrapper.model.layers[attn_idx].output_alpha = True
    # hack to make "output_alpha" take effect... there must be a better way
    wrapper.model = load(dump(wrapper.model))
    wrapper.model.load_weights(args.prefix + '.h5')
    wrapper.model = Model(input=wrapper.model.inputs, output=wrapper.model.layers[attn_idx].output)
    wrapper.model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer="adam"
    )

    print("Generating attention weights from: {0}".format(args.infile.name))
    data = get_data_from_file(args.infile)
    X, y = convert_to_xy(data, for_decoder_input='<START>', sentence_input=args.sentence_input)
    wrapper.input_vectorizer[1] = wrapper.output_vectorizer[0]
    #wrapper.input_padding = ['left', 'right']
    preds = wrapper.predict([X, y], batch_size=args.batch, verbose=args.verbose)

    for i, (p_i, x_i, y_i) in enumerate(zip(preds, X, y)):
        if wrapper.input_padding[0] == 'right':
            weight_map = p_i[:len(y_i), :len(x_i)].T
        else:
            weight_map = p_i[-len(y_i):, -len(x_i):].T
        y_i = y_i[1:] + ['<END>']
        if args.csv:
            args.csv.write("\t" + "\t".join(y_i) + "\n")
            for j, x_j in enumerate(x_i):
                args.csv.write(x_j + "\t")
                weight_row = ["{0:.4f}".format(e) for e in weight_map[j]]
                args.csv.write("\t".join(weight_row) + "\n")
        if args.plot:
            ax = sns.heatmap(weight_map[:, ::-1],
                             xticklabels=y_i,
                             yticklabels=x_i[::-1],
                             square=True, robust=True,
                             cbar=False)
            fig = ax.get_figure()
            fig.savefig(args.plot + str(i) + ".png")


if __name__ == "__main__":
    description = "Normalization using encoder-decoder architectures."
    epilog = ("You can also specify command-line arguments by saving them "
              "to a file <opts_file> containing one argument per line, "
              "then add them by specifying an argument '@<opts_file>'.")
    parser = argparse.ArgumentParser(
        description=description,
        epilog=epilog,
        fromfile_prefix_chars='@'
        )
    subparsers = parser.add_subparsers(title="available commands", dest='command')

    p_train = subparsers.add_parser('train')
    p_train.set_defaults(func=train,
                         help="Train a model")
    add_common_train_params(p_train)
    p_train.add_argument('-u', '--recurrent-unit',
                         dest="recurrent_unit",
                         choices=('LSTM','GRU','SimpleRNN','htcLSTM','RAN'),
                         default=None,
                         help=('Recurrent unit to use (compatibility alias for '
                               'setting --encoder/--decoder simultaneously)'))
    p_train.add_argument('--encoder',
                         choices=('LSTM','GRU','SimpleRNN','htcLSTM','RAN','CNN','Dense'),
                         default='LSTM',
                         help='Type of encoder to use (default: %(default)s)')
    p_train.add_argument('--decoder',
                         choices=('LSTM','GRU','SimpleRNN','htcLSTM','RAN'),
                         default='LSTM',
                         help='Type of decoder to use (default: %(default)s)')
    p_train.add_argument('--att',
                         nargs='?',
                         choices=('global','local','mono','diag'),
                         const='global',
                         help='Use attentional decoders (default type: global)')
    p_train.add_argument('--bidir',
                         action='store_true',
                         default=False,
                         help='Use bi-directional encoders')
    p_train.add_argument('--early-stopping',
                         action='store_true',
                         default=False,
                         help=('Stop training early if validation accuracy '
                               'no longer improves; with --prefix, the best '
                               'epoch is saved instead of the last one'))
    p_train.add_argument('--ensembles',
                         metavar='N',
                         type=int,
                         default=1,
                         help='Train an ensemble of N enc/decs')
    p_train.add_argument('--pretrain',
                         metavar='INPUT',
                         type=argparse.FileType('r', encoding="UTF-8"),
                         help='File for pretraining the model')
    p_train.add_argument('--pretrain-epochs',
                         metavar='N',
                         type=int,
                         default=0,
                         help=('Number of epochs for pretraining (default: '
                               'same as --epochs)'))
    p_train.add_argument('--sentence-input',
                         action='store_true',
                         default=False,
                         help='Use full sentences as input, not words')

    p_multi = subparsers.add_parser('mtl')
    p_multi.set_defaults(func=train_mtl,
                         help="Train a multi-task learning model")
    p_multi.add_argument('infiles',
                         metavar='INPUT',
                         type=argparse.FileType('r', encoding="UTF-8"),
                         nargs='+',
                         help='Files to train on')
    p_multi.add_argument('--validate',
                         metavar='INPUT',
                         type=argparse.FileType('r', encoding="UTF-8"),
                         nargs='+',
                         help=('Files to validate on (must match '
                               'the number of input files)'))
    p_multi.add_argument('-x', '--prefix',
                         metavar='STR',
                         type=str,
                         nargs='+',
                         help=('Prefix(es) for saving the models; '
                               'if only one is given, the name of each dataset '
                               'will be appended to it (default: do not save)'))
    add_model_and_learning_params(p_multi)
    add_system_params(p_multi)
    p_multi.add_argument('--encoder',
                         choices=('LSTM','GRU','SimpleRNN','htcLSTM','RAN','CNN'),
                         default='LSTM',
                         help='Type of encoder to use (default: %(default)s)')
    p_multi.add_argument('--decoder',
                         choices=('LSTM','GRU','SimpleRNN','htcLSTM','RAN'),
                         default='LSTM',
                         help='Type of decoder to use (default: %(default)s)')
    p_multi.add_argument('--att',
                         nargs='?',
                         choices=('global','local','mono','diag'),
                         const='global',
                         help='Use attentional decoders (default type: global)')
    p_multi.add_argument('--bidir',
                         action='store_true',
                         default=False,
                         help='Use bi-directional encoders')
    p_multi.add_argument('--early-stopping',
                         action='store_true',
                         default=False,
                         help=('Stop training early if average validation '
                               'accuracy across all validation sets '
                               'no longer improves; with --prefix, the best '
                               'epoch is saved instead of the last one'))
    p_multi.add_argument('--samples-per-epoch',
                         type=int,
                         help=('Number of samples (from all input files) that '
                               'should be considered an epoch; if not given, '
                               'the size of the first input file will be used'))
    p_multi.add_argument('--sentence-input',
                         action='store_true',
                         default=False,
                         help='Use full sentences as input, not words')
    p_multi.add_argument('--with-task-embeddings',
                         metavar='TASK',
                         type=str,
                         nargs='+',
                         help=('Use task embeddings with these names (must match '
                               'the number of input files)'))

    p_eval = subparsers.add_parser('eval')
    p_eval.set_defaults(func=evaluate,
                        help="Evaluate a model")
    add_common_eval_params(p_eval)
    p_eval.add_argument('-d', '--decoder',
                        choices=('greedy','beam'),
                        default='greedy',
                        help='Decoder to use (default: %(default)s)')
    p_eval.add_argument('-w', '--width',
                        type=int,
                        default=5,
                        help='Beam width when using "--decoder beam" (default: %(default)i)')
    p_eval.add_argument('-f', '--filter',
                        metavar='FILE',
                        type=argparse.FileType('r', encoding="UTF-8"),
                        default=None,
                        help='File with words for a dictionary filter')
    p_eval.add_argument('--sentence-input',
                        action='store_true',
                        default=False,
                        help='Use full sentences as input, not words')
    p_eval.add_argument('--print',
                        action='store_true',
                        default=False,
                        help='Print predictions instead of scores')
    p_eval.add_argument('--trainfile',
                        metavar='FILE',
                        type=argparse.FileType('r', encoding="UTF-8"),
                        default=None,
                        help='File the model was trained on; used for extra evaluations')
    p_eval.add_argument('--task',
                        metavar=str,
                        default=None,
                        help=('Select a specific task in a model trained with '
                              'task embeddings'))

    p_prob = subparsers.add_parser('proba')
    p_prob.set_defaults(func=proba,
                        help="Output probabilities associated with predictions")
    add_common_eval_params(p_prob)
    p_prob.add_argument('-p', '--predictions',
                        metavar='NORMFILE',
                        type=argparse.FileType('r', encoding="UTF-8"),
                        help=('File with predictions for which probabilities '
                              'should be calculated; if not given, predictions '
                              'are expected in INPUT file instead.'))
    p_prob.add_argument('--sentence-input',
                       action='store_true',
                       default=False,
                       help='Use full sentences as input, not words')
    p_prob.add_argument('--task',
                        metavar=str,
                        default=None,
                        help=('Select a specific task in a model trained with '
                              'task embeddings'))

    p_att = subparsers.add_parser('pratt')
    p_att.set_defaults(func=pratt,
                       help="Print attention weights")
    p_att.add_argument('--sentence-input',
                       action='store_true',
                       default=False,
                       help='Use full sentences as input, not words')
    p_att.add_argument('--csv',
                       metavar='FILE',
                       type=argparse.FileType('w', encoding="UTF-8"),
                       default=sys.stdout,
                       help='Save to file as CSV (default: <stdout>)')
    p_att.add_argument('--plot',
                       metavar='FILEPREFIX',
                       type=str,
                       default=None,
                       help='Save plots as FILEPREFIX{n}.png')
    add_common_eval_params(p_att)

    p_emb = subparsers.add_parser('embed')
    p_emb.set_defaults(func=embed,
                       help="Extract embeddings")
    p_emb.add_argument('-s', '--seed',
                       metavar='N',
                       type=int,
                       default=0,
                       help='Seed for RNG (default: 0=none)')
    p_emb.add_argument('-w', '--words',
                       type=str,
                       nargs='+',
                       help='Words to generate embeddings for')
    p_emb.add_argument('-x', '--prefix',
                       metavar='STR',
                       type=str,
                       required=True,
                       help='Prefix for loading the model (required)')

    args = parser.parse_args()
    if args.seed > 0:
        np.random.seed(args.seed)
    if args.command == "eval" and args.print:
        args.verbose = 0
    if args.command == "train":
        if args.pretrain_epochs == 0:
            args.pretrain_epochs = args.epochs
        if args.early_stopping and args.validation_split <= 0 \
            and not args.validate:
            parser.error("--early-stopping requires validation data")
        if (args.momentum > 0 or args.decay > 0) \
            and args.optimizer != "sgd":
            parser.error("--momentum/--decay only work with --optimizer sgd")
        #if args.recurrent_unit is not None:
        #    args.decoder = args.recurrent_unit
        #    args.encoder = args.recurrent_unit
        print("Encoder: {}   Decoder: {}".format(args.encoder, args.decoder))

    args.func(args)
