import argparse
import csv
import sys

def filter_unknowns(X, y, preds, train_X):
    f_y, f_preds = [], []
    vocabulary = set(train_X)
    for (i_X, i_y, i_p) in zip(X, y, preds):
        if i_X not in vocabulary:
            f_y.append(i_y)
            f_preds.append(i_p)
    return (f_y, f_preds)

def get_data_from_file(infile, cutoff=None, cutoff_mode="remove"):
    from mblearn.data import AnnotatedTextData
    data = AnnotatedTextData(infile.read(), cols=2)
    if cutoff is not None:
        (max_in, max_out) = cutoff
        input_length = min(max_in, max(len(x) for x in data.words(0)))
        output_length = min(max_out - 1, max(len(y) for y in data.words(1)))
        data.length_cutoff(input_length, layer=0, mode=cutoff_mode)
        data.length_cutoff(output_length, layer=1, mode=cutoff_mode)
    data.lower(layer="all")
    return data

def train_model(model, X, y, args, **kwargs):
    if 'batch_size' not in kwargs:
        kwargs['batch_size'] = args.batch
    if 'nb_epoch' not in kwargs:
        kwargs['nb_epoch'] = args.epochs
    if 'verbose' not in kwargs:
        kwargs['verbose'] = args.verbose
    try:
        history = model.fit(
            X, y,
            **kwargs
        )
    except KeyboardInterrupt:
        history = None

    return history

def save_history(history, prefix):
    columns = []
    for metric in ("acc", "loss", "Wacc", "perplexity"):
        if metric in history.history:
            columns.append(metric)
        if "val_" + metric in history.history:
            columns.append("val_" + metric)
    epochs = len(history.history[columns[0]])
    with open(prefix + '.history.tsv', 'w', newline='') as csvfile:
        f = csv.writer(csvfile, delimiter="\t",
                       quoting=csv.QUOTE_MINIMAL,
                       lineterminator="\n")
        f.writerow(["epoch"] + columns)
        for i in range(epochs):
            f.writerow([i+1] + [history.history[m][i] for m in columns])

def save_model(model, prefix, skip_weights=False):
    from mblearn.serialization import dump
    sys.stderr.write("Saving model...\n")
    with open(prefix + '.yaml', 'w') as f:
        f.write(dump(model))
    if not skip_weights:
        model.model.save_weights(prefix + '.h5', overwrite=True)

def load_model(prefix):
    from mblearn.serialization import load
    sys.stderr.write("Loading model...\n")
    with open(prefix + '.yaml', 'r') as f:
        model = load(f.read())
    model.model.load_weights(prefix + '.h5')
    return model

def add_system_params(parser):
    parser.add_argument('-V', '--verbose',
                        choices=(0,1,2),
                        type=int,
                        default=1,
                        help='Output verbosity')

def add_model_and_learning_params(parser):
    parser.add_argument('-b', '--batch',
                        metavar='SIZE',
                        type=int,
                        default=50,
                        help='Batch size (default: %(default)i)')
    parser.add_argument('-d', '--depth',
                        metavar='N',
                        type=int,
                        default=2,
                        help='Depth of the recurrent layers (default: %(default)i)')
    parser.add_argument('-e', '--epochs',
                        metavar='N',
                        type=int,
                        default=20,
                        help='Number of epochs (default: %(default)i)')
    parser.add_argument('-i', '--hidden',
                        metavar='DIM',
                        type=int,
                        default=128,
                        help='Dimensionality of hidden layer (default: %(default)i)')
    parser.add_argument('-m', '--embedding',
                        metavar='DIM',
                        type=int,
                        default=64,
                        help='Dimensionality of embedding layer (default: %(default)i)')
    parser.add_argument('-o', '--optimizer',
                        choices=('adam','sgd'),
                        default='adam',
                        help='Optimizer to use (default: %(default)s)')
    parser.add_argument('--lr',
                        dest="learning_rate",
                        type=float,
                        default=0.003,
                        help='Learning rate (default: %(default)f)')
    parser.add_argument('--decay',
                        type=float,
                        default=0.,
                        help='Learning rate decay to use with SGD (default: %(default)f)')
    parser.add_argument('--momentum',
                        type=float,
                        default=0.,
                        help='Momentum to use with SGD (default: %(default)f)')
    parser.add_argument('-r', '--dropout',
                        metavar='D',
                        type=float,
                        default=0.0,
                        help='Dropout (default: %(default)f)')
    parser.add_argument('-s', '--seed',
                        metavar='N',
                        type=int,
                        default=0,
                        help='Seed for RNG (default: 0=none)')
    parser.add_argument('--max-input',
                        metavar='N',
                        type=int,
                        default=12,
                        help='Maximum input length (default: %(default)i)')
    parser.add_argument('--max-output',
                        metavar='N',
                        type=int,
                        default=12,
                        help='Maximum output length (default: %(default)i)')

def add_common_train_params(parser):
    parser.add_argument('infile',
                        metavar='INPUT',
                        type=argparse.FileType('r', encoding="UTF-8"),
                        default=sys.stdin,
                        help='File to train on (default: STDIN)')
    parser.add_argument('--eval',
                        metavar='INPUT',
                        type=argparse.FileType('r', encoding="UTF-8"),
                        nargs="+",
                        help='File(s) to evaluate on')
    parser.add_argument('--validate',
                        metavar='INPUT',
                        type=argparse.FileType('r', encoding="UTF-8"),
                        help='File to validate on (implies --validation-split 0)')
    parser.add_argument('-v', '--validation-split',
                        metavar='VAL',
                        type=float,
                        default=0.0,
                        help='Amount of data to use for validation split (default: %(default)f)')
    parser.add_argument('-x', '--prefix',
                        metavar='STR',
                        type=str,
                        default="",
                        help='Prefix for saving the model (default: None -- do not save)')
    add_model_and_learning_params(parser)
    add_system_params(parser)

def add_common_eval_params(parser):
    parser.add_argument('infile',
                        metavar='INPUT',
                        type=argparse.FileType('r', encoding="UTF-8"),
                        default=sys.stdin,
                        help='File to evaluate on (default: STDIN)')
    parser.add_argument('-b', '--batch',
                        metavar='SIZE',
                        type=int,
                        default=250,
                        help='Batch size (default: %(default)i)')
    parser.add_argument('-s', '--seed',
                        metavar='N',
                        type=int,
                        default=0,
                        help='Seed for RNG (default: 0=none)')
    parser.add_argument('-x', '--prefix',
                        metavar='STR',
                        type=str,
                        required=True,
                        help='Prefix for loading the model (required)')
    add_system_params(parser)
