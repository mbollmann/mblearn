"""Functions to build and apply encoder/decoder layers.

Building the layers (= returning a list of layer objects) and applying them (to
an input, returning a tensor) is separated to make it easier to build models
with shared layers.

In theory, it would be best to just build a Model for each encoder/decoder,
which can than be easily applied to any input, however this doesn't currently
work with masking.
"""

from keras.layers import *
from mblearn.layers import \
     AttentionLSTM, AttentionGRU, HiddenStateLSTM, HiddenStateGRU, \
     HiddenStateSimpleRNN, HiddenToCellStateLSTM, HiddenStateRAN, \
     merge, BidirectionalHidden, RAN, \
     MaskingFrom

def get_recurrent_unit(ru):
    if ru == "LSTM":
        return HiddenStateLSTM
    elif ru == "htcLSTM":
        return HiddenToCellStateLSTM
    elif ru == "GRU":
        return HiddenStateGRU
    elif ru == "RAN":
        return HiddenStateRAN
    else:
        return HiddenStateSimpleRNN

def get_attention_unit(ru):
    if ru == "LSTM":
        return AttentionLSTM
    elif ru == "GRU":
        return AttentionGRU
    else:
        raise Exception("No attentional variant of {}".format(ru))

def build_encoder_layers(hidden_dim, bidirectional=False, depth=2,
                         recurrent_unit="LSTM", return_sequences=False, **kwargs):
    layers = []
    RNN = get_recurrent_unit(recurrent_unit)
    if bidirectional:
        if type(bidirectional) is bool:
            bidirectional = 'concat'
        if bidirectional == 'concat':
            hidden_dim //= 2
    for n in range(depth, 0, -1):
        rs = (n>1) or return_sequences
        rnn = RNN(hidden_dim, return_sequences=rs, **kwargs)
        if bidirectional:
            rnn = BidirectionalHidden(rnn, merge_mode=bidirectional)
        layers.append(rnn)
    return layers

def build_decoder_layers(hidden_dim, depth=2, recurrent_unit="LSTM",
                         with_attention=False, **kwargs):
    layers = []
    RNN = get_recurrent_unit(recurrent_unit)
    for i in range(depth):
        if with_attention and i == 0:
            if recurrent_unit == "LSTM":
                layer = AttentionLSTM(hidden_dim, return_sequences=True, **kwargs)
            elif recurrent_unit == "GRU":
                layer = AttentionGRU(hidden_dim, return_sequences=True, **kwargs)
            else:
                raise Exception("Attentional decoder not supported with {}".format(recurrent_unit))
        else:
            layer = RNN(hidden_dim, return_sequences=True, **kwargs)
        layers.append(layer)
    return layers

def encode(enc_input, layers, return_all_layers=False, hidden_states=None):
    enc_layer = enc_input
    enc_layers, hidden_out = [], []
    for n, layer in enumerate(layers):
        if hidden_states:
            layer_input = [enc_layer] + hidden_states[n]
        else:
            layer_input = enc_layer
        if isinstance(layer, list):
            sub_outs, sub_hiddens = [], []
            for sublayer in layer:
                so, *sh = sublayer(layer_input)
                sub_outs.append(so)
                sub_hiddens.append(sh)
            enc_layer = merge(sub_outs, mode='concat', concat_axis=-1)
            hidden = [merge(h, mode='ave') for h in zip(*sub_hiddens)]
        else:
            enc_layer, *hidden = layer(layer_input)
        enc_layers.append(enc_layer)
        hidden_out.append(hidden)
    if return_all_layers:
        return (enc_layers, hidden_out)
    else:
        return (enc_layer, hidden_out)

def decode(dec_input, layers, attend=None, hidden_states=None):
    dec_layer = dec_input
    for n, layer in enumerate(layers):
        if attend and len(attend) > n:
            layer_input = [dec_layer, attend[n]]
        elif hidden_states:
            layer_input = [dec_layer] + hidden_states[n]
        else:
            layer_input = dec_layer
        dec_layer = layer(layer_input)
        if isinstance(dec_layer, (tuple, list)):
            dec_layer = dec_layer[0]
    return dec_layer

def encoder(hidden_dim, enc_input, hidden_states=None, **kwargs):
    layers = build_encoder_layers(hidden_dim, **kwargs)
    return encode(enc_input, layers, hidden_states=hidden_states)

def decoder(hidden_dim, dec_input, hidden_states=None, **kwargs):
    layers = build_decoder_layers(hidden_dim, **kwargs)
    return decode(dec_input, layers, hidden_states=hidden_states)

def encdec(hidden_dim, enc_input, dec_input, bi_enc=False,
           recurrent_unit="LSTM", with_attention=False,
           start_hidden=None,
           **kwargs):
    if with_attention:
        enc_layers = build_encoder_layers(hidden_dim, bidirectional=bi_enc,
                                          recurrent_unit=recurrent_unit,
                                          return_sequences=True, **kwargs)
        encoded, enc_hidden = encode(enc_input, enc_layers, hidden_states=start_hidden)
        dec_layers = build_decoder_layers(hidden_dim, recurrent_unit=recurrent_unit,
                                          with_attention=True, **kwargs)
        return decode(dec_input, dec_layers, attend=[encoded], hidden_states=None)
    else:
        enc_layers = build_encoder_layers(hidden_dim, recurrent_unit=recurrent_unit,
                                          bidirectional=bi_enc, **kwargs)
        _, enc_hidden = encode(enc_input, enc_layers, hidden_states=start_hidden)
        dec_layers = build_decoder_layers(hidden_dim, recurrent_unit=recurrent_unit,
                                          **kwargs)
        return decode(dec_input, dec_layers, hidden_states=enc_hidden)

##############################################################################
### Code above is deprecated; but more complex models still use it and     ###
### are harder to refactor, so we keep it for now.                         ###
##############################################################################

def make_convolutional_encoder(args, enc_inputs):
    enc_tensors = enc_inputs
    enc_hiddens = [[] for _ in range(len(enc_inputs))]

    convos = [[] for _ in range(len(enc_inputs))]
    for n in range(5):
        conv_layer = Conv1D(args.hidden // 5, n+1, border_mode='same', activation='tanh')
        for m, tensor in enumerate(enc_tensors):
            conv_vector = conv_layer(tensor)
            convos[m].append(conv_vector)
        #pool_vector = MaxPooling1D(pool_length=2)(conv_vector)
        #conv_vector = Conv1D(args.hidden, n+1, activation='tanh')(pool_vector)

        #pool_vector = GlobalMaxPooling1D()(conv_vector)
        #pool_vector = LSTM(args.hidden // 5)(pool_vector)
    enc_tensors = [merge(c, mode='concat', concat_axis=-1) for c in convos]
    #enc_tensor = BatchNormalization()(enc_tensor)

    #for n in range(args.depth):
    #    enc_tensor = Dense(args.hidden, activation='tanh')(enc_tensor)
    #    enc_hidden.append([enc_tensor])
    for m, c in enumerate(convos):
        pool_vector = merge([GlobalMaxPooling1D()(conv) for conv in c], mode='concat')
        enc_hiddens[m] = [pool_vector] * args.depth

    return enc_tensors, enc_hiddens

def make_recurrent_encoder(args, enc_inputs, start_hidden=None, use_layers=None):
    enc_tensors = enc_inputs
    enc_hiddens = [[] for _ in range(len(enc_inputs))]
    enc_layers = []
    if start_hidden:
        assert len(start_hidden) == len(enc_inputs)
        assert all(isinstance(h, (tuple, list)) for h in start_hidden)

    RNN = get_recurrent_unit(args.encoder)
    hidden_dim = args.hidden
    if args.bidir:
        bidirectional_mode = 'concat'
        hidden_dim //= 2

    if use_layers is not None:
        assert isinstance(use_layers, (tuple,list)) and len(use_layers) == args.depth

    for n in range(args.depth):
        enc_kwargs = {
            'return_sequences': (n<args.depth-1) or bool(args.att),
            'dropout_W': args.dropout,
            'dropout_U': args.dropout
            }
        if use_layers is not None:
            enc_layer = use_layers[n]
        else:
            enc_layer = RNN(hidden_dim, **enc_kwargs)
            if args.bidir:
                enc_layer = BidirectionalHidden(enc_layer, merge_mode=bidirectional_mode)
        enc_layers.append(enc_layer)
        new_tensors = []
        for m, tensor in enumerate(enc_tensors):
            if start_hidden:
                tensor = [tensor] + start_hidden[m][n]
            new_tensor, *hidden = enc_layer(tensor)
            new_tensors.append(new_tensor)
            enc_hiddens[m].append(hidden)
        enc_tensors = new_tensors

    return enc_tensors, enc_hiddens, enc_layers

def make_recurrent_decoder(args, dec_inputs, enc_tensors, enc_hiddens):
    dec_tensors = dec_inputs
    for n in range(args.depth):
        dec_kwargs = {
            'return_sequences': True,
            'dropout_W': args.dropout,
            'dropout_U': args.dropout
            }
        if args.att is not None and n == 0:
            AttRNN = get_attention_unit(args.decoder)
            dec_layer = AttRNN(args.hidden, aligner=args.att, **dec_kwargs)
        else:
            RNN = get_recurrent_unit(args.decoder)
            dec_layer = RNN(args.hidden, **dec_kwargs)
        new_tensors = []
        for dt, et, eh in zip(dec_tensors, enc_tensors, enc_hiddens):
            if args.att is not None and n == 0:
                tensor = [dt, et]
            else:
                tensor = [dt] + eh[n]
            tensor = dec_layer(tensor)
            if isinstance(tensor, (tuple, list)):
                tensor = tensor[0]
            new_tensors.append(tensor)
        dec_tensors = new_tensors

    return dec_tensors

def encdec2(args, enc_input, dec_input, start_hidden=None):
    # encoder
    if args.encoder == "CNN":
        enc_tensors, enc_hiddens = make_convolutional_encoder(args, [enc_input])
        # Does this even make a difference...?
        enc_tensors = [MaskingFrom()([et, enc_input]) for et in enc_tensors]

        if False: # dual LSTM and CNN encoding ...
            from mblearn.layers import HighwayMerge
            args.encoder = "LSTM"
            r_enc_tensor, r_enc_hidden, _ = make_recurrent_encoder(args, enc_input)
            new_enc_hidden = []
            for ((e_v,), (e_h, e_c)) in zip(enc_hidden, r_enc_hidden):
                h1 = HighwayMerge()([e_h, e_v])
                h2 = HighwayMerge()([e_c, e_v])
                new_enc_hidden.append([h1, h2])
            enc_hidden = new_enc_hidden

    elif args.encoder == "Dense":
        enc_tensor, enc_hiddens = enc_input, []
        for n in range(args.depth):
            enc_tensor = Dense(args.hidden, activation='tanh')(enc_tensor)
            enc_hiddens.append([enc_tensor])
        enc_tensors = [MaskingFrom()([enc_tensor, enc_input])]

    else:
        start_hidden = [start_hidden] if start_hidden else None
        enc_tensors, enc_hiddens, _ = make_recurrent_encoder(
            args, [enc_input], start_hidden=start_hidden
            )

    # decoder
    dec_tensors = make_recurrent_decoder(args, [dec_input], enc_tensors, enc_hiddens)

    return dec_tensors[0]
