#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import numpy as np
import sys

from boltons.iterutils import split as bsplit

from norm_common import get_data_from_file, train_model, \
     save_model, load_model, save_history, \
     add_common_train_params, add_common_eval_params, \
     filter_unknowns
from norm_vocab import Vocab

def Wacc(*args):
    from mblearn.metrics import sparse_ca_per_sequence_drop_mask
    return sparse_ca_per_sequence_drop_mask(*args)

def convert_to_xy(data, batch_size=None):
    X = list(data.sentences(0))
    y = list(data.sentences(1))
    if batch_size is not None:
        batch_cutoff = len(X) - (len(X) % batch_size)
        X, y = X[:batch_cutoff], y[:batch_cutoff]
    return (X, y)

def count_correct(preds, truths):
    correct = 0
    EPS_LOWER = "__eps__"
    for pred, truth in zip(preds, truths):
        p_word = ''.join(p for p in pred if p.lower() != EPS_LOWER)
        t_word = ''.join(t for t in truth if t.lower() != EPS_LOWER)
        correct += (p_word == t_word)
    return correct

def count_correct_charwise(preds, truths):
    correct, total = 0, 0
    for pred, truth in zip(preds, truths):
        correct += sum(p == t for (p, t) in zip(pred, truth))
        total   += max((len(pred), len(truth)))
    return correct, total

def get_recurrent_unit(unit):
    from keras.layers import LSTM, GRU, SimpleRNN
    from mblearn.layers import RAN
    if unit == "LSTM":
        return LSTM
    elif unit == "GRU":
        return GRU
    elif unit == "SimpleRNN":
        return SimpleRNN
    elif unit == "RAN":
        return RAN
    raise NotImplementedError("Unknown unit: {}".format(unit))

def build_model(args, vec_in, vec_out, max_len):
    from keras.layers import \
     Input, Embedding, Dense, TimeDistributed, Activation, Bidirectional
    from keras.optimizers import Adam, SGD
    from keras.models import Model
    from mblearn.models.wrappers import CategoricalModelWrapper

    ### build model
    input_ = Input(shape=(max_len,), dtype='int32', name='model_input')
    embed  = Embedding(len(vec_in), args.embedding, mask_zero=True)(input_)

    if args.ensembles > 1:
        raise NotImplementedError()

    RU = get_recurrent_unit(args.recurrent_unit)
    layer = embed
    hidden_dim = args.hidden if not args.bidir else (args.hidden // 2)
    for _ in range(args.depth):
        RecurrentLayer = RU(hidden_dim, dropout_W=args.dropout, return_sequences=True)
        if args.bidir:
            RecurrentLayer = Bidirectional(RecurrentLayer)
        layer = RecurrentLayer(layer)

    ### prediction layer
    layer = TimeDistributed(Dense(len(vec_out)))(layer)
    output = Activation('softmax', name='model_output')(layer)

    ### build model
    model = Model(input=[input_], output=[output])
    if args.optimizer == "adam":
        optimizer = Adam(lr=args.learning_rate, clipnorm=1.)
    elif args.optimizer == "sgd":
        optimizer = SGD(lr=args.learning_rate, momentum=args.momentum,
                        decay=args.decay, nesterov=(args.momentum > 0),
                        clipnorm=1.)
    else:
        optimizer = args.optimizer # doesn't use learning rate or anything...
    model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer=optimizer,
        metrics={'model_output': Wacc}
    )
    wrapper = CategoricalModelWrapper(
        model=model,
        input_vectorizer=[vec_in],
        output_vectorizer=[vec_out]
    )
    wrapper.input_padding = 'right'
    return wrapper

def evaluate_model(wrapper, args, infile=None):
    def print_accuracy(name, preds, truth):
        Cc, Ct = count_correct_charwise(preds, truth)
        print(name)
        print("  Char-level accuracy:  {0}/{1} = {2}".format(Cc, Ct, Cc/Ct))
        correct = count_correct(preds, truth)
        print("  Word-level accuracy:  {0}/{1} = {2}".format(correct, len(preds),
                                                             correct / len(preds)))

    if infile is None:
        infile = args.infile
    sys.stderr.write("Evaluating on: {0}\n".format(infile.name))
    data = get_data_from_file(infile)
    sys.stderr.write("Found {} words.\n".format(data.sentence_count))
    X, y = convert_to_xy(data)
    preds = wrapper.predict_classes([X], batch_size=args.batch, verbose=args.verbose)[0]
    # mask is not considered...
    preds = [p[:len(x)] for p, x in zip(preds, X)]
    if hasattr(args, 'print') and args.print:
        for p_sent in preds:
            print('\n'.join(p_sent))
        print('')
    else:
        print_accuracy("SeqLabelling", preds, y)
        traindata = None
        if args.command == "train":
            args.infile.seek(0)
            traindata = get_data_from_file(args.infile)
        elif args.trainfile:
            traindata = get_data_from_file(args.trainfile)
        if traindata is not None:
            train_X, train_y = convert_to_xy(traindata)
            vocab = Vocab(train_X, train_y)
            # Greedy mapper
            preds_map = vocab.predict_with_map(X, preds, min_count=0, max_ambiguity=1.0)
            print_accuracy("Mapper (greedy: c>=0, a<=1) + SeqLabelling", preds_map, y)
            # Semi-greedy mapper
            preds_map = vocab.predict_with_map(X, preds, min_count=0, max_ambiguity=0.75)
            print_accuracy("Mapper (semi-g: c>=0, a<=0.75) + SeqLabelling", preds_map, y)
            # Strict mapper
            preds_map = vocab.predict_with_map(X, preds, min_count=0, max_ambiguity=0.0)
            print_accuracy("Mapper (strict: c>=0, a==0) + SeqLabelling", preds_map, y)

def train(args):
    from mblearn.utils import Vectorizer
    from keras.callbacks import EarlyStopping, ModelCheckpoint

    data = get_data_from_file(args.infile, cutoff=(args.max_input, args.max_input))
    vec_in = Vectorizer(unk_symbol="<UNK>")
    vec_in.update(data.words(0))
    vec_out = Vectorizer(unk_symbol="<UNK>")
    vec_out.update(data.words(1))

    if args.pretrain:
        pretrain_data = get_data_from_file(args.pretrain,
                                           cutoff=(args.max_input, args.max_input))
        vec_in.update(pretrain_data.words(0))
        vec_out.update(pretrain_data.words(1))

    wrapper = build_model(
        args, vec_in, vec_out, args.max_input
    )
    params = {'validation_split': 0.0}

    if args.pretrain:
        pre_X, pre_y = convert_to_xy(pretrain_data)
        epochs, args.epochs = args.epochs, args.pretrain_epochs
        train_model(wrapper, [pre_X], [pre_y], args, **params)
        args.epochs = epochs

    X, y = convert_to_xy(data)
    if args.validate:
        v_data = get_data_from_file(args.validate)
        X_v, y_v = convert_to_xy(v_data)
        params['validation_data'] = ([X_v], [y_v])
    else:
        params['validation_split'] = args.validation_split

    if args.early_stopping:
        min_delta = 0.001
        # patience between [1, 10], depending on training set size
        patience = max(min(round(10000 * (1 / len(X))), 10), 1)
        callbacks = [EarlyStopping(monitor='val_loss', min_delta=min_delta,
                                   patience=patience, mode='min',
                                   verbose=args.verbose)]
        if args.prefix:
            filepath = args.prefix + '.h5'
            callbacks.append(ModelCheckpoint(filepath, monitor='val_Wacc',
                                             mode='max', period=1,
                                             save_best_only=True,
                                             save_weights_only=True,
                                             verbose=args.verbose))
        params['callbacks'] = callbacks

    history = train_model(wrapper, [X], [y], args, **params)

    if args.prefix:
        save_model(wrapper, args.prefix, skip_weights=args.early_stopping)
        save_history(history, args.prefix)

    if args.eval is not None:
        for evalfile in args.eval:
            evaluate_model(wrapper, args, infile=evalfile)

def evaluate(args):
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer

    wrapper = load_model(args.prefix)
    wrapper.model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer="adam",
        metrics={'model_output': Wacc}
    )
    evaluate_model(wrapper, args)

def embed(args):
    from keras import backend as K
    from keras.models import Model
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer
    from mblearn.serialization import dump, load

    from pprint import pprint

    wrapper = load_model(args.prefix)
    model = wrapper.model

    embedding = model.get_layer(name="embedding_1")
    embeds, *_ = embedding.get_weights()
    for n in range(embeds.shape[0]):
        char = wrapper.input_vectorizer[0].get_item(n)
        weights = [str(w) for w in embeds[n]]
        print("\t".join((char, ",".join(weights))))


if __name__ == "__main__":
    description = "Normalization as sequence labelling, using neural architectures."
    epilog = ""
    parser = argparse.ArgumentParser(
        description=description,
        epilog=epilog,
        fromfile_prefix_chars='@'
        )
    subparsers = parser.add_subparsers(title="available commands", dest='command')

    p_train = subparsers.add_parser('train')
    p_train.set_defaults(func=train,
                         help="Train a model")
    add_common_train_params(p_train)
    p_train.add_argument('-u', '--recurrent-unit',
                         dest="recurrent_unit",
                         choices=('LSTM','GRU','SimpleRNN','RAN'),
                         default='LSTM',
                         help='Recurrent unit to use (default: %(default)s)')
    p_train.add_argument('--bidir',
                         action='store_true',
                         default=False,
                         help='Use bi-directional encoders')
    p_train.add_argument('--early-stopping',
                         action='store_true',
                         default=False,
                         help=('Stop training early if validation accuracy '
                               'no longer improves; with --prefix, the best '
                               'epoch is saved instead of the last one'))
    p_train.add_argument('--ensembles',
                         metavar='N',
                         type=int,
                         default=1,
                         help='Train an ensemble of N enc/decs')
    p_train.add_argument('--pretrain',
                         metavar='INPUT',
                         type=argparse.FileType('r', encoding="UTF-8"),
                         help='File for pretraining the model')
    p_train.add_argument('--pretrain-epochs',
                         metavar='N',
                         type=int,
                         default=0,
                         help=('Number of epochs for pretraining (default: '
                               'same as --epochs)'))

    p_eval = subparsers.add_parser('eval')
    p_eval.set_defaults(func=evaluate,
                        help="Evaluate a model")
    add_common_eval_params(p_eval)
    p_eval.add_argument('-w', '--width',
                        type=int,
                        default=5,
                        help='Beam width when using "--decoder beam" (default: %(default)i)')
    p_eval.add_argument('--sentence-input',
                        action='store_true',
                        default=False,
                        help='Use full sentences as input, not words')
    p_eval.add_argument('--print',
                        action='store_true',
                        default=False,
                        help='Print predictions instead of scores')
    p_eval.add_argument('--trainfile',
                        metavar='FILE',
                        type=argparse.FileType('r', encoding="UTF-8"),
                        default=None,
                        help='File the model was trained on; used for extra evaluations')

    p_emb = subparsers.add_parser('embed')
    p_emb.set_defaults(func=embed,
                       help="Extract embeddings")
    p_emb.add_argument('-s', '--seed',
                       metavar='N',
                       type=int,
                       default=0,
                       help='Seed for RNG (default: 0=none)')
    p_emb.add_argument('-x', '--prefix',
                       metavar='STR',
                       type=str,
                       required=True,
                       help='Prefix for loading the model (required)')

    args = parser.parse_args()
    if args.seed > 0:
        np.random.seed(args.seed)
    if args.command == "eval" and args.print:
        args.verbose = 0
    if args.command == "train":
        if args.pretrain_epochs == 0:
            args.pretrain_epochs = args.epochs
        if args.early_stopping and args.validation_split <= 0 \
            and not args.validate:
            parser.error("--early-stopping requires validation data")
        if (args.momentum > 0 or args.decay > 0) \
            and args.optimizer != "sgd":
            parser.error("--momentum/--decay only work with --optimizer sgd")

    # accounting for the __EPS__ symbol
    args.max_input += 1

    args.func(args)
