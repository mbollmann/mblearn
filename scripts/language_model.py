#!/usr/bin/env python3

import argparse
import numpy as np
from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.embeddings import Embedding
from keras.layers.recurrent import LSTM
from keras.optimizers import Adam
import sys

from mblearn.data import TextData
from mblearn.models.lm import LanguageModel, CharAwareLM
from mblearn.utils import Vectorizer
from norm_common import save_model, load_model, save_history

def generate_chunks(text, vectorizer, size=128, length=10, step=1, shuffle=True):
    chunks = list(range(length, len(text), step))
    max_idx = len(chunks) - (len(chunks) % size)
    idx = 0
    vec_len = len(vectorizer)
    while True:
        if idx == 0 and shuffle:
            import random
            random.shuffle(chunks)
        X = np.zeros((size, length, vec_len), dtype=np.bool)
        y = np.zeros((size, vec_len), dtype=np.bool)
        for batch_idx, chunk_idx in enumerate(chunks[idx:(idx + size)]):
            X[batch_idx] = vectorizer.map_to_onehot(text[(chunk_idx - length):chunk_idx])
            y[batch_idx] = vectorizer.get_onehot(text[chunk_idx])
        yield (X, y)
        idx = (idx + size) % max_idx

def read_data(source):
    data = TextData()
    data.read_vertical(source)
    data.replace_classes(digits="N", nonword="X", mixed_digits="M")
    return data


def train(args):
    batch_size = args.batch
    sequence_length = args.seqlen
    total_iters = args.epochs

    data = read_data(args.infile.read())
    sys.stderr.write("   Training: Read {0} sentences with {1} tokens of {2} types.\n".format(
        len(tuple(data.sentences)), len(tuple(data.words)), len(data.types)
    ))

    if args.validate:
        data_valid = read_data(args.validate.read())
        sys.stderr.write(" Validation: Read {0} sentences with {1} tokens of {2} types.\n".format(
            len(tuple(data_valid.sentences)), len(tuple(data_valid.words)), len(data_valid.types)
        ))
    else:
        data_valid = None

    sys.stderr.write("Training the model...\n")
    LM = CharAwareLM if args.type == "full" else LanguageModel
    model = LM(
        context_len=sequence_length,
        embedding_dim=args.embedding,
        hidden_dim=args.hidden,
        depth=args.depth,
        dropout=args.dropout,
        optimizer=Adam(lr=0.005, clipnorm=5.)
    )
    history = model.train(data, validation_data=data_valid, frequency_cutoff=None,
                          model_type=args.type, batch_size=batch_size,
                          nb_epoch=total_iters, verbose=1)

    #for diversity in [0, 0.2, 0.5, 1.0, 1.2]:
    #    print('----- diversity:', diversity)
    #    sentence = ["<S>", "a", "b"]
    #    print('----- Generating with seed: "' + ' '.join(sentence) + '"')
    #    gen = model.generate(seed=sentence, length=50, diversity=diversity)
    #    sys.stdout.write(' '.join(gen))
    #    sys.stdout.write('\n')

    if args.prefix:
        save_model(model, args.prefix)
        save_history(history, args.prefix)

    sys.stderr.write("Done.\n")


def generate(args):
    model = load_model(args.prefix)

    if args.seed is not None:
        seed = ["<S>"] + [c for c in args.seed]
    else:
        seed = None

    def replace_special(char):
        if char in ("_", "<PAD>"):
            return " "
        if char == "<UNK>":
            return "?"
        return char

    for _ in range(args.num):
        gen = model.generate(
            seed=seed,
            length=args.max_length,
            diversity=args.diversity
        )[1:]
        if gen[-1] == "</S>":
            gen = gen[:-1]
        sentence = ''.join(replace_special(char) for char in gen).rstrip()
        print(sentence)


def evaluate(args):
    model = load_model(args.prefix)
    data = read_data(args.infile.read())
    sys.stderr.write(" Evaluation: Read {0} sentences with {1} tokens of {2} types.\n".format(
        len(tuple(data.sentences)), len(tuple(data.words)), len(data.types)
    ))

    loss, perplexity = model.evaluate(data, batch_size=args.batch)
    print()
    print("Average loss: {:2.4f}".format(loss))
    print("  Perplexity: {:2.4f}".format(perplexity))
    print()


if __name__ == '__main__':
    description = ""
    epilog = ""
    mainparser = argparse.ArgumentParser(description=description, epilog=epilog)
    subparsers = mainparser.add_subparsers(title="available commands", dest='command')

    parser = subparsers.add_parser('train')
    parser.set_defaults(func=train,
                        help="Train a model")
    parser.add_argument('infile',
                        metavar='INPUT',
                        type=argparse.FileType('r', encoding="utf-8"),
                        help='Input file')
    parser.add_argument('-b', '--batch',
                        type=int,
                        default=512,
                        help='Batch size (default: %(default)i)')
    parser.add_argument('-d', '--depth',
                        type=int,
                        default=2,
                        help='Depth of network (default: %(default)i)')
    parser.add_argument('-e', '--epochs',
                        type=int,
                        default=10,
                        help='Train for this amount of epochs (default: %(default)i)')
    parser.add_argument('-i', '--hidden',
                        type=int,
                        default=128,
                        help='Dimensionality of hidden layers (default: %(default)i)')
    parser.add_argument('-l', '--seqlen',
                        type=int,
                        default=25,
                        help='Length of context window (default: %(default)i)')
    parser.add_argument('-m', '--embedding',
                        type=int,
                        default=64,
                        help='Dimensionality of embeddings (default: %(default)i)')
    parser.add_argument('-r', '--dropout',
                        metavar='D',
                        type=float,
                        default=0.0,
                        help='Dropout (default: %(default)f)')
    parser.add_argument('-t', '--type',
                        choices=('cnn', 'lstm', 'full'),
                        default='cnn',
                        help='Type of LM to train (default: %(default)s)')
    parser.add_argument('-x', '--prefix',
                        metavar='PREFIX',
                        type=str,
                        help='Filename prefix for saving the model')
    parser.add_argument('--validate',
                        metavar='INPUT',
                        required=False,
                        type=argparse.FileType('r', encoding="utf-8"),
                        help='Validation file')

    parser = subparsers.add_parser('sample')
    parser.set_defaults(func=generate,
                        help="Sample from a model")
    parser.add_argument('-d', '--diversity',
                        type=float,
                        default=0.2,
                        help='Diversity of the output (default: %(default)f)')
    parser.add_argument('-l', '--max-length',
                        type=int,
                        default=100,
                        help='Maximum sentence length (default: %(default)i)')
    parser.add_argument('-n', '--num',
                        type=int,
                        default=10,
                        help='Number of sentences to sample (default: %(default)i)')
    parser.add_argument('-s', '--seed',
                        type=str,
                        default=None,
                        help='Starting seed for each sentence (default: None)')
    parser.add_argument('-x', '--prefix',
                        metavar='PREFIX',
                        type=str,
                        required=True,
                        help='Filename prefix for loading the model')

    parser = subparsers.add_parser('evaluate')
    parser.set_defaults(func=evaluate,
                        help="Evaluate a model")
    parser.add_argument('infile',
                        metavar='INPUT',
                        type=argparse.FileType('r', encoding="utf-8"),
                        help='Input file')
    parser.add_argument('-b', '--batch',
                        type=int,
                        default=512,
                        help='Batch size (default: %(default)i)')
    parser.add_argument('-x', '--prefix',
                        metavar='PREFIX',
                        type=str,
                        help='Filename prefix for saving the model')

    args = mainparser.parse_args()
    args.func(args)
