#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import numpy as np
import sys

from norm_common import get_data_from_file, train_model, \
     save_model, load_model, save_history, \
     add_common_train_params, add_common_eval_params

def Wacc(*args):
    from mblearn.metrics import sparse_ca_per_sequence_drop_mask
    return sparse_ca_per_sequence_drop_mask(*args)

def decorate_words(wordlist):
    # for an easy way to distinguish between historical and modern characters,
    # prefix one of those with an underscore
    return tuple(['_' + c for c in word] for word in wordlist)

def append_end_symbol(wordlist, symbol='<END>'):
    return tuple([c for c in word] + [symbol] for word in wordlist)

def convert_to_xy(data, batch_size=None):
    X, y = decorate_words(data.words(0)), append_end_symbol(data.words(1))
    if batch_size is not None:
        batch_cutoff = len(X) - (len(X) % batch_size)
        X, y = X[:batch_cutoff], y[:batch_cutoff]
    return (X, y)

def evaluate_model(wrapper, args, infile=None):
    from norm_advanced_seq2seq import count_correct, count_correct_charwise
    if infile is None:
        infile = args.infile
    print("Evaluating on: {0}".format(infile.name))
    data = get_data_from_file(infile)
    X, y = convert_to_xy(data, batch_size=args.batch)
    preds = wrapper.predict_classes(X, batch_size=args.batch, verbose=args.verbose)
    Cc, Ct = count_correct_charwise(preds, y)
    print("  Char-level accuracy:  {0}/{1} = {2}".format(Cc, Ct, Cc/Ct))
    correct = count_correct(preds, y)
    print("  Word-level accuracy:  {0}/{1} = {2}".format(correct, len(preds), correct / len(preds)))

def train(args):
    from keras.layers import Activation, Embedding, Dense, Dropout
    from keras.layers.wrappers import TimeDistributed

    from mblearn.models.seq2seq import Seq2seq, get_recurrent_unit
    from mblearn.models.decoders import GreedySequentialDecoder
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer

    data = get_data_from_file(args.infile, cutoff=(args.max_input, args.max_output))
    vectorizer = Vectorizer(unk_symbol="<UNK>")

    vectorizer.update(('_' + c for c in set(data.characters(0))))
    vectorizer.update(data.characters(1))
    vectorizer.update(('<START>', '<END>'))
    RU = get_recurrent_unit(args.recurrent_unit)
    model = Seq2seq(
        (args.max_input, args.max_output),
        vectorizer['<START>'],
        [vectorizer['<END>']],
        [0])
    model.add(Embedding(len(vectorizer), args.embedding,
                        input_length=(args.max_input + args.max_output),
                        mask_zero=True))
    for _ in range(args.depth):
        model.add(RU(args.hidden, return_sequences=True))
        model.add(Dropout(args.dropout))
    model.add(TimeDistributed(Dense(len(vectorizer))))
    model.add(Activation('softmax'))
    model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer=args.optimizer,
        metrics=['accuracy', Wacc]
    )
    wrapper = CategoricalModelWrapper(model=model, vectorizer=vectorizer)

    X, y = convert_to_xy(data)
    if args.validate:
        v_data = get_data_from_file(args.validate)
        params = {'validation_data': convert_to_xy(v_data)}
    else:
        params = {'validation_split': args.validation_split}
    history = train_model(wrapper, X, y, args,
                          validation_split=args.validation_split)

    if args.prefix:
        save_model(wrapper, args.prefix)
        save_history(history, args.prefix)

    if args.eval is not None:
        wrapper.model.decoder = GreedySequentialDecoder()
        for evalfile in args.eval:
            evaluate_model(wrapper, args, infile=evalfile)

def evaluate(args):
    from mblearn.models.seq2seq import Seq2seq
    from mblearn.models.decoders import GreedySequentialDecoder
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer

    wrapper = load_model(args.prefix)
    wrapper.model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer="adam",
        metrics=['accuracy', Wacc]
    )
    if args.decoder == 'greedy':
        wrapper.model.decoder = GreedySequentialDecoder()
    evaluate_model(wrapper, args)


if __name__ == "__main__":
    description = "Normalization using neural network architectures."
    epilog = ""
    parser = argparse.ArgumentParser(
        description=description,
        epilog=epilog,
        fromfile_prefix_chars='@'
        )
    subparsers = parser.add_subparsers(title="available commands")

    p_train = subparsers.add_parser('train')
    p_train.set_defaults(func=train,
                         help="Train a model")
    add_common_train_params(p_train)
    p_train.add_argument('-u', '--recurrent-unit',
                         dest="recurrent_unit",
                         choices=('LSTM','GRU'),
                         default='LSTM',
                         help='Recurrent unit to use (default: %(default)s)')

    p_eval = subparsers.add_parser('eval')
    p_eval.set_defaults(func=evaluate,
                        help="Evaluate a model")
    add_common_eval_params(p_eval)
    p_eval.add_argument('-d', '--decoder',
                        choices=('greedy',),
                        default='greedy',
                        help='Decoder to use (default: %(default)s)')

    args = parser.parse_args()
    if args.seed > 0:
        np.random.seed(args.seed)
    args.func(args)
