#!/usr/bin/python3
# -*- coding: utf-8 -*-

from keras.layers import *
from keras.models import Model
from mblearn.layers.recurrent import HiddenStateLSTM

### build encoder
enc_input = Input(shape=(24,), dtype='int32', name='encoder_input')
enc_layer = Embedding(128, 64, mask_zero=True)(enc_input)
enc_layer, *hidden = HiddenStateLSTM(64, dropout_W=0.5, return_sequences=False)(enc_layer)

### build decoder
dec_input = Input(shape=(24,), dtype='int32', name='decoder_input')
dec_layer = Embedding(128, 64, mask_zero=True)(dec_input)
dec_layer, _, _ = HiddenStateLSTM(64, dropout_W=0.5, return_sequences=True)([dec_layer] + hidden)
dec_layer = TimeDistributed(Dense(128))(dec_layer)
dec_output = Activation('softmax', name='decoder_output')(dec_layer)

### build model
model = Model(input=[enc_input, dec_input], output=dec_output)
model.compile(optimizer='adam', loss='categorical_crossentropy')
