
from camel import Camel
import argparse
import numpy as np
import matplotlib
matplotlib.use('Agg')
import seaborn as sns
from sklearn.preprocessing import normalize
import sys

camel = Camel([])

def prepare_plot(x, y, grads):
    # prepare labels
    xlabels, ylabels = [''] * 28, [''] * 14
    xlabels[14-len(x):14] = list(x)
    xlabels[14] = "<start>"
    xlabels[15:15+len(y)] = list(y)
    ylabels[0:len(y)] = list(y)
    ylabels[len(y)] = "<end>"
    # prepare dataset
    grads = np.fabs(grads)
    # return everything transposed
    return (ylabels, xlabels, grads.T)

def prepare_attlike(x, y, grads):
    # prepare labels
    xlabels = list(y) + ['<END>']
    ylabels = list(x)[::-1]
    # prepare dataset
    grads = np.fabs(grads)
    grads = grads[0:len(y)+1, (14-len(x)):14]
    grads = np.flipud(grads.T)
    grads = normalize(grads, axis=1, norm='l1')
    return (xlabels, ylabels, grads)

def main(args):
    gradients = camel.load(args.infile.read())
    prepare = prepare_plot if not args.attlike else prepare_attlike

    for i, entry in enumerate(gradients):
        x, y, grads = entry['x'], entry['y'], entry['grads']
        xlabels, ylabels, grads = prepare(x, y, grads)
        # plot
        ax = sns.heatmap(grads,
                         xticklabels=xlabels, yticklabels=ylabels,
                         square=True, robust=True, cbar=False)
        #ax.invert_yaxis()
        fig = ax.get_figure()
        if args.prefix:
            fig.savefig("{}_{}.png".format(args.prefix, i))
        else:
            fig.show()


if __name__ == "__main__":
    description = "Plot saliency of timesteps."
    epilog = ""
    parser = argparse.ArgumentParser(description=description, epilog=epilog)

    parser.add_argument('infile',
                        metavar='INFILE',
                        type=argparse.FileType('r', encoding="UTF-8"),
                        default=sys.stdin,
                        help='Gradients as .yaml')
    parser.add_argument('-p', '--prefix',
                        metavar='PREFIX',
                        type=str,
                        help='Output plots to files <PREFIX>_<NUM>.png')
    parser.add_argument('-a', '--attlike',
                        action='store_true',
                        default=False,
                        help='Output plot similar to attention weights')

    args = parser.parse_args()
    main(args)
