import numpy as np
from keras import backend as K
from keras.layers import *
from mblearn.layers import *
from theano import tensor as T

from encdec import make_recurrent_encoder

np.random.seed(0)

def build_model():
    class Args:
        embedding = 48
        hidden = 192
        att = "global"
        bidir = True
        depth = 1
        dropout = 0.0
        encoder = "LSTM"
        decoder = "LSTM"

    args = Args()
    enc_len, dec_len = 22, 22
    CONTEXT_SIZE = 3
    len_enc_vec, len_dec_vec = 17, 17

    ### build inputs
    enc_input = Input(shape=(enc_len,), dtype='int32', name='encoder_input')
    dec_input = Input(shape=(dec_len,), dtype='int32', name='decoder_input')
    ctx_input = Input(shape=(CONTEXT_SIZE, enc_len), dtype='int32', name='context_input')

    samples = 50
    enc_sample = np.random.randint(0, len_enc_vec, (samples, enc_len))
    dec_sample = np.random.randint(0, len_dec_vec, (samples, dec_len))
    ctx_sample = np.random.randint(0, len_enc_vec, (samples, CONTEXT_SIZE, enc_len))

    enc_embed_layer = Embedding(len_enc_vec, args.embedding, mask_zero=True)
    dec_embed_layer = Embedding(len_dec_vec, args.embedding, mask_zero=True)

    enc_embed = enc_embed_layer(enc_input)
    dec_embed = dec_embed_layer(dec_input)
    ctx_embed = TimeDistributed(enc_embed_layer)(ctx_input)

    enc_tensors, enc_hiddens, enc_layers = make_recurrent_encoder(args, [enc_embed])
    ctx_layers = [TimeDistributedMulti(layer) for layer in enc_layers]
    ctx_tensors, ctx_hiddens, _ = make_recurrent_encoder(args, [ctx_embed], use_layers=ctx_layers)

    if args.att:
        if args.bidir:
            co = args.hidden // 2
            last_timestep = Lambda(lambda x: K.concatenate([x[:, :, -1, :co], x[:, :, 0, co:]], axis=-1),
                                   output_shape=(CONTEXT_SIZE, args.hidden))
        else:
            last_timestep = Lambda(lambda x: x[:, :, -1, :], output_shape=(CONTEXT_SIZE, args.hidden))
        ctx_tensors = [last_timestep(t) for t in ctx_tensors]

    #ctx_tensor = GlobalAveragePooling1D()(ctx_tensors[0])
    #ctx_tensor = LSTM(41, return_sequences=False)(ctx_tensors[0])
    return_ = ctx_tensors[0]

    return ((enc_input, dec_input, ctx_input),
            [enc_sample, dec_sample, ctx_sample],
            return_)


inputs, sample_inputs, outputs = build_model()

from keras.models import Model
model = Model(input=inputs, output=outputs)
model.compile(loss="categorical_crossentropy", optimizer="adam")

output_ = model.predict(sample_inputs)
print(type(output_))
print(output_.shape)
print(output_[0, :, :3])
print(output_[0, :, -3:])


exit()
fn = K.function(inputs=inputs, outputs=outputs)
output_ = fn(sample_inputs)

print("=== Input shapes:")
for in_ in sample_inputs:
    print(np.shape(in_))
print("=== Output shapes:")
if isinstance(output_, (tuple, list)):
    for out_ in output_:
        print(T.shape(out_).eval())
else:
    print(T.shape(output_).eval())
