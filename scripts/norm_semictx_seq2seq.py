#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import numpy as np
import sys

from norm_common import get_data_from_file, train_model, \
     save_model, load_model, save_history, \
     add_common_train_params, add_common_eval_params, \
     filter_unknowns
from norm_vocab import Vocab

def Wacc(*args):
    from mblearn.metrics import sparse_ca_per_sequence_drop_mask
    return sparse_ca_per_sequence_drop_mask(*args)

def append_end_symbol(wordlist, symbol='<END>'):
    return tuple([c for c in word] + [symbol] for word in wordlist)

def convert_to_xy(data, context_len,
                  batch_size=None,
                  for_decoder_input=False):
    X = list(data.words(0))
    Y = data.words(1)
    X_l, X_r = [], []
    for words in data.sentences(0):
        for i, word in enumerate(words):
            w_l = '_' + '_'.join(words[:i])
            w_r = '_' + '_'.join(words[:i:-1])
            if len(w_l) > context_len:
                w_l = w_l[-context_len:]
            if len(w_r) > context_len:
                w_r = w_r[-context_len:]
            X_l.append(w_l)
            X_r.append(w_r)
    assert len(X) == len(X_l)
    assert len(X) == len(X_r)
    if for_decoder_input:
        y = tuple([for_decoder_input] + [c for c in word] for word in Y)
    else:
        y = append_end_symbol(Y)
    if batch_size is not None:
        batch_cutoff = len(X) - (len(X) % batch_size)
        X, X_l, X_r = X[:batch_cutoff], X_l[:batch_cutoff], X_r[:batch_cutoff]
        y = y[:batch_cutoff]
    return ([X, None, X_l, X_r], y)

def count_correct(preds, truths):
    return sum(p == t for p, t in zip(preds, truths))

def count_correct_charwise(preds, truths):
    correct, total = 0, 0
    for pred, truth in zip(preds, truths):
        correct += sum(p == t for (p, t) in zip(pred, truth))
        total   += max((len(pred), len(truth)))
    return correct, total

def build_model(args, enc_vec, dec_vec, enc_len, dec_len, context_len):
    from keras.layers import Input, Embedding, Dense, TimeDistributed, Activation, LSTM, Reshape, Masking
    from keras.optimizers import Adam
    from mblearn.layers import merge
    from mblearn.models.seq2seq import Seq2seqModel
    from mblearn.models.wrappers import CategoricalModelWrapper
    from encdec import encdec2

    enc_embed_layer = Embedding(len(enc_vec), args.embedding, mask_zero=True)
    dec_embed_layer = Embedding(len(dec_vec), args.embedding, mask_zero=True)

    ### build inputs
    enc_input = Input(shape=(enc_len,), dtype='int32', name='encoder_input')
    enc_embed = enc_embed_layer(enc_input)
    dec_input = Input(shape=(dec_len,), dtype='int32', name='decoder_input')
    dec_embed = dec_embed_layer(dec_input)

    ### build context-providing RNNs
    left_input  = Input(shape=(context_len,), dtype='int32', name='left_ctx_input')
    left_vec  = enc_embed_layer(left_input)
    right_input = Input(shape=(context_len,), dtype='int32', name='right_ctx_input')
    right_vec = enc_embed_layer(right_input)
    for i in range(args.depth_context, 0, -1):
        left_vec  = LSTM(args.hidden // 2,
                         dropout_W=args.dropout_context,
                         return_sequences=(i > 1))(left_vec)
        right_vec = LSTM(args.hidden // 2,
                         dropout_W=args.dropout_context,
                         return_sequences=(i > 1))(right_vec)
    left_context  = left_vec
    right_context = right_vec
    if args.bidir:
        context_vector = [left_context, right_context]
    else:
        context_vector = [merge([left_context, right_context], mode='concat')]
    start_hidden = [context_vector] * args.depth
    #left_context = Masking()(Reshape((1, args.embedding))(left_context))
    #right_context = Masking()(Reshape((1, args.embedding))(right_context))
    #enc_embed = merge([left_context, enc_embed], mode='concat', concat_axis=1)
    #enc_embed = merge([enc_embed, right_context], mode='concat', concat_axis=1)
    #start_hidden = None

    ### build encoder/decoder
    if args.ensembles > 1:
        dec_layers = []
        for _ in range(args.ensembles):
            dec_layer = encdec2(args, enc_embed, dec_embed,
                                start_hidden=start_hidden)
            dec_layers.append(dec_layer)
        dec_layer = merge(dec_layers, mode='ave')
    else:
        dec_layer = encdec2(args, enc_embed, dec_embed,
                            start_hidden=start_hidden)

    ### prediction layer
    dec_layer = TimeDistributed(Dense(len(dec_vec)))(dec_layer)
    dec_output = Activation('softmax', name='decoder_output')(dec_layer)

    ### build model
    model = Seq2seqModel(
        input=[enc_input, dec_input, left_input, right_input],
        output=[dec_output],
        decoder_input=1, decoder_output=0,
        start_symbol=dec_vec['<START>'],
        end_symbol=[dec_vec['<END>']],
        mask_symbol=[0]
    )
    if args.optimizer == "adam":
        optimizer = Adam(lr=args.learning_rate, clipnorm=1.)
    elif args.optimizer == "sgd":
        optimizer = SGD(lr=args.learning_rate, momentum=args.momentum,
                        decay=args.decay, nesterov=(args.momentum > 0),
                        clipnorm=1.)
    else:
        optimizer = args.optimizer # doesn't use learning rate or anything...
    model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer=optimizer,
        metrics={'decoder_output': Wacc}
    )
    wrapper = CategoricalModelWrapper(
        model=model,
        input_vectorizer=[enc_vec, None, enc_vec, enc_vec],
        output_vectorizer=[dec_vec]
    )
    return wrapper

def evaluate_model(wrapper, args, infile=None):
    def print_accuracy(name, preds, truth):
        Cc, Ct = count_correct_charwise(preds, truth)
        print(name)
        print("  Char-level accuracy:  {0}/{1} = {2}".format(Cc, Ct, Cc/Ct))
        correct = count_correct(preds, truth)
        print("  Word-level accuracy:  {0}/{1} = {2}".format(correct, len(preds),
                                                             correct / len(preds)))

    if infile is None:
        infile = args.infile
    sys.stderr.write("Evaluating on: {0}\n".format(infile.name))
    data = get_data_from_file(infile)
    sys.stderr.write("Found {} words.\n".format(data.word_count))
    X, y = convert_to_xy(data, args.context_len)
    preds = wrapper.predict_classes(X, batch_size=args.batch, verbose=args.verbose)[0]
    X, y = convert_to_xy(data, args.context_len) # data is modified by wrapper somehow...
    if hasattr(args, 'print') and args.print:
        sentence_boundaries = set()
        offset = -1
        for s in data.sentences(0):
            offset += len(s)
            sentence_boundaries.add(offset)
        for i, p in enumerate(preds):
            assert p[-1] == "<END>"
            print(''.join(p[:-1]))
            if i in sentence_boundaries:
                print('')
    else:
        print_accuracy("Seq2seq", preds, y)
        traindata = None
        if args.command == "train":
            args.infile.seek(0)
            traindata = get_data_from_file(args.infile)
        elif args.trainfile:
            traindata = get_data_from_file(args.trainfile)
        if traindata is not None:
            train_X, train_y = convert_to_xy(traindata, args.context_len)
            vocab = Vocab(train_X[0], train_y)
            # Greedy mapper
            preds_map = vocab.predict_with_map(X[0], preds, min_count=0, max_ambiguity=1.0)
            print_accuracy("Mapper (greedy: c>=0, a<=1) + Seq2seq", preds_map, y)
            # Semi-greedy mapper
            preds_map = vocab.predict_with_map(X[0], preds, min_count=0, max_ambiguity=0.75)
            print_accuracy("Mapper (semi-g: c>=0, a<=0.75) + Seq2seq", preds_map, y)
            # Strict mapper
            preds_map = vocab.predict_with_map(X[0], preds, min_count=0, max_ambiguity=0.0)
            print_accuracy("Mapper (strict: c>=0, a==0) + Seq2seq", preds_map, y)

def train(args):
    from mblearn.utils import Vectorizer
    from mblearn.models.decoders import GreedyDecoder
    from keras.callbacks import EarlyStopping, ModelCheckpoint
    from mblearn.callbacks import ClearLine

    data = get_data_from_file(args.infile, cutoff=(args.max_input, args.max_output))
    enc_vectorizer = Vectorizer(unk_symbol="<UNK>")
    enc_vectorizer.update(data.characters(0))
    dec_vectorizer = Vectorizer(unk_symbol="<UNK>")
    dec_vectorizer.update(data.characters(1))
    dec_vectorizer.update(('<START>', '<END>'))

    if args.pretrain:
        pretrain_data = get_data_from_file(args.pretrain,
                                           cutoff=(args.max_input, args.max_output))
        enc_vectorizer.update(pretrain_data.characters(0))
        dec_vectorizer.update(pretrain_data.characters(1))

    wrapper = build_model(
        args, enc_vectorizer, dec_vectorizer,
        args.max_input, args.max_output,
        args.context_len
    )
    X, y = convert_to_xy(data, args.context_len)
    if args.validate:
        v_data = get_data_from_file(args.validate)
        X_v, y_v = convert_to_xy(v_data, args.context_len)
        params = {'validation_data': (X_v, [y_v])}
    else:
        params = {'validation_split': args.validation_split}

    if args.pretrain:
        pre_X, pre_y = convert_to_xy(pretrain_data, args.context_len)
        epochs, args.epochs = args.epochs, args.pretrain_epochs
        train_model(wrapper, pre_X, [pre_y], args, validation_split=0.0)
        args.epochs = epochs

    if args.early_stopping:
        callbacks = [ClearLine()] if args.verbose else []
        min_delta = 0.01
        # patience between [1, 10], depending on training set size
        patience = max(min(round(10000 * (1 / len(X))), 10), 1)
        callbacks.append(EarlyStopping(monitor='val_Wacc', min_delta=min_delta,
                                       patience=patience, mode='max',
                                       verbose=args.verbose))
        if args.prefix:
            filepath = args.prefix + '.h5'
            callbacks.append(ModelCheckpoint(filepath, monitor='val_Wacc',
                                             mode='max', period=1,
                                             save_best_only=True,
                                             save_weights_only=True,
                                             verbose=args.verbose))
        params['callbacks'] = callbacks

    history = train_model(wrapper, X, [y], args, **params)

    if args.prefix:
        save_model(wrapper, args.prefix, skip_weights=args.early_stopping)
        save_history(history, args.prefix)

    if args.eval is not None:
        wrapper.model.decoder = GreedyDecoder()
        for evalfile in args.eval:
            evaluate_model(wrapper, args, infile=evalfile)

def evaluate(args):
    from mblearn.layers import HiddenStateLSTM, MaskedMerge, merge, TimeCutoff
    from mblearn.models.seq2seq import Seq2seq
    from mblearn.models.decoders import GreedyDecoder, BeamSearchDecoder
    from mblearn.models.filters import DictionaryFilter
    from mblearn.models.wrappers import CategoricalModelWrapper
    from mblearn.utils import Vectorizer

    wrapper = load_model(args.prefix)
    wrapper.model.compile(
        loss="sparse_categorical_crossentropy",
        optimizer="adam",
        metrics={'decoder_output': Wacc}
    )
    if args.decoder == 'greedy':
        wrapper.model.decoder = GreedyDecoder()
    elif args.decoder == 'beam':
        wrapper.model.decoder = BeamSearchDecoder(beam_width=args.width)
    if args.filter:
        words = [w.strip() for w in args.filter.readlines()]
        vectorizer = wrapper.output_vectorizer[wrapper.model.decoder_output[0]]
        eos_idx = wrapper.model.end_symbol[0]
        sys.stderr.write("Building dictionary...")
        dictfilt = DictionaryFilter(words, vectorizer, eos_idx)
        sys.stderr.write(" done.\n")
        wrapper.model.decoder.append_filter(dictfilt)

    evaluate_model(wrapper, args)

if __name__ == "__main__":
    description = "Normalization using encoder-decoder architectures."
    epilog = ""
    parser = argparse.ArgumentParser(
        description=description,
        epilog=epilog,
        fromfile_prefix_chars='@'
        )
    subparsers = parser.add_subparsers(title="available commands", dest='command')

    p_train = subparsers.add_parser('train')
    p_train.set_defaults(func=train,
                         help="Train a model")
    add_common_train_params(p_train)
    p_train.add_argument('-u', '--recurrent-unit',
                         dest="recurrent_unit",
                         choices=('LSTM','GRU','SimpleRNN','htcLSTM'),
                         default='LSTM',
                         help='Recurrent unit to use (default: %(default)s)')
    p_train.add_argument('--encoder',
                         choices=('LSTM','GRU','SimpleRNN','htcLSTM','RAN','CNN'),
                         default='LSTM',
                         help='Type of encoder to use (default: %(default)s)')
    p_train.add_argument('--decoder',
                         choices=('LSTM','GRU','SimpleRNN','htcLSTM','RAN'),
                         default='LSTM',
                         help='Type of decoder to use (default: %(default)s)')
    p_train.add_argument('--att',
                         action='store_true',
                         default=False,
                         help='Use attentional decoders')
    p_train.add_argument('--bidir',
                         action='store_true',
                         default=False,
                         help='Use bi-directional encoders')
    p_train.add_argument('--early-stopping',
                         action='store_true',
                         default=False,
                         help=('Stop training early if validation accuracy '
                               'no longer improves; with --prefix, the best '
                               'epoch is saved instead of the last one'))
    p_train.add_argument('--ensembles',
                         metavar='N',
                         type=int,
                         default=1,
                         help='Train an ensemble of N enc/decs')
    p_train.add_argument('-L', '--context-len',
                         metavar='N',
                         type=int,
                         default=100,
                         help=('Maximum characters for context'))
    p_train.add_argument('-D', '--depth-context',
                         metavar='N',
                         type=int,
                         default=1,
                         help='Depth of the context RNN (default: %(default)i)')
    p_train.add_argument('-R', '--dropout-context',
                         metavar='D',
                         type=float,
                         default=0.0,
                         help='Dropout for the context RNN (default: %(default)f)')
    p_train.add_argument('--pretrain',
                         metavar='INPUT',
                         type=argparse.FileType('r', encoding="UTF-8"),
                         help='File for pretraining the model')
    p_train.add_argument('--pretrain-epochs',
                         metavar='N',
                         type=int,
                         default=0,
                         help=('Number of epochs for pretraining (default: '
                               'same as --epochs)'))

    p_eval = subparsers.add_parser('eval')
    p_eval.set_defaults(func=evaluate,
                        help="Evaluate a model")
    add_common_eval_params(p_eval)
    p_eval.add_argument('-d', '--decoder',
                        choices=('greedy','beam'),
                        default='greedy',
                        help='Decoder to use (default: %(default)s)')
    p_eval.add_argument('-w', '--width',
                        type=int,
                        default=5,
                        help='Beam width when using "--decoder beam" (default: %(default)i)')
    p_eval.add_argument('-f', '--filter',
                        metavar='FILE',
                        type=argparse.FileType('r', encoding="UTF-8"),
                        default=None,
                        help='File with words for a dictionary filter')
    p_eval.add_argument('--context-len',
                        metavar='N',
                        type=int,
                        default=100,
                        help=('Maximum characters for context'))
    p_eval.add_argument('--print',
                        action='store_true',
                        default=False,
                        help='Print predictions instead of scores')
    p_eval.add_argument('--trainfile',
                        metavar='FILE',
                        type=argparse.FileType('r', encoding="UTF-8"),
                        default=None,
                        help='File the model was trained on; used for extra evaluations')

    args = parser.parse_args()
    if args.seed > 0:
        np.random.seed(args.seed)
    if args.command == "eval" and args.print:
        args.verbose = 0
    if args.command == "train" and args.pretrain_epochs == 0:
        args.pretrain_epochs = args.epochs

    args.func(args)
