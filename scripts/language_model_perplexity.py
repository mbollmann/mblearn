#!/usr/bin/env python3

import argparse

from mblearn.data import TextData
from mblearn.models import CategoricalModelWrapper
from mblearn.utils import Vectorizer
from mblearn.serialization import load, dump

def flatten_sentences(sentences):
    pad = lambda x: x + ["</s>"]
    return [w for s in sentences for w in pad(s)]

def main(args):
    sequence_length = 30 if args.charbased else 5
    batch_size = 256
    unk_types = 87962

    print("Loading model...")
    with open("language_model.yaml", "r") as f:
        wrapper = load(f.read())
    wrapper.model.compile(loss='sparse_categorical_crossentropy', optimizer='rmsprop')
    wrapper.model.load_weights("language_model.hd5")

    print("Preparing test data...")
    data = TextData(args.infile.read())
    data.replace_classes()
    x = []
    y = []
    text = flatten_sentences(data.sentences)
    for i in range(sequence_length, len(text)):
        x.append(text[(i-sequence_length):i])
        y.append(text[i])

    print("Evaluating...")
    unk_index = wrapper.vectorizer["<UNK>"]
    count, inv_count = len(y), (- 1.0 / len(y))
    correct, perplexity = 0, 1.0

    for i in range(0, len(y), batch_size):
        preds = wrapper.predict_on_batch(x[i:(i+batch_size)])
        for (y_t, p) in zip(y[i:(i+batch_size)], preds):
            p[unk_index] /= unk_types
            y_p = wrapper.vectorizer.get_item(p.argmax())
            if y_t == y_p:
                correct += 1
            idx = wrapper.vectorizer.get_index(y_t)
            perplexity *= p[idx] ** inv_count

    print("  Accuracy: {0:.4f}".format(correct / count))
    print("Perplexity: {0:.2f}".format(perplexity))

    #print("Multiclass logloss: {0:.4f}".format(loss))
    #print("        Perplexity: {0:.2f}".format(2 ** loss))


if __name__ == '__main__':
    description = ""
    epilog = ""
    parser = argparse.ArgumentParser(description=description, epilog=epilog)
    parser.add_argument('infile',
                        metavar='INPUT',
                        type=argparse.FileType('r', encoding="utf-8"),
                        help='Input file')
    parser.add_argument('-c', '--charbased',
                        action='store_true',
                        default=False,
                        help='Use characters instead of words as units')

    args = parser.parse_args()
    main(args)
