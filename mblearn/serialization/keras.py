"""Serialization functions for native `Keras`_ objects.

.. _Keras: http://keras.io/
"""

import yaml
from keras.models import Model, Sequential, model_from_yaml
from . import registry

_custom_objects = {}

# Keras can only dump/load directly to/from a YAML string, so we perform an
# extra conversion step to save the data properly as part of *our* YAML, while
# still relying on the proper Keras functions

def _dump_keras_model(model):
    return yaml.load(model.to_yaml())

@registry.dumper(Model, 'keras.model', version=1)
def _dump_model(model):
    return _dump_keras_model(model)

@registry.dumper(Sequential, 'keras.model', version=1)
def _dump_sequential(model):
    return _dump_keras_model(model)

@registry.loader('keras.model', version=1)
def _load_model(data, version):
    return model_from_yaml(yaml.dump(data), custom_objects=_custom_objects)

# decorator to register custom Keras layers
def custom_object(obj):
    _custom_objects[obj.__name__] = obj
    return obj

def load_model_with_custom_params(data, custom_params):
    if 'config' in data:
        # fix for some models that seem to be incorrectly saved (semictx with
        # Keras 1.2): make sure that all InputLayer instances are defined first
        n  = [l for l in data['config']['layers'] if l['class_name'] == "InputLayer"]
        n += [l for l in data['config']['layers'] if l['class_name'] != "InputLayer"]
        data['config']['layers'] = n
    model = model_from_yaml(yaml.dump(data), custom_objects=_custom_objects)
    for k, v in custom_params.items():
        setattr(model, k, v)
    return model
