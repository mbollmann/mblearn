import numpy as np
from keras.callbacks import Callback, ModelCheckpoint, EarlyStopping
from .models.wrappers import CategoricalModelWrapper

class ClearLine(Callback):
    def on_epoch_end(self, epoch, logs=None):
        print()

class MultiModelCheckpoint(ModelCheckpoint):
    """Save a list of models after every epoch.

    This callback is intended for multi-task learning models, or any other type
    of model which is made up of several sub-models.  The callback is attached
    to the full model which reports loss/accuracy scores for all sub-models, but
    it is the individual sub-models that are to be saved.

    The monitored quantity (e.g., loss) can be tracked for each sub-model
    individually, so with `save_best_only=True`, only those sub-models for which
    the respective quantity has improved are saved.

    Arguments are the same as for `ModelCheckpoint` except for those listed.

    Args:
        models: List of sub-models to be saved by the checkpoint.
        filepaths: List of strings, paths to save the model files, one for each
            sub-model.
        monitor: Quantity to monitor; is formatted with the index of each
            sub-model, e.g., "{}_loss" would monitor "0_loss", "1_loss" etc.

    See Also:
        `ModelCheckpoint`_ in the Keras documentation.

        .. _ModelCheckpoint: https://keras.io/callbacks/#modelcheckpoint
    """

    def __init__(self, models, filepaths, **kwargs):
        assert len(models) == len(filepaths)
        super(MultiModelCheckpoint, self).__init__("", **kwargs)
        self.models = models
        self.filepaths = filepaths
        self.bests = [self.best] * len(self.models)

    def save_model(self, model, filepath, **kwargs):
        if isinstance(model, CategoricalModelWrapper):
            model = model.model
        if self.save_weights_only:
            model.save_weights(filepath, **kwargs)
        else:
            model.save(filepath, **kwargs)

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        self.epochs_since_last_save += 1
        if self.epochs_since_last_save >= self.period:
            self.epochs_since_last_save = 0
            if self.save_best_only:
                for (n, model) in enumerate(self.models):
                    filepath = self.filepaths[n].format(epoch=epoch, **logs)
                    monitor = self.monitor.format(n)
                    current = logs.get(monitor)
                    if current is None:
                        warnings.warn('Can save best model only with %s available, '
                                      'skipping.' % (monitor), RuntimeWarning)
                    else:
                        if self.monitor_op(current, self.bests[n]):
                            if self.verbose > 0:
                                print('Epoch %05d: %s improved from %0.5f to %0.5f,'
                                      ' saving model to %s'
                                      % (epoch, monitor, self.bests[n],
                                         current, filepath))
                            self.bests[n] = current
                            self.save_model(model, filepath, overwrite=True)
                        else:
                            if self.verbose > 0:
                                print('Epoch %05d: %s did not improve' %
                                      (epoch, monitor))
            else:
                for (n, model) in enumerate(self.models):
                    filepath = self.filepaths[n].format(epoch=epoch, **logs)
                    if self.verbose > 0:
                        print('Epoch %05d: saving model to %s' % (epoch, filepath))
                    self.save_model(model, filepath, overwrite=True)


class MultiEarlyStopping(EarlyStopping):
    """Stop training when a list of monitored quantities has stopped improving.

    Args:
        monitor: List of quantities to be monitored.
        combine_mode: One of {ave, any, all}.  In `ave` mode,
            training will stop when the average of all
            monitored quantities has stopped improving;
            in `any` mode, training will stop when any
            of the monitored quantities has stopped
            improving; in `all` mode, training will stop
            when all monitored quantities have stopped
            improving.
    """

    def __init__(self, monitor=None, combine_mode='ave', **kwargs):
        if not isinstance(monitor, (tuple, list)):
            raise ValueError('MultiEarlyStopping requires a list or tuple of '
                             'quantities to be given via "monitor"; got {}'\
                             .format(type(monitor)))
        if combine_mode not in ('ave', 'any', 'all'):
            warnings.warn('MultiEarlyStopping mode {} is unknown, '
                          'fallback to ave mode.'.format(combine_mode),
                          RuntimeWarning)
            combine_mode = 'ave'

        super(MultiEarlyStopping, self).__init__(monitor=monitor[0], **kwargs)

        self.monitor = monitor
        self.combine_mode = combine_mode
        if len(self.monitor) > 3:
            self.monitor_values_str = '['+','.join(self.monitor[:3])+',...]'
        else:
            self.monitor_values_str = '['+','.join(self.monitor)+']'

    def on_train_begin(self, logs=None):
        self.wait = 0  # Allow instances to be re-used
        if self.monitor_op == np.less:
            self.best = np.Inf
            self.bests = [np.Inf] * len(self.monitor)
        else:
            self.best = -np.Inf
            self.bests = [-np.Inf] * len(self.monitor)

    def on_epoch_end(self, epoch, logs=None):
        current_all = [logs.get(m) for m in self.monitor]
        if any(c is None for c in current_all):
            warnings.warn('Early stopping requires all of [%s] to be available!' %
                          (','.join(self.monitor)), RuntimeWarning)

        ave = np.mean(current_all)
        if self.combine_mode in ('any', 'all'):
            monitor_all = [self.monitor_op(c - self.min_delta, b) \
                           for c, b in zip(current_all, self.bests)]
            if all(monitor_all):
                msg = 'all quantities have improved above min_delta'
            elif any(monitor_all):
                msg = '%2d/%2d quantities have improved above min_delta' % (sum(monitor_all), len(monitor_all))
            else:
                msg = 'all quantities have stopped improving above min_delta'
            if self.verbose > 0:
                print('Epoch %05d: %s (ave %0.5f)' % (epoch, msg, ave))

            self.bests = [new_ if better else old_ \
                          for (new_, old_, better) in zip(current_all, self.bests, monitor_all)]

            improved = \
                (self.combine_mode == "all" and any(monitor_all)) \
                or (self.combine_mode == "any" and all(monitor_all))
        elif self.combine_mode == 'ave':
            if self.monitor_op(ave - self.min_delta, self.best):
                if self.verbose > 0:
                    print('Epoch %05d: ave of %s improved from %0.5f to %0.5f'
                          % (epoch, self.monitor_values_str,
                             self.best, ave))
                self.best = ave
                improved = True
            else:
                if self.verbose > 0:
                    print('Epoch %05d: ave of %s did not improve' %
                          (epoch, self.monitor_values_str))
                improved = False

        if improved:
            self.wait = 0
        else:
            if self.wait >= self.patience:
                self.stopped_epoch = epoch
                self.model.stop_training = True
            self.wait += 1

    def on_train_end(self, logs=None):
        if self.stopped_epoch > 0 and self.verbose > 0:
            print('Epoch %05d: early stopping' % (self.stopped_epoch))
