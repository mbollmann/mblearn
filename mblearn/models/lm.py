
import numpy as np
from keras.models import Model, Sequential
from keras.layers import Input, Embedding, Dense, Activation, Dropout, Reshape, merge
from keras.layers import Convolution1D as Conv1D, Convolution2D as Conv2D
from keras.layers import GlobalMaxPooling1D, GlobalAveragePooling1D, MaxPooling2D, BatchNormalization
from keras.layers import LSTM, TimeDistributed
from keras.metrics import sparse_categorical_crossentropy
from keras.optimizers import SGD
from keras.regularizers import l2
from keras import backend as K

from .wrappers import CategoricalModelWrapper
from ..utils import Vectorizer
from ..serialization import keras, registry
from ..layers import HighwayMerge

special_symbols = {
    'bos': '<S>',
    'eos': '</S>',
    'unk': '<UNK>',
    'space': '_',
    'bow': '{',
    'eow': '}'
}

def perplexity(y_true, y_pred):
    return K.exp(sparse_categorical_crossentropy(y_true, y_pred))

def sample(a, temperature=1.0):
    """Helper function to sample an index from a probability array."""
    a = np.cast[np.float64](a)
    a = np.log(a) / temperature
    a = np.exp(a) / np.sum(np.exp(a))
    if np.sum(a) > 1.0:
        a /= np.sum(a)
    return np.argmax(np.random.multinomial(1, a, 1))

class sSGD(SGD):
    def __init__(self, scale=1., **kwargs):
        super(sSGD, self).__init__(**kwargs)
        self.scale = scale;
    def get_gradients(self, loss, params):
        grads = K.gradients(loss, params)
        if self.scale != 1.:
            grads = [g*K.variable(self.scale) for g in grads]
        if hasattr(self, 'clipnorm') and self.clipnorm > 0:
            norm = K.sqrt(sum([K.sum(K.square(g)) for g in grads]))
            grads = [K.switch(norm >= self.clipnorm, g * self.clipnorm / norm, g) for g in grads]
        if hasattr(self, 'clipvalue') and self.clipvalue > 0:
            grads = [K.clip(g, -self.clipvalue, self.clipvalue) for g in grads]
        return grads

class LanguageModel:
    """Model for character-based language modelling.

    Args:
        context_len (int): Length of context window.
        embedding_dim (int): Dimensionality of the word embedding vectors.
        hidden_dim (int): Dimensionality of the hidden layer units.
        depth (int): Depth of the net (amount of LSTM units).
        dropout (float): Dropout to apply to the output of recurrent layers.
        optimizer (str): Optimizer for the model.
    """

    def __init__(self, context_len=2, embedding_dim=100, hidden_dim=100,
                 depth=2, dropout=0.0, optimizer="rmsprop"):
        self.wrapper = None
        self._model = None
        self._vectorizer = Vectorizer(unk_symbol=special_symbols['unk'])
        self._vectorizer.update(special_symbols.values())
        self.context_len = context_len
        self.depth = depth
        self.dropout = dropout
        self.embedding_dim = embedding_dim
        self.hidden_dim = hidden_dim
        self.optimizer = optimizer
        self._unk_types = 1

    @property
    def model(self):
        return self._model

    @model.setter
    def model(self, obj):
        self._model = obj
        if obj is None:
            self.wrapper = None
        else:
            self._model.compile(
                loss='sparse_categorical_crossentropy',
                optimizer=self.optimizer,
                metrics=[perplexity]
            )
            self.wrapper = CategoricalModelWrapper(self._model,
                                                   vectorizer=self._vectorizer)

    @property
    def output_dim(self):
        return len(self._vectorizer)

    def build_lstm_model(self):
        input_ = Input(shape=(self.context_len,), dtype='int32', name='encoder_input')
        vector = Embedding(self.output_dim, self.embedding_dim, mask_zero=True)(input_)
        for i in range(self.depth, 0, -1):
            vector = LSTM(self.hidden_dim, return_sequences=(i>1))(vector)
            if self.dropout > 0:
                vector = Dropout(self.dropout)(vector)
        vector = Dense(self.output_dim,
                       activation='softmax')(vector)
        model = Model(input=[input_], output=[vector])
        return model

    def build_cnn_model(self):
        input_ = Input(shape=(self.context_len,), dtype='int32', name='encoder_input')
        vector = Embedding(self.output_dim, self.embedding_dim)(input_)
        convos = []
        for n in range(self.depth): # interpret depth as maximum n-gram length
            conv_vector = Conv1D(self.hidden_dim, n+1, activation='tanh')(vector)
            pool_vector = GlobalMaxPooling1D()(conv_vector)
            convos.append(pool_vector)
        vector = merge(convos, mode='concat')
        vector = BatchNormalization()(vector)
        if self.dropout > 0:
            vector = Dropout(self.dropout)(vector)
        highway_vector = vector
        transform_vector = Dense(self.hidden_dim * self.depth, activation='relu')(vector)
        vector = HighwayMerge()([highway_vector, transform_vector])
        vector = Dense(self.output_dim,
                       activation='softmax')(vector)
        model = Model(input=[input_], output=[vector])
        return model

    def _make_x_y(self, data):
        x, y = [], []
        for sent in data.sentences:
            chars = special_symbols['space'].join(sent)
            chars = [special_symbols['bos']] + [c for c in chars] + [special_symbols['eos']]
            for i in range(1, len(chars)):
                left_idx = max(i - self.context_len, 0)
                x.append(chars[left_idx:i])
                y.append(chars[i])
        #from pprint import pprint
        #for i in (0, 42):
        #    print("##### sample x/y[{}]".format(i))
        #    pprint(x[i])
        #    pprint(y[i])
        return (x, y)

    def train(self, data, frequency_cutoff=None, validation_data=None,
              model_type="cnn", **kwargs):
        """Train the language model.

        Args:
            data (TextData): Data to train on, as a
                :py:class:`mblearn.data.TextData` instance.
                Any desired pre-processing on the data should be done before
                training *except* for frequency cutoff.
            frequency_cutoff (Optional[int]):
                see :py:meth:`mblearn.data.TextData.frequency_cutoff`
            validation_data (Optional[TextData]): Data to validate on, as a
                :py:class:`mblearn.data.TextData` instance.
            model_type (Optional[str]): One of "cnn" or "lstm".
            **kwargs: Arguments to pass to `Model.fit()`_.

        Returns:
            The return value of `Model.fit()`_.

        .. _Model.fit(): http://keras.io/models/model/#fit
        """
        self._vectorizer.update(data.alphabet)
        if model_type.lower() == "cnn":
            self.model = self.build_cnn_model()
        elif model_type.lower() == "lstm":
            self.model = self.build_lstm_model()
        else:
            self.model = self.build_model()
        self.model.summary()
        if frequency_cutoff is not None:
            self._unk_types, _ = data.frequency_cutoff(frequency_cutoff)
        x, y = self._make_x_y(data)
        if validation_data is not None:
            validation_data = self._make_x_y(validation_data)
        return self.wrapper.fit(x, y, validation_data=validation_data, **kwargs)

    def generate(self, seed=None, length=10, diversity=0.2):
        """Generate new text based on the learned model.

        Args:
            seed (Optional[list]): List of words to use as the seed for
                generation.  If not given, a random word from the vocabulary
                will be picked.
            length (Optional[int]): How many words to generate.
            diversity (Optional[float]): Diversity of the generated text;
                lower values produce text close to the model's probabilities,
                higher values produce more randomized output.

        Returns:
            A list of generated words of length `length`.
        """
        if seed is None:
            idx = np.random.randint(5, self.output_dim)
            seed = [special_symbols['bos'], self.wrapper.vectorizer.get_item(idx)]
        x, y = seed[:], seed[:]
        unk = self.wrapper.vectorizer.get_index(special_symbols['unk'])

        for _ in range(length):
            if diversity == 0:
                preds = self.wrapper.predict_classes([[x]], verbose=0)
                next_word = preds[0][0]
            else:
                preds = self.wrapper.predict([[x]], verbose=0)[0]
                preds[unk] /= self._unk_types
                next_idx = sample(preds, diversity)
                next_word = self.wrapper.vectorizer.get_item(next_idx)
            if len(x) == self.context_len:
                x = x[1:] + [next_word]
            else:
                x = x + [next_word]
            y.append(next_word)
            if next_word == special_symbols['eos']:
                break
        return y

    def evaluate(self, data, **kwargs):
        x, y = self._make_x_y(data)
        return self.wrapper.evaluate(x, y, **kwargs)

    def save_weights(self, filename):
        return self.wrapper.model.save_weights(filename)

class CharAwareLM(LanguageModel):

    def __init__(self, word_len=50, depth=2, embedding_dim=15,
                 filter_depth=6, filter_width=200,
                 hidden_dim=300, highway_layers=1, **kwargs):
        super(CharAwareLM, self).__init__(
            depth=depth, embedding_dim=embedding_dim,
            hidden_dim=hidden_dim, **kwargs)
        self.filter_depth = filter_depth
        self.filter_width = filter_width
        self.highway_layers = highway_layers
        self.word_len = word_len

    def _make_x_y(self, data):
        x, y = [], []
        for sent in data.sentences:
            words = [special_symbols['bow'] + w + special_symbols['eow'] for w in sent]
            for i, word in enumerate(words):
                left_idx = max(i - self.context_len, 0)
                for j, char in enumerate(word):
                    if char == special_symbols['bow']:
                        continue
                    x.append(words[left_idx:i] + [word[:j]])
                    y.append(char)
                    if j+1 >= self.word_len or char == special_symbols['eow']:
                        break
        return (x, y)

    def build_model(self):
        char_dim = self.output_dim
        input_ = Input(shape=(self.context_len, self.word_len), dtype='int32', name='encoder_input')
        vector = TimeDistributed(Embedding(char_dim, self.embedding_dim))(input_)
        convos = []
        total_conv_width = 0
        for n in range(self.filter_depth): # interpret depth as maximum n-gram length
            #conv_width = min(self.filter_width, (self.filter_width // 4) * (n+1))
            conv_width = 25 * (n+1)
            total_conv_width += conv_width
            conv_vector = Conv2D(conv_width, 1, n+1, activation='tanh', dim_ordering="tf")(vector)
            pool_vector = MaxPooling2D((1, self.word_len - n), dim_ordering="tf")(conv_vector)
            #conv_vector = TimeDistributed(Conv1D(conv_width, n+1, activation='tanh'))(vector)
            #pool_vector = TimeDistributed(GlobalMaxPooling1D())(conv_vector)
            convos.append(pool_vector)
        vector = merge(convos, mode='concat') # TODO: output_mask?
        vector = Reshape((self.context_len, -1))(vector)
        #vector = BatchNormalization()(vector)
        for _ in range(self.highway_layers):
            highway_vector = vector
            transform_vector = Dense(total_conv_width, activation='relu')(vector)
            vector = HighwayMerge()([highway_vector, transform_vector])
        for n in range(self.depth, 0, -1):
            vector = LSTM(self.hidden_dim, return_sequences=(n>1), activation='tanh')(vector)
            if self.dropout:
                vector = Dropout(self.dropout)(vector)
        vector = Dense(self.output_dim, activation='softmax')(vector)
        model = Model(input=[input_], output=[vector])
        return model

    def train(self, *args, **kwargs):
        kwargs['model_type'] = "full"
        return super(CharAwareLM, self).train(*args, **kwargs)



@registry.dumper(LanguageModel, 'mblearn.LanguageModel', version=1)
def _dump_lm(obj):
    return dict(
        model=obj.model,
        vectorizer=obj._vectorizer,
        context_len=obj.context_len,
        depth=obj.depth,
        dropout=obj.dropout,
        embedding_dim=obj.embedding_dim,
        hidden_dim=obj.hidden_dim,
        output_dim=obj.output_dim,
        unk_types=obj._unk_types
    )

@registry.loader('mblearn.LanguageModel', version=1)
def _load_lm(data, version):
    lm = LanguageModel(
        context_len=data['context_len'],
        depth=data['depth'],
        dropout=data['dropout'],
        embedding_dim=data['embedding_dim'],
        hidden_dim=data['hidden_dim'],
        optimizer="sgd" # doesn't matter, loaded models are not for (re-)training
        )
    lm._unk_types = data['unk_types']
    lm._vectorizer = data['vectorizer']
    lm.model = data['model'] # compiles model & instantiates wrapper
    return lm

@registry.dumper(CharAwareLM, 'mblearn.CharAwareLM', version=1)
def _dump_calm(obj):
    dict_ = _dump_lm(obj)
    dict_.update(dict(
        filter_depth=obj.filter_depth,
        filter_width=obj.filter_width,
        highway_layers=obj.highway_layers,
        word_len=obj.word_len
        ))
    return dict_

@registry.loader('mblearn.CharAwareLM', version=1)
def _load_calm(data, version):
    lm = CharAwareLM(
        context_len=data['context_len'],
        depth=data['depth'],
        dropout=data['dropout'],
        embedding_dim=data['embedding_dim'],
        hidden_dim=data['hidden_dim'],
        filter_depth=data['filter_depth'],
        filter_width=data['filter_width'],
        highway_layers=data['highway_layers'],
        word_len=data['word_len'],
        optimizer="sgd" # doesn't matter, loaded models are not for (re-)training
        )
    lm._unk_types = data['unk_types']
    lm._vectorizer = data['vectorizer']
    lm.model = data['model'] # compiles model & instantiates wrapper
    return lm
