import itertools
import numpy as np
from keras.engine import Model
from keras.layers import Embedding, Wrapper  # for checks
from keras.models import Sequential
from ..helper_functions import standardize_generic_input, predict_classes
from ..serialization import keras, registry

def _to_argmax(seq):
    return [v.argmax() for v in seq]

def _get_by_tuple(data, tuple_idx):
    elem = data
    for i in tuple_idx:
        try:
            elem = elem[i]
        except IndexError:
            elem = []
            break
    return elem

def _expects_sparse_input(elem):
    if elem == "sparse_categorical_crossentropy":
        return True
    elif callable(elem) and elem.__name__ == "sparse_loss":
        return True
    return False

class CategoricalModelWrapper:
    """Wrapper for a Keras model that takes categorical data as input/output.

    Args:
        model: An instance of a Keras model.
        input_vectorizer: A list or dict containing :py:class:`Vectorizer`
            instances for converting between categorical data and numeric
            indices on the input side.  One item for each input of the model,
            or None if the respective input should not be converted.
        output_vectorizer: A list or dict containing :py:class:`Vectorizer`
            instances for converting between categorical data and numeric
            indices on the output side.  One item for each output of the model,
            or None if the respective output should not be converted.
        vectorizer: An instance of :py:class:`Vectorizer`
            for converting between categorical data and numeric indices.
            Identical to setting both `input_vectorizer` and `output_vectorizer`
            to this object on a model with only one input and output.
        embedding_at: An index or list of indices where the model should
            assume an Embedding layer.  (Can be detected automatically in most
            cases.)
    """

    def __init__(self, model=None, input_vectorizer=None,
                 output_vectorizer=None, vectorizer=None,
                 embedding_at=None, input_padding=None):
        self.model = model
        self._input_padding = input_padding
        if vectorizer is not None:
            self.input_vectorizer = vectorizer
            self.output_vectorizer = vectorizer
        else:
            self.input_vectorizer = input_vectorizer
            self.output_vectorizer = output_vectorizer
        if embedding_at is not None:
            if isinstance(embedding_at, (tuple, list)):
                self._embedding_inputs = list(embedding_at)
            else:
                self._embedding_inputs = [embedding_at]

    @property
    def is_sequential(self):
        return isinstance(self.model, Sequential)

    @property
    def input_names(self):
        if isinstance(self.model, Sequential):
            return ['seq_input']
        else:
            return self.model.input_names

    @property
    def output_names(self):
        if isinstance(self.model, Sequential):
            return ['seq_output']
        else:
            return self.model.output_names

    @property
    def input_padding(self):
        if self._input_padding:
            return self._input_padding
        else:
            return ['left'] * len(self.model.inputs)

    @input_padding.setter
    def input_padding(self, value):
        if not isinstance(value, (tuple, list)):
            value = [value]
        if len(value) != len(self.model.inputs):
            raise Exception('Input padding must be specified for all model inputs')
        self._input_padding = value

    @property
    def input_shape(self):
        if isinstance(self.model, Sequential) or len(self.model.inputs) == 1:
            return [self.model.input_shape]
        else:
            return self.model.input_shape

    @property
    def output_shape(self):
        if isinstance(self.model, Sequential) or len(self.model.outputs) == 1:
            return [self.model.output_shape]
        else:
            return self.model.output_shape

    @property
    def inputs(self):
        return self.model.inputs

    @property
    def outputs(self):
        return self.model.outputs

    @property
    def input_vectorizer(self):
        return self._input_vectorizer

    @input_vectorizer.setter
    def input_vectorizer(self, value):
        self._input_vectorizer = standardize_generic_input(value, self.input_names)

    @property
    def output_vectorizer(self):
        return self._output_vectorizer

    @output_vectorizer.setter
    def output_vectorizer(self, value):
        self._output_vectorizer = standardize_generic_input(value, self.output_names)

    @property
    def vectorizer(self):
        if self.input_vectorizer == self.output_vectorizer:
            return None if self.input_vectorizer is None else self.input_vectorizer[0]
        else:
            raise Exception('Model has different vectorizer settings -- '
                            'please access them explicitly via "input_vectorizer" '
                            'or "output_vectorizer" attributes')

    @vectorizer.setter
    def vectorizer(self, value):
        self.input_vectorizer = value
        self.output_vectorizer = value

    @property
    def embedding_inputs(self):
        def is_or_wraps_embedding(layer):
            if isinstance(layer, Embedding):
                return True
            elif isinstance(layer, Wrapper):
                return is_or_wraps_embedding(layer.layer)
            else:
                return False
        if not hasattr(self, '_embedding_inputs'):
            embedding_layers = filter(is_or_wraps_embedding, self.model.layers)
            self._embedding_inputs = [self.inputs.index(layer.get_input_at(n)) \
                                      for layer in embedding_layers \
                                      for n in range(len(layer.inbound_nodes)) \
                                      if layer.get_input_at(n) in self.inputs]
        return self._embedding_inputs

    def has_embedding_at(self, i):
        """Check whether input at position `i` feeds into an Embedding layer."""
        return (i in self.embedding_inputs)

    def transform(self, data, shape, vectorizer, **kwargs):
        """Transform categorical data to one-hot vectors.

        Args:
            data: Categorical data to convert, interpreted depending on `shape`.
            shape (tuple): Shape of the data, as used by Keras.
                If 2-dimensional, `data` is treated as a list of categorical
                items; if higher-dimensional, `data` is treated as containing
                sequences of items.
            vectorizer: Vectorizer to use for the transformation.

        Returns:
            A NumPy vector or matrix with one-hot representations of `data`.
        """
        if len(shape) > 2:
            new_x = np.zeros(tuple([len(data)] + list(shape[1:])), dtype=np.bool)
            sequence_dims = [range(i) for i in new_x.shape[:-2]]
            for idx in itertools.product(*sequence_dims):
                elem = _get_by_tuple(data, idx)
                new_x[idx] = vectorizer.map_to_onehot(elem, padding=shape[-2], **kwargs)
        else:
            new_x = vectorizer.map_to_onehot(data, padding=False, **kwargs)
        return new_x

    def transform_indices(self, data, shape, vectorizer, sparse_output=False, **kwargs):
        """Transform categorical data to index vectors.

        Args:
            data: Categorical data to convert, interpreted depending on `shape`
                and `sparse_output`.
            shape (tuple): Shape of the data, as used by Keras.
            vectorizer: Vectorizer to use for the transformation.
            sparse_output (Optional[bool]): If True, `data` and `shape` are
                interpreted just as with :py:meth:`transform`.
                If False (default), `shape` is interpreted as the input shape
                of an Embedding layer, i.e., the value of the last dimension
                specifies the sequence length.

        Returns:
            A NumPy vector or matrix with index representations of `data`.
        """
        if len(shape) == 2 and (sparse_output or shape[1] == 1):
            new_x = np.array(
                vectorizer.map_to_index(data, padding=False, **kwargs)
            )
        else:
            if sparse_output:
                new_x = np.zeros(tuple([len(data)] + list(shape[1:-1])))
                padding = shape[-2]
            else:
                new_x = np.zeros(tuple([len(data)] + list(shape[1:])))
                padding = shape[-1] if shape[-1] > 1 else False
            sequence_dims = [range(i) for i in new_x.shape[:-1]]
            for idx in itertools.product(*sequence_dims):
                elem = _get_by_tuple(data, idx)
                new_x[idx] = np.array(
                    vectorizer.map_to_index(elem, padding=padding, **kwargs)
                )
        if sparse_output:
            new_x = np.expand_dims(new_x, -1)
        return new_x

    def transform_inputs(self, x, y):
        """Transform model inputs to vectors.

        Checks if arguments are supposed to be categorical and calls
        :py:meth:`transform` on them where necessary.

        Args:
            x: Data in the shape of the model's input.
            y: Data in the shape of the model's output (e.g., for training
                or evaluating the model).

        Returns:
            A tuple ``(x, y)`` containing transformed versions of `x` and `y`.
        """
        (x, y) = (standardize_generic_input(x, self.input_names),
                  standardize_generic_input(y, self.output_names))
        for (i, x_i) in enumerate(x):
            if x_i is not None and self.input_vectorizer[i] is not None:
                if self.has_embedding_at(i):
                    x[i] = self.transform_indices(
                        x_i, self.input_shape[i], self.input_vectorizer[i],
                        pad_on=self.input_padding[i]
                        )
                else:
                    x[i] = self.transform(
                        x_i, self.input_shape[i], self.input_vectorizer[i],
                        pad_on=self.input_padding[i]
                        )
        losses = standardize_generic_input(self.model.loss, self.output_names)
        for (i, y_i) in enumerate(y):
            if y_i is not None and self.output_vectorizer[i] is not None:
                if _expects_sparse_input(losses[i]):
                    y[i] = self.transform_indices(
                        y_i, self.output_shape[i], self.output_vectorizer[i],
                        sparse_output=True
                        )
                else:
                    y[i] = self.transform(
                        y_i, self.output_shape[i], self.output_vectorizer[i]
                        )
        if self.is_sequential:
            return (np.asarray(x[0]), np.asarray(y[0]))
        else:
            return ([np.asarray(x_i) for x_i in x],
                    [np.asarray(y_i) for y_i in y])

    def transform_output(self, y, to_argmax=False):
        """Transform model output to categorical data.

        Checks if model output is supposed to be categorical and converts it
        back to categorical items, if necessary.

        Args:
            y: List of NumPy matrices in the shape(s) of the model's output(s).

        Returns:
            List of items corresponding to `y` if model has categorical output.
        """
        tf = _to_argmax if to_argmax else lambda x: x
        def map_to_items(sequence, dim):
            if dim > 3:
                return [map_to_items(s, dim-1) for s in sequence]
            else:
                return [self.output_vectorizer[i].map_to_items(tf(elem)) for elem in sequence]

        if type(y) not in (list, tuple):
            y = [y]
        for (i, y_i) in enumerate(y):
            if self.output_vectorizer[i] is not None:
                if len(self.output_shape[i]) == 2:
                    y[i] = [self.output_vectorizer[i].get_item(idx) for idx in tf(y_i)]
                else:
                    y[i] = map_to_items(y_i, len(self.output_shape[i]))
        if self.is_sequential:
            return y[0]
        else:
            return y

    def transform_output_with_proba(self, y):
        """Transform model output to lists of categorical data with probabilities.

        Args:
            y: List of NumPy matrices in the shape(s) of the model's output(s).

        Returns:
            If model has categorical output, a list of tuples of the form
            `(class, probability)` for each element to be predicted.
        """
        def map_proba(sequence, dim):
            if dim > 3:
                return [map_proba(s, dim-1) for s in sequence]
            else:
                new_y = []
                for elem in sequence:
                    elem_y = []
                    for proba in elem:
                        elem_y.append([
                            (self.output_vectorizer[i].get_item(idx), proba[idx]) \
                             for idx in proba.argsort()[::-1]
                        ])
                    new_y.append(elem_y)
                return new_y

        for (i, y_i) in enumerate(y):
            if self.output_vectorizer[i] is not None:
                if len(self.output_shape[i]) == 2:
                    new_y = []
                    for proba in y_i:
                        new_y.append([
                            (self.output_vectorizer[i].get_item(idx), proba[idx]) \
                            for idx in proba.argsort()[::-1]
                        ])
                else:
                    new_y = map_proba(y_i, len(self.output_shape[i]))
                y[i] = new_y
        if self.is_sequential:
            return y[0]
        else:
            return y

    def fit(self, x, y, validation_data=None, **kwargs):
        """Train the model with categorical data.

        See Also:
            `Model.fit()`_ in the Keras documentation.

        .. _Model.fit(): http://keras.io/models/model/#fit
        """
        (x, y) = self.transform_inputs(x, y)
        if validation_data is not None:
            x_v, y_v, *w_v = validation_data
            x_v, y_v = self.transform_inputs(x_v, y_v)
            validation_data = (x_v, y_v) + tuple(w_v)
        return self.model.fit(x, y, validation_data=validation_data, **kwargs)

    def fit_generator(self, generator, *args, validation_data=None, **kwargs):
        """Train the model with categorical data.

        See Also:
            `Model.fit_generator()`_ in the Keras documentation.

        .. _Model.fit_generator(): http://keras.io/models/model/#fit_generator
        """
        def wrap_generator(gen):
            while True:
                inputs, targets, *sample_weights = next(gen)
                inputs, targets = self.transform_inputs(inputs, targets)
                yield (inputs, targets) + tuple(sample_weights)
        if validation_data is not None:
            if type(validation_data) in (tuple, list):
                x_v, y_v, *w_v = validation_data
                x_v, y_v = self.transform_inputs(x_v, y_v)
                validation_data = (x_v, y_v) + tuple(w_v)
            else:
                validation_data = wrap_generator(validation_data)
        return self.model.fit_generator(
            wrap_generator(generator), *args,
            validation_data=validation_data, **kwargs
        )

    def evaluate(self, x, y, **kwargs):
        """Return the loss value and metrics values for the model in test mode.

        See Also:
            `Model.evaluate()`_ in the Keras documentation.

        .. _Model.evaluate(): http://keras.io/models/model/#evaluate
        """
        (x, y) = self.transform_inputs(x, y)
        return self.model.evaluate(x, y, **kwargs)

    def predict(self, x, **kwargs):
        """Generate output predictions for the input samples.

        Returns:
            A NumPy array, just like `Model.predict()`_.  Predictions are
            never mapped back to categorical data; for that, use either
            :py:meth:`predict_classes` or :py:meth:`predict_proba`.

        See Also:
            `Model.predict()`_ in the Keras documentation.

        .. _Model.predict(): http://keras.io/models/model/#predict
        """
        (x, _) = self.transform_inputs(x, None)
        preds = self.model.predict(x, **kwargs)
        return preds

    def predict_classes(self, x, **kwargs):
        """Generate class predictions.

        Returns:
            The model output, with one class per item to be predicted whenever
            the output is categorical.

        Note:
            Arguments are the same as for :py:meth:`predict`.
        """
        (x, _) = self.transform_inputs(x, None)
        if self.is_sequential:
            preds = [self.model.predict_classes(x, **kwargs)]
        elif hasattr(self.model, 'predict_classes') and callable(self.model.predict_classes):
            preds = self.model.predict_classes(x, **kwargs)
        else:
            preds = self.model.predict(x, **kwargs)
            if len(self.outputs) == 1:
                preds = [preds]
            for (i, proba) in enumerate(preds):
                if self.output_vectorizer[i] is not None:
                    preds[i] = predict_classes(proba)
        return self.transform_output(preds)

    def predict_proba(self, x, **kwargs):
        """Generate class probabilities.

        Returns:
            The model output, with a list of tuples `(class, probability)` per
            item to be predicted whenever the output is categorical.

        Note:
            Arguments are the same as for :py:meth:`predict`.
        """
        (x, _) = self.transform_inputs(x, None)
        if self.is_sequential or len(self.model.outputs) == 1:
            preds = [self.model.predict(x, **kwargs)] # TODO: use predict_proba?
        else:
            preds = self.model.predict(x, **kwargs)
        return self.transform_output_with_proba(preds)

    def train_on_batch(self, x, y, **kwargs):
        """Runs a single gradient update on a single batch of data.

        See Also:
            `Model.train_on_batch()`_ in the Keras documentation.

        .. _Model.train_on_batch(): http://keras.io/models/model/#train_on_batch
        """
        (x, y) = self.transform_inputs(x, y)
        return self.model.train_on_batch(x, y, **kwargs)

    def test_on_batch(self, x, y, **kwargs):
        """Test the model on a single batch of samples.

        See Also:
            `Model.test_on_batch()`_ in the Keras documentation.

        .. _Model.test_on_batch(): http://keras.io/models/model/#test_on_batch
        """
        (x, y) = self.transform_inputs(x, y)
        return self.model.test_on_batch(x, y, **kwargs)

    def predict_on_batch(self, x):
        """Returns predictions for a single batch of samples.

        See Also:
            :py:meth:`predict`
        """
        (x, _) = self.transform_inputs(x, None)
        preds = self.model.predict_on_batch(x)
        return preds

    def predict_classes_on_batch(self, x):
        """Returns predictions for a single batch of samples.

        See Also:
            :py:meth:`predict_classes`
        """
        (x, _) = self.transform_inputs(x, None)
        if self.is_sequential:
            preds = [self.model.predict_on_batch(x)]
        else:
            preds = self.model.predict_on_batch(x)
        return self.transform_output(preds, to_argmax=True)

    def predict_proba_on_batch(self, x):
        """Returns predictions for a single batch of samples.

        See Also:
            :py:meth:`predict_proba`
        """
        (x, _) = self.transform_inputs(x, None)
        if self.is_sequential:
            preds = [self.model.predict_on_batch(x)]
        else:
            preds = self.model.predict_on_batch(x)
        return self.transform_output_with_proba(preds)


@registry.dumper(CategoricalModelWrapper, 'mblearn.CategoricalModelWrapper', version=2)
def _dump_cmw(obj):
    return dict(
        model=obj.model,
        input_vectorizer=obj.input_vectorizer,
        output_vectorizer=obj.output_vectorizer,
        input_padding=obj.input_padding,
        embedding_at=obj.embedding_inputs
    )

@registry.loader('mblearn.CategoricalModelWrapper', version=1)
def _load_cmw_v1(data, version):
    input_vectorizer = data['vectorizer'] if data['categorical_input'] else None
    output_vectorizer = data['vectorizer'] if data['categorical_output'] else None
    wrapper = CategoricalModelWrapper(
        model=data['model'],
        input_vectorizer=input_vectorizer,
        output_vectorizer=output_vectorizer
        )
    return wrapper

@registry.loader('mblearn.CategoricalModelWrapper', version=2)
def _load_cmw_v2(data, version):
    return CategoricalModelWrapper(**data)
