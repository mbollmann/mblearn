from collections import defaultdict
import itertools as it
import numpy as np

_WORD_SPLIT_SYMBOLS = (' ', '÷')

def _numpy_zeros(shape):
    def fn():
        return np.zeros(shape)
    return fn

class AbstractFilter:
    def __init__(self):
        pass

    def __call__(self, history, preds):
        return self.call(history, preds)


class DictionaryFilter(AbstractFilter):
    """Dictionary filter for model predictions.

    Takes a list of words and filters out all predictions that do not lead to a
    valid dictionary word.

    Args:
        wordlist (list): List of words that should be recognized
        vectorizer: The Vectorizer object belonging to the model output to be
            filtered; this is important, because it will be assumed the model's
            numeric indices correspond to the characters returned by the
            Vectorizer, and the dimensionality of the model's output must match
            the length of the Vectorizer as well.
        eos_index: Index of the "end of sequence" symbol in the vectorizer
        force_padding (bool): If true, only accept words that have been padded
            by a word separator before the EOS symbol
    """
    def __init__(self, wordlist, vectorizer, eos_index, force_padding=True):
        self.vectorizer = vectorizer
        self.eos_index = eos_index
        self.force_padding = force_padding
        self.ws_indices = []
        for symbol in _WORD_SPLIT_SYMBOLS:
            if symbol in vectorizer:
                self.ws_indices.append(vectorizer[symbol])
        self.to_numpy = defaultdict(_numpy_zeros((len(self.vectorizer),)))
        self.compile(wordlist)

    def _get_last_split(self, history):
        if not self.ws_indices:
            return history
        idx = None
        rev_history = history[::-1]
        for ws_index in self.ws_indices:
            if ws_index in history:
                idx_ = -(rev_history.index(ws_index))
                if idx is None or idx_ > idx:
                    idx = idx_
        if idx is not None:
            if idx == 0:
                return tuple()
            else:
                return history[idx:]
        else:
            return history

    def compile(self, wordlist):
        for ws_index in self.ws_indices:
            self.to_numpy[tuple()][ws_index] = 1.0
        self.to_numpy[tuple()][self.eos_index] = 1.0
        for word in wordlist:
            word = tuple(self.vectorizer.map_to_index(word))
            for i in range(len(word)):
                history, current = word[:i], word[i]
                history = self._get_last_split(history)
                self.to_numpy[history][current] = 1.0
            if not self.force_padding:
                self.to_numpy[word][self.eos_index] = 1.0
            for ws_index in self.ws_indices:
                self.to_numpy[word][ws_index] = 1.0

    def call(self, history, preds):
        new_preds = np.zeros(preds.shape, dtype=preds.dtype)
        for i in it.product(*(range(j) for j in preds.shape[:-1])):
            word = self._get_last_split(tuple(history[i]))
            modifier = self.to_numpy[word]
            new_preds[i] = preds[i] * modifier
            # force the _whole sequence_ to be filtered out, by setting the
            # probability as close to -inf as possible (instead of just zero)
            new_preds[i] += (modifier == 0) * np.finfo(modifier.dtype).min
        return new_preds
