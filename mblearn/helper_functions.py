import numpy as np

def prestandardize_input_data(data, names):
    # this code is partly copied from:
    #   keras.engine.training.standardize_input_data()
    if type(data) is dict:
        arrays = []
        for name in names:
            if name not in data:
                raise Exception('No data provided for "' +
                                name + '". Need data for each key in: ' +
                                str(data.keys()))
            arrays.append(np.asarray(data[name]))
    elif type(data) is list:
        if len(data) != len(names):
            if len(data) > 0 and hasattr(data[0], 'shape'):
                raise Exception('Expected to see ' + str(len(names)) +
                                ' arrays but instead got ' + str(len(data)))
            else:
                if len(names) == 1:
                    data = [np.asarray(data)]
                else:
                    raise Exception('Expected a list of ' + str(len(names)) +
                                    ' numpy arrays but instead got a single list')
        arrays = [np.asarray(x) for x in data]
    else:
        if not hasattr(data, 'shape'):
            raise Exception('Data should be a Numpy array, '
                            'or list/dict of Numpy arrays. '
                            'Found: ' + str(data)[:200] + '...')
        if len(names) != 1:
            # case: model expects multiple inputs but only received
            # a single Numpy array
            raise Exception('The model expects ' + str(len(names)) +
                            ' input arrays, but only received one array. '
                            'Found: array with shape ' + str(data.shape))
        arrays = [np.asarray(data)]
    return arrays

def standardize_generic_input(data, names):
    if type(data) is dict:
        arrays = []
        for name in names:
            if name not in data:
                raise Exception('No data provided for "' +
                                name + '". Need data for each key in: ' +
                                str(data.keys()))
            arrays.append(data[name])
    elif type(data) is list:
        if len(data) != len(names):
            if len(names) == 1:
                data = [data]
            else:
                raise Exception('Expected a list of ' + str(len(names)) +
                                ' items but instead got ' + str(len(data)) + ' items')
        arrays = data
    else:
        arrays = [data] * len(names)
    return arrays

def get_sample_size(data):
    """Get the sample size for a list of model inputs.

    Sample size is the first dimension of model inputs, however, in some cases,
    we are dealing with inputs where not all these variables are set and it's
    unknown which of them are.  Hence, this function.
    """
    assert type(data) in (list, tuple)
    for x_i in data:
        if hasattr(x_i, 'shape') and x_i.shape[0] is not None:
            nb_samples = x_i.shape[0]
            break
    else:
        raise Exception("Could not derive sample size: no element of the "
                        "input list appears to be a numpy array")
    return nb_samples

def predict_classes(ndarray):
    # Keras's class prediction in a nutshell
    if ndarray.shape[-1] > 1:
        return ndarray.argmax(axis=-1)
    else:
        return (ndarray > 0.5).astype('int32')
