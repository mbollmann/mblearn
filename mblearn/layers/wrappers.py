from keras import backend as K
from keras.engine import InputSpec
from keras.layers import Bidirectional, TimeDistributed
from ..serialization import keras

@keras.custom_object
class BidirectionalHidden(Bidirectional):
    """Hack to make Bidirectional wrapper work with custom recurrent layers that
    return multiple outputs."""

    def _split_hidden(self, input_):
        if isinstance(input_, list) and len(input_) > 1 and len(input_) % 2 == 1:
            input_main, *input_hidden = input_
            cut = len(input_hidden) // 2
            input_fw = [input_main] + input_hidden[:cut]
            input_bw = [input_main] + input_hidden[cut:]
        else:
            input_fw, input_bw = input_, input_
        return (input_fw, input_bw)

    def call(self, X, mask=None):
        X_fw, X_bw = self._split_hidden(X)
        # call forward & backward layers
        Y, *Y_h = self.forward_layer.call(X_fw, mask)
        Y_rev, *Y_h_rev = self.backward_layer.call(X_bw, mask)
        # combine results depending on merge_mode
        if self.return_sequences:
            Y_rev = K.reverse(Y_rev, 1)
        if self.merge_mode == 'concat':
            ret = K.concatenate([Y, Y_rev])
            hidden = [K.concatenate([f, b]) for (f, b) in zip(Y_h, Y_h_rev)]
        elif self.merge_mode == 'sum':
            ret = Y + Y_rev
            hidden = [(f + b) for (f, b) in zip(Y_h, Y_h_rev)]
        elif self.merge_mode == 'ave':
            ret = (Y + Y_rev) / 2
            hidden = [(f + b) / 2 for (f, b) in zip(Y_h, Y_h_rev)]
        elif self.merge_mode == 'mul':
            ret = Y * Y_rev
            hidden = [(f * b) for (f, b) in zip(Y_h, Y_h_rev)]
        elif self.merge_mode is None:
            ret = [Y, Y_rev]
            hidden = Y_h + Y_h_rev
        return [ret] + hidden

    def get_output_shape_for(self, input_shape):
        input_shape, _ = self._split_hidden(input_shape)
        shape = self.forward_layer.get_output_shape_for(input_shape)
        if self.merge_mode == 'concat':
            shape = [(s[:-1] + (s[-1] * 2,)) for s in shape]
        elif self.merge_mode is None:
            raise Exception()
        return shape

    def compute_mask(self, input, input_mask=None):
        input, _ = self._split_hidden(input)
        return self.forward_layer.compute_mask(input, input_mask=input_mask)

@keras.custom_object
class TimeDistributedMulti(TimeDistributed):
    """Hack to make TimeDistributed wrapper work with multi-input/-output
    layers."""

    def build(self, input_shape):
        if type(input_shape) != list:
            input_shape = [input_shape]
        for shape in input_shape:
            assert len(shape) >= 3
        self.input_spec = [InputSpec(shape=shape) for shape in input_shape]
        if K._BACKEND == 'tensorflow':
            for shape in input_shape:
                if not shape[1]:
                    raise Exception('When using TensorFlow, you should define '
                                    'explicitly the number of timesteps of '
                                    'your sequences.\n'
                                    'If your first layer is an Embedding, '
                                    'make sure to pass it an "input_length" '
                                    'argument. Otherwise, make sure '
                                    'the first layer has '
                                    'an "input_shape" or "batch_input_shape" '
                                    'argument, including the time axis.')
        child_input_shape = [((shape[0],) + shape[2:]) for shape in input_shape]
        if len(input_shape) == 1:
            child_input_shape = child_input_shape[0]
        if not self.layer.built:
            self.layer.build(child_input_shape)
            self.layer.built = True
        super(TimeDistributed, self).build()

    def compute_mask(self, input, input_mask=None):
        return self.layer.compute_mask(input, input_mask=input_mask)

    def get_output_shape_for(self, input_shape):
        if type(input_shape) == list:
            child_input_shape = [((shape[0],) + shape[2:]) for shape in input_shape]
            timesteps = input_shape[0][1]
        else:
            child_input_shape = (input_shape[0],) + input_shape[2:]
            timesteps = input_shape[1]
        child_output_shape = self.layer.get_output_shape_for(child_input_shape)
        if type(child_output_shape) == list:
            output_shape = [((shape[0], timesteps) + shape[1:]) for shape in child_output_shape]
        else:
            output_shape = (child_output_shape[0], timesteps) + child_output_shape[1:]
        return output_shape

    def call(self, X, mask=None):
        if type(X) != list:
            X = [X]
        input_shapes = [K.int_shape(x) for x in X]
        batch_size = any(shape[0] is not None for shape in input_shapes)
        if batch_size:
            # batch size matters, use rnn-based implementation
            raise Exception("Can't use rnn-based implementation with TimeDistributedMulti")
        else:
            # no batch size specified, therefore the layer will be able
            # to process batches of any size
            # we can go with reshape-based implementation for performance
            input_length = input_shapes[0][1]
            if not input_length:
                input_length = K.shape(X[0])[1]
            X = [K.reshape(X[i], (-1, ) + input_shapes[i][2:]) for i in range(len(X))] # (nb_samples * timesteps, ...)
            if len(X) == 1:
                X = X[0]
                input_shape = input_shapes[0]
            else:
                input_shape = input_shapes
            y = self.layer.call(X)  # (nb_samples * timesteps, ...)
            # (nb_samples, timesteps, ...)
            output_shape = self.get_output_shape_for(input_shape)
            if type(output_shape) == list:
                y = [K.reshape(y[i], (-1, input_length) + output_shape[i][2:]) for i in range(len(y))]
            else:
                y = K.reshape(y, (-1, input_length) + output_shape[2:])

        # Apply activity regularizer if any:
        if hasattr(self.layer, 'activity_regularizer') and self.layer.activity_regularizer is not None:
            regularization_loss = self.layer.activity_regularizer(y)
            self.add_loss(regularization_loss, X)
        return y
