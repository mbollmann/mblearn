# Based on suggestions in:
# https://github.com/fchollet/keras/issues/2995

from keras import backend as K
from keras.activations import softmax
from keras.engine import InputSpec
from keras.layers import \
     activations, initializations, regularizers, time_distributed_dense
from keras.layers.recurrent import Recurrent, SimpleRNN, GRU, LSTM
from .. import aligners
from ..serialization import keras

def make_safe(x):
    return K.clip(x, K.common._EPSILON, 1.0 - K.common._EPSILON)

def get_hidden_state_rnn(RNN):
    """Convert a Recurrent subclass to a version with input/output capabilities
    for its hidden state.

    This layer behaves just like the provided Recurrent layer, except that it
    accepts further inputs to be used as its initial states, and returns
    additional outputs, representing the layer's final states.

    See Also:
        https://github.com/fchollet/keras/issues/2995
    """
    class HiddenStateRNN(RNN):
        def build(self, input_shape):
            if isinstance(input_shape, list) and len(input_shape) > 1:
                input_shape, *hidden_shapes = input_shape
                for shape in hidden_shapes:
                    assert shape[0]  == input_shape[0]
                    assert shape[-1] == self.output_dim
            super().build(input_shape)

        def call(self, x, mask=None):
            # input shape: (nb_samples, time (padded with zeros), input_dim)
            input_shape = self.input_spec[0].shape
            if isinstance(x, (tuple, list)):
                x, *custom_initial = x
            else:
                custom_initial = None
            if K._BACKEND == 'tensorflow':
                if not input_shape[1]:
                    raise Exception('When using TensorFlow, you should define '
                                    'explicitly the number of timesteps of '
                                    'your sequences.\n'
                                    'If your first layer is an Embedding, '
                                    'make sure to pass it an "input_length" '
                                    'argument. Otherwise, make sure '
                                    'the first layer has '
                                    'an "input_shape" or "batch_input_shape" '
                                    'argument, including the time axis. '
                                    'Found input shape at layer ' + self.name +
                                    ': ' + str(input_shape))
            if self.stateful and custom_initial:
                raise Exception(('Initial states should not be specified '
                                 'for stateful LSTMs, since they would overwrite '
                                 'the memorized states.'))
            elif custom_initial:
                initial_states = self.get_initial_states(x)
                for i, c_i in enumerate(custom_initial):
                    initial_states[i] = c_i
            elif self.stateful:
                initial_states = self.states
            else:
                initial_states = self.get_initial_states(x)
            constants = self.get_constants(x, custom_initial)
            preprocessed_input = self.preprocess_input(x)

            # only use the main input mask
            if isinstance(mask, list):
                mask = mask[0]

            last_output, outputs, states = K.rnn(self.step, preprocessed_input,
                                                 initial_states,
                                                 go_backwards=self.go_backwards,
                                                 mask=mask,
                                                 constants=constants,
                                                 unroll=self.unroll,
                                                 input_length=input_shape[1])
            if self.stateful:
                self.updates = []
                for i in range(len(states)):
                    self.updates.append((self.states[i], states[i]))

            if self.return_sequences:
                return [outputs] + states
            else:
                return [last_output] + states

        def get_output_shape_for(self, input_shape):
            if isinstance(input_shape, list) and len(input_shape) > 1:
                input_shape = input_shape[0]
            if self.return_sequences:
                output_shape = (input_shape[0], input_shape[1], self.output_dim)
            else:
                output_shape = (input_shape[0], self.output_dim)
            state_output = (input_shape[0], self.output_dim)
            return [output_shape] + [state_output] * len(self.states)

        def compute_mask(self, input, input_mask=None):
            if isinstance(input_mask, list):
                if len(input_mask) == len(self.states) + 1:
                    return input_mask
                else:
                    return input_mask + [None] * (len(self.states) - len(input_mask) + 1)
            elif self.return_sequences:
                return [input_mask] + [None] * len(self.states)
            else:
                return [None] * (len(self.states) + 1)

        def get_constants(self, x, custom_initial):
            return super().get_constants(x)

    HiddenStateRNN.__name__ = "HiddenState{}".format(RNN.__name__)
    return HiddenStateRNN

HiddenStateSimpleRNN = keras.custom_object(get_hidden_state_rnn(SimpleRNN))
HiddenStateGRU = keras.custom_object(get_hidden_state_rnn(GRU))
HiddenStateLSTM = keras.custom_object(get_hidden_state_rnn(LSTM))

@keras.custom_object
class HiddenToCellStateLSTM(HiddenStateLSTM):
    """HiddenStateLSTM that additionally learns weights to factor in the
    supplied hidden state at every timestep, instead of just setting the initial
    state with it.
    """

    def get_constants(self, x, custom_initial):
        constants = super().get_constants(x, custom_initial)
        if custom_initial is not None:
            for c in custom_initial:
                constants.append(c)
        return constants

    def build(self, input_shape):
        super().build(input_shape)
        if not isinstance(input_shape, list) or len(input_shape) <= 1:
            # we're called without custom initial states
            return
        if self.consume_less == 'gpu':
            self.C = self.inner_init((self.output_dim, 4 * self.output_dim),
                                     name='{}_C'.format(self.name))
            self.trainable_weights += [self.C]
        else:
            self.C_i = self.inner_init((self.output_dim, self.output_dim),
                                       name='{}_C_i'.format(self.name))
            self.C_f = self.inner_init((self.output_dim, self.output_dim),
                                       name='{}_C_f'.format(self.name))
            self.C_c = self.inner_init((self.output_dim, self.output_dim),
                                       name='{}_C_c'.format(self.name))
            self.C_o = self.inner_init((self.output_dim, self.output_dim),
                                       name='{}_C_o'.format(self.name))
            self.trainable_weights += [self.C_i, self.C_f, self.C_c, self.C_o]
            self.C = K.concatenate([self.C_i, self.C_f, self.C_c, self.C_o])

    def step(self, x, states):
        if len(states) == 4:
            # we're called without custom initial states
            return super().step(x, states)

        h_tm1 = states[0]
        c_tm1 = states[1]
        B_U = states[2]
        B_W = states[3]
        h_initial = states[4]

        if self.consume_less == 'gpu':
            z = K.dot(x * B_W[0], self.W) + K.dot(h_tm1 * B_U[0], self.U) + self.b
            z += K.dot(h_initial * B_U[0], self.C)

            z0 = z[:, :self.output_dim]
            z1 = z[:, self.output_dim: 2 * self.output_dim]
            z2 = z[:, 2 * self.output_dim: 3 * self.output_dim]
            z3 = z[:, 3 * self.output_dim:]

            i = self.inner_activation(z0)
            f = self.inner_activation(z1)
            c = f * c_tm1 + i * self.activation(z2)
            o = self.inner_activation(z3)
        else:
            if self.consume_less == 'cpu':
                x_i = x[:, :self.output_dim]
                x_f = x[:, self.output_dim: 2 * self.output_dim]
                x_c = x[:, 2 * self.output_dim: 3 * self.output_dim]
                x_o = x[:, 3 * self.output_dim:]
            elif self.consume_less == 'mem':
                x_i = K.dot(x * B_W[0], self.W_i) + self.b_i
                x_f = K.dot(x * B_W[1], self.W_f) + self.b_f
                x_c = K.dot(x * B_W[2], self.W_c) + self.b_c
                x_o = K.dot(x * B_W[3], self.W_o) + self.b_o
            else:
                raise Exception('Unknown `consume_less` mode.')

            i = self.inner_activation(x_i + K.dot(h_tm1 * B_U[0], self.U_i) + K.dot(h_initial * B_U[0], self.C_i))
            f = self.inner_activation(x_f + K.dot(h_tm1 * B_U[1], self.U_f) + K.dot(h_initial * B_U[0], self.C_f))
            c = f * c_tm1 + i * self.activation(x_c + K.dot(h_tm1 * B_U[2], self.U_c) + K.dot(h_initial * B_U[0], self.C_c))
            o = self.inner_activation(x_o + K.dot(h_tm1 * B_U[3], self.U_o) + K.dot(h_initial * B_U[0], self.C_o))

        h = o * self.activation(c)
        return h, [h, c]



@keras.custom_object
class AttentionLSTM(LSTM):
    """LSTM with attention mechanism

    This is an LSTM incorporating an attention mechanism into its hidden states.
    The attention component feeds the context vector calculated from the
    attended vector into the model's internal states, closely following the
    model by Xu et al. (2016, Sec. 3.1.2), using a soft attention model
    following Bahdanau et al. (2014).

    The layer expects two inputs instead of the usual one:
        1. the "normal" layer input; and
        2. a 3D vector to attend.

    Args:
        aligner: Alignment model to use
        attn_activation: Activation function for attentional components
        attn_init: Initialization function for attention weights
        output_alpha (boolean): If true, outputs the alpha values, i.e.,
            what parts of the attention vector the layer attends to at each
            timestep.

    References:
        * Bahdanau, Cho & Bengio (2014), "Neural Machine Translation by Jointly
          Learning to Align and Translate", <https://arxiv.org/pdf/1409.0473.pdf>
        * Xu, Ba, Kiros, Cho, Courville, Salakhutdinov, Zemel & Bengio (2016),
          "Show, Attend and Tell: Neural Image Caption Generation with Visual
          Attention", <http://arxiv.org/pdf/1502.03044.pdf>

    See Also:
        `LSTM`_ in the Keras documentation.

        .. _LSTM: http://keras.io/layers/recurrent/#lstm
    """
    def __init__(self, *args, aligner='global',
                 attn_activation='tanh', attn_init='orthogonal',
                 output_alpha=False, **kwargs):
        self.attn_activation = activations.get(attn_activation)
        self.attn_init = initializations.get(attn_init)
        self.aligner = aligners.get(aligner)(self)
        self.output_alpha = output_alpha
        super().__init__(*args, **kwargs)

    def build(self, input_shape):
        if not (isinstance(input_shape, list) and len(input_shape) == 2):
            raise Exception('Input to AttentionLSTM must be a list of '
                            'two tensors [lstm_input, attn_input].')

        input_shape, attn_input_shape = input_shape
        super().build(input_shape)
        self.input_spec.append(InputSpec(shape=attn_input_shape))

        # weights for alignment model
        attn_weights = self.aligner.build(input_shape, attn_input_shape)
        self.trainable_weights += attn_weights

        # weights for incorporating attention into hidden states
        if self.consume_less == 'gpu':
            self.Z = self.attn_init((attn_input_shape[-1], 4 * self.output_dim),
                                    name='{}_Z'.format(self.name))
            self.trainable_weights += [self.Z]
        else:
            self.Z_i = self.attn_init((attn_input_shape[-1], self.output_dim),
                                      name='{}_Z_i'.format(self.name))
            self.Z_f = self.attn_init((attn_input_shape[-1], self.output_dim),
                                      name='{}_Z_f'.format(self.name))
            self.Z_c = self.attn_init((attn_input_shape[-1], self.output_dim),
                                      name='{}_Z_c'.format(self.name))
            self.Z_o = self.attn_init((attn_input_shape[-1], self.output_dim),
                                      name='{}_Z_o'.format(self.name))
            self.trainable_weights += [self.Z_i, self.Z_f, self.Z_c, self.Z_o]
            self.Z = K.concatenate([self.Z_i, self.Z_f, self.Z_c, self.Z_o])

        # weights for initializing states based on attention vector
        if not self.stateful:
            self.W_init_c = self.attn_init((attn_input_shape[-1], self.output_dim),
                                           name='{}_W_init_c'.format(self.name))
            self.W_init_h = self.attn_init((attn_input_shape[-1], self.output_dim),
                                           name='{}_W_init_h'.format(self.name))
            self.b_init_c = K.zeros((self.output_dim,),
                                      name='{}_b_init_c'.format(self.name))
            self.b_init_h = K.zeros((self.output_dim,),
                                      name='{}_b_init_h'.format(self.name))
            self.trainable_weights += [self.W_init_c, self.b_init_c,
                                       self.W_init_h, self.b_init_h]

        if self.initial_weights is not None:
            self.set_weights(self.initial_weights)
            del self.initial_weights

    def get_output_shape_for(self, input_shape):
        # output shape is not affected by the attention component
        return super().get_output_shape_for(input_shape[0])

    def compute_mask(self, input, input_mask=None):
        if input_mask is not None:
            input_mask = input_mask[0]
        return super().compute_mask(input, input_mask)

    def get_initial_states(self, x_input, x_attn, mask_attn):
        # set initial states from mean attention vector fed through a dense
        # activation
        xm_attn = x_attn if mask_attn is None else x_attn * K.expand_dims(mask_attn)
        mean_attn = K.mean(xm_attn, axis=1)
        h0 = K.dot(mean_attn, self.W_init_h) + self.b_init_h
        c0 = K.dot(mean_attn, self.W_init_c) + self.b_init_c
        initial_states = [self.attn_activation(h0), self.attn_activation(c0)]
        aligner_states = self.aligner.get_initial_states(x_input, x_attn, mask_attn)
        if aligner_states:
            initial_states += aligner_states
        return initial_states

    def call(self, x, mask=None):
        assert isinstance(x, list) and len(x) == 2
        x_input, x_attn = x
        if mask is not None:
            mask_input, mask_attn = mask
        else:
            mask_input, mask_attn = None, None
        # input shape: (nb_samples, time (padded with zeros), input_dim)
        input_shape = self.input_spec[0].shape
        if K._BACKEND == 'tensorflow':
            if not input_shape[1]:
                raise Exception('When using TensorFlow, you should define '
                                'explicitly the number of timesteps of '
                                'your sequences.\n'
                                'If your first layer is an Embedding, '
                                'make sure to pass it an "input_length" '
                                'argument. Otherwise, make sure '
                                'the first layer has '
                                'an "input_shape" or "batch_input_shape" '
                                'argument, including the time axis. '
                                'Found input shape at layer ' + self.name +
                                ': ' + str(input_shape))
        if self.stateful:
            initial_states = self.states
        else:
            initial_states = self.get_initial_states(x_input, x_attn, mask_attn)
        constants = self.get_constants(x_input, x_attn, mask_attn)
        preprocessed_input = self.preprocess_input(x_input)

        last_output, outputs, states = K.rnn(self.step, preprocessed_input,
                                             initial_states,
                                             go_backwards=self.go_backwards,
                                             mask=mask_input,
                                             constants=constants,
                                             unroll=self.unroll,
                                             input_length=input_shape[1])
        if self.stateful:
            self.updates = []
            for i in range(len(states)):
                self.updates.append((self.states[i], states[i]))

        if self.return_sequences:
            return outputs
        else:
            return last_output

    def step(self, x, states):
        h_tm1 = states[0]
        c_tm1 = states[1]
        i, aligner_states = 2, []
        for j in range(self.aligner.states):
            aligner_states.append(states[i+j])
        i += self.aligner.states
        B_U = states[i]
        B_W = states[i+1]
        x_attn = states[i+2]
        mask_attn = states[i+3] if len(states) > i+3 else None
        attn_shape = self.input_spec[1].shape

        # alignment model
        alpha, aligner_states = self.aligner.align(h_tm1, x_attn, attn_shape,
                                                   mask_attn=mask_attn,
                                                   states=aligner_states)
        alpha_r = K.repeat(alpha, attn_shape[2])
        alpha_r = K.permute_dimensions(alpha_r, (0, 2, 1))
        # make context vector -- soft attention after Bahdanau et al.
        z_hat = x_attn * alpha_r
        z_hat = K.sum(z_hat, axis=1)

        if self.consume_less == 'gpu':
            z = K.dot(x * B_W[0], self.W) + K.dot(h_tm1 * B_U[0], self.U) \
                + K.dot(z_hat, self.Z) + self.b

            z0 = z[:, :self.output_dim]
            z1 = z[:, self.output_dim: 2 * self.output_dim]
            z2 = z[:, 2 * self.output_dim: 3 * self.output_dim]
            z3 = z[:, 3 * self.output_dim:]
        else:
            if self.consume_less == 'cpu':
                x_i = x[:, :self.output_dim]
                x_f = x[:, self.output_dim: 2 * self.output_dim]
                x_c = x[:, 2 * self.output_dim: 3 * self.output_dim]
                x_o = x[:, 3 * self.output_dim:]
            elif self.consume_less == 'mem':
                x_i = K.dot(x * B_W[0], self.W_i) + self.b_i
                x_f = K.dot(x * B_W[1], self.W_f) + self.b_f
                x_c = K.dot(x * B_W[2], self.W_c) + self.b_c
                x_o = K.dot(x * B_W[3], self.W_o) + self.b_o
            else:
                raise Exception('Unknown `consume_less` mode.')

            z0 = x_i + K.dot(h_tm1 * B_U[0], self.U_i) + K.dot(z_hat, self.Z_i)
            z1 = x_f + K.dot(h_tm1 * B_U[1], self.U_f) + K.dot(z_hat, self.Z_f)
            z2 = x_c + K.dot(h_tm1 * B_U[2], self.U_c) + K.dot(z_hat, self.Z_c)
            z3 = x_o + K.dot(h_tm1 * B_U[3], self.U_o) + K.dot(z_hat, self.Z_o)

        i = self.inner_activation(z0)
        f = self.inner_activation(z1)
        c = f * c_tm1 + i * self.activation(z2)
        o = self.inner_activation(z3)

        h = o * self.activation(c)
        if self.output_alpha:
            return alpha, [h, c] + aligner_states
        else:
            return h, [h, c] + aligner_states

    def get_constants(self, x_input, x_attn, mask_attn):
        constants = super().get_constants(x_input)
        attn_shape = self.input_spec[1].shape
        constants.append(x_attn)
        if mask_attn is not None:
            if K.ndim(mask_attn) == 3:
                mask_attn = K.all(mask_attn, axis=-1)
            constants.append(mask_attn)
        return constants

    def get_config(self):
        cfg = super().get_config()
        cfg['output_alpha'] = self.output_alpha
        cfg['attn_activation'] = self.attn_activation.__name__
        cfg['attn_init'] = self.attn_init.__name__
        cfg['aligner'] = self.aligner.get_config()
        return cfg

    @classmethod
    def from_config(cls, config):
        instance = super(AttentionLSTM, cls).from_config(config)
        if 'output_alpha' in config:
            instance.output_alpha = config['output_alpha']
        if 'attn_activation' in config:
            instance.attn_activation = activations.get(config['attn_activation'])
        if 'attn_init' in config:
            instance.attn_init = initializations.get(config['attn_init'])
        if 'aligner' in config:
            instance.aligner = aligners.from_config(config['aligner'], instance)
        return instance


@keras.custom_object
class AttentionGRU(GRU):
    """Like AttentionLSTM, but for GRU.

    This actually shares a lot of code with the AttentionLSTM, so maybe we
    should refactor this to avoid duplication.
    """

    def __init__(self, *args, aligner='global',
                 attn_activation='tanh', attn_init='orthogonal',
                 output_alpha=False, **kwargs):
        self.attn_activation = activations.get(attn_activation)
        self.attn_init = initializations.get(attn_init)
        self.aligner = aligners.get(aligner)(self)
        self.output_alpha = output_alpha
        super().__init__(*args, **kwargs)

    def build(self, input_shape):
        if not (isinstance(input_shape, list) and len(input_shape) == 2):
            raise Exception('Input to AttentionGRU must be a list of '
                            'two tensors [lstm_input, attn_input].')

        input_shape, attn_input_shape = input_shape
        super().build(input_shape)
        self.input_spec.append(InputSpec(shape=attn_input_shape))

        # weights for alignment model
        attn_weights = self.aligner.build(input_shape, attn_input_shape)
        self.trainable_weights += attn_weights

        # weights for incorporating attention into hidden states
        if self.consume_less == 'gpu':
            self.Z = self.init((attn_input_shape[-1], 3 * self.output_dim),
                               name='{}_Z'.format(self.name))
            self.trainable_weights += [self.Z]
        else:
            self.Z_z = self.attn_init((attn_input_shape[-1], self.output_dim),
                                      name='{}_Z_z'.format(self.name))
            self.Z_r = self.attn_init((attn_input_shape[-1], self.output_dim),
                                      name='{}_Z_r'.format(self.name))
            self.Z_h = self.attn_init((attn_input_shape[-1], self.output_dim),
                                      name='{}_Z_h'.format(self.name))
            self.trainable_weights += [self.Z_z, self.Z_r, self.Z_h]
            self.Z = K.concatenate([self.Z_z, self.Z_r, self.Z_h])

        # weights for initializing states based on attention vector
        if not self.stateful:
            self.W_init_h = self.attn_init((attn_input_shape[-1], self.output_dim),
                                           name='{}_W_init_h'.format(self.name))
            self.b_init_h = K.zeros((self.output_dim,),
                                    name='{}_b_init_h'.format(self.name))
            self.trainable_weights += [self.W_init_h, self.b_init_h]

        if self.initial_weights is not None:
            self.set_weights(self.initial_weights)
            del self.initial_weights
        self.built = True

    def get_output_shape_for(self, input_shape):
        # output shape is not affected by the attention component
        return super().get_output_shape_for(input_shape[0])

    def compute_mask(self, input, input_mask=None):
        if input_mask is not None:
            input_mask = input_mask[0]
        return super().compute_mask(input, input_mask)

    def get_initial_states(self, x_input, x_attn, mask_attn):
        # set initial states from mean attention vector fed through a dense
        # activation
        xm_attn = x_attn if mask_attn is None else x_attn * K.expand_dims(mask_attn)
        mean_attn = K.mean(xm_attn, axis=1)
        h0 = K.dot(mean_attn, self.W_init_h) + self.b_init_h
        initial_states = [self.attn_activation(h0)]
        aligner_states = self.aligner.get_initial_states(x_input, x_attn, mask_attn)
        if aligner_states:
            initial_states += aligner_states
        return initial_states

    def call(self, x, mask=None):
        assert isinstance(x, list) and len(x) == 2
        x_input, x_attn = x
        if mask is not None:
            mask_input, mask_attn = mask
        else:
            mask_input, mask_attn = None, None
        # input shape: (nb_samples, time (padded with zeros), input_dim)
        input_shape = self.input_spec[0].shape
        if K._BACKEND == 'tensorflow':
            if not input_shape[1]:
                raise Exception('When using TensorFlow, you should define '
                                'explicitly the number of timesteps of '
                                'your sequences.\n'
                                'If your first layer is an Embedding, '
                                'make sure to pass it an "input_length" '
                                'argument. Otherwise, make sure '
                                'the first layer has '
                                'an "input_shape" or "batch_input_shape" '
                                'argument, including the time axis. '
                                'Found input shape at layer ' + self.name +
                                ': ' + str(input_shape))
        if self.stateful:
            initial_states = self.states
        else:
            initial_states = self.get_initial_states(x_input, x_attn, mask_attn)
        constants = self.get_constants(x_input, x_attn, mask_attn)
        preprocessed_input = self.preprocess_input(x_input)

        last_output, outputs, states = K.rnn(self.step, preprocessed_input,
                                             initial_states,
                                             go_backwards=self.go_backwards,
                                             mask=mask_input,
                                             constants=constants,
                                             unroll=self.unroll,
                                             input_length=input_shape[1])
        if self.stateful:
            self.updates = []
            for i in range(len(states)):
                self.updates.append((self.states[i], states[i]))

        if self.return_sequences:
            return outputs
        else:
            return last_output

    def step(self, x, states):
        h_tm1 = states[0]  # previous memory
        i, aligner_states = 1, []
        for j in range(self.aligner.states):
            aligner_states.append(states[i+j])
        i += self.aligner.states
        B_U = states[i]  # dropout matrices for recurrent units
        B_W = states[i+1]
        x_attn = states[i+2]
        mask_attn = states[i+3] if len(states) > i+3 else None
        attn_shape = self.input_spec[1].shape

        # alignment model
        alpha, aligner_states = self.aligner.align(h_tm1, x_attn, attn_shape,
                                                   mask_attn=mask_attn,
                                                   states=aligner_states)
        alpha_r = K.repeat(alpha, attn_shape[2])
        alpha_r = K.permute_dimensions(alpha_r, (0, 2, 1))
        # make context vector -- soft attention after Bahdanau et al.
        z_hat = x_attn * alpha_r
        z_hat = K.sum(z_hat, axis=1)

        if self.consume_less == 'gpu':
            matrix_x = K.dot(x * B_W[0], self.W) + K.dot(z_hat, self.Z) + self.b
            matrix_inner = K.dot(h_tm1 * B_U[0], self.U[:, :2 * self.output_dim])

            x_z = matrix_x[:, :self.output_dim]
            x_r = matrix_x[:, self.output_dim: 2 * self.output_dim]
            inner_z = matrix_inner[:, :self.output_dim]
            inner_r = matrix_inner[:, self.output_dim: 2 * self.output_dim]

            z = self.inner_activation(x_z + inner_z)
            r = self.inner_activation(x_r + inner_r)

            x_h = matrix_x[:, 2 * self.output_dim:]
            inner_h = K.dot(r * h_tm1 * B_U[0], self.U[:, 2 * self.output_dim:])
            hh = self.activation(x_h + inner_h)
        else:
            if self.consume_less == 'cpu':
                x_z = x[:, :self.output_dim]
                x_r = x[:, self.output_dim: 2 * self.output_dim]
                x_h = x[:, 2 * self.output_dim:]
            elif self.consume_less == 'mem':
                x_z = K.dot(x * B_W[0], self.W_z) + self.b_z
                x_r = K.dot(x * B_W[1], self.W_r) + self.b_r
                x_h = K.dot(x * B_W[2], self.W_h) + self.b_h
            else:
                raise ValueError('Unknown `consume_less` mode.')
            z = self.inner_activation(x_z + K.dot(h_tm1 * B_U[0], self.U_z) + K.dot(z_hat, self.Z_z))
            r = self.inner_activation(x_r + K.dot(h_tm1 * B_U[1], self.U_r) + K.dot(z_hat, self.Z_r))

            hh = self.activation(x_h + K.dot(r * h_tm1 * B_U[2], self.U_h) + K.dot(z_hat, self.Z_h))
        h = z * h_tm1 + (1 - z) * hh
        if self.output_alpha:
            return alpha, [h] + aligner_states
        else:
            return h, [h] + aligner_states

    def get_constants(self, x_input, x_attn, mask_attn):
        constants = super().get_constants(x_input)
        attn_shape = self.input_spec[1].shape
        constants.append(x_attn)
        if mask_attn is not None:
            if K.ndim(mask_attn) == 3:
                mask_attn = K.all(mask_attn, axis=-1)
            constants.append(mask_attn)
        return constants

    def get_config(self):
        cfg = super().get_config()
        cfg['output_alpha'] = self.output_alpha
        cfg['attn_activation'] = self.attn_activation.__name__
        cfg['attn_init'] = self.attn_init.__name__
        cfg['aligner'] = self.aligner.get_config()
        return cfg

    @classmethod
    def from_config(cls, config):
        instance = super(AttentionGRU, cls).from_config(config)
        if 'output_alpha' in config:
            instance.output_alpha = config['output_alpha']
        if 'attn_activation' in config:
            instance.attn_activation = activations.get(config['attn_activation'])
        if 'attn_init' in config:
            instance.attn_init = initializations.get(config['attn_init'])
        if 'aligner' in config:
            instance.aligner = aligners.from_config(config['aligner'], instance)
        return instance


@keras.custom_object
class RAN(Recurrent):
    """Recurrent Additive Network -- Lee et al. 2017

    # Arguments
        output_dim: dimension of the internal projections and the final output.
        init: weight initialization function.
            Can be the name of an existing function (str),
            or a Theano function (see: [initializations](../initializations.md)).
        inner_init: initialization function of the inner cells.
        activation: activation function.
            Can be the name of an existing function (str),
            or a Theano function (see: [activations](../activations.md)).
        inner_activation: activation function for the inner cells.
        W_regularizer: instance of [WeightRegularizer](../regularizers.md)
            (eg. L1 or L2 regularization), applied to the input weights matrices.
        U_regularizer: instance of [WeightRegularizer](../regularizers.md)
            (eg. L1 or L2 regularization), applied to the recurrent weights matrices.
        b_regularizer: instance of [WeightRegularizer](../regularizers.md),
            applied to the bias.
        dropout_W: float between 0 and 1. Fraction of the input units to drop for input gates.
        dropout_U: float between 0 and 1. Fraction of the input units to drop for recurrent connections.

    # References
        - [Recurrent Additive Networks](http://www.kentonl.com/pub/llz.2017.pdf)
    """

    def __init__(self, output_dim,
                 init='glorot_uniform', inner_init='orthogonal',
                 activation='linear', inner_activation='hard_sigmoid',
                 W_regularizer=None, U_regularizer=None, b_regularizer=None,
                 dropout_W=0., dropout_U=0., **kwargs):
        self.output_dim = output_dim
        self.init = initializations.get(init)
        self.inner_init = initializations.get(inner_init)
        self.activation = activations.get(activation)
        self.inner_activation = activations.get(inner_activation)
        self.W_regularizer = regularizers.get(W_regularizer)
        self.U_regularizer = regularizers.get(U_regularizer)
        self.b_regularizer = regularizers.get(b_regularizer)
        self.dropout_W = dropout_W
        self.dropout_U = dropout_U

        if self.dropout_W or self.dropout_U:
            self.uses_learning_phase = True
        super(RAN, self).__init__(**kwargs)

    def build(self, input_shape):
        self.input_spec = [InputSpec(shape=input_shape)]
        self.input_dim = input_shape[2]

        if self.stateful:
            self.reset_states()
        else:
            # initial states: all-zero tensor of shape (output_dim)
            self.states = [None, None]

        if self.consume_less == 'gpu':
            self.W = self.add_weight((self.input_dim, 3 * self.output_dim),
                                     initializer=self.init,
                                     name='{}_W'.format(self.name),
                                     regularizer=self.W_regularizer)
            self.U = self.add_weight((self.output_dim, 2 * self.output_dim),
                                     initializer=self.inner_init,
                                     name='{}_U'.format(self.name),
                                     regularizer=self.U_regularizer)
            self.b = self.add_weight((self.output_dim * 2,),
                                     initializer='zero',
                                     name='{}_b'.format(self.name),
                                     regularizer=self.b_regularizer)
        else:
            self.W_i = self.add_weight((self.input_dim, self.output_dim),
                                       initializer=self.init,
                                       name='{}_W_i'.format(self.name),
                                       regularizer=self.W_regularizer)
            self.U_i = self.add_weight((self.output_dim, self.output_dim),
                                       initializer=self.init,
                                       name='{}_U_i'.format(self.name),
                                       regularizer=self.W_regularizer)
            self.b_i = self.add_weight((self.output_dim,),
                                       initializer='zero',
                                       name='{}_b_i'.format(self.name),
                                       regularizer=self.b_regularizer)
            self.W_f = self.add_weight((self.input_dim, self.output_dim),
                                       initializer=self.init,
                                       name='{}_W_f'.format(self.name),
                                       regularizer=self.W_regularizer)
            self.U_f = self.add_weight((self.output_dim, self.output_dim),
                                       initializer=self.init,
                                       name='{}_U_f'.format(self.name),
                                       regularizer=self.W_regularizer)
            self.b_f = self.add_weight((self.output_dim,),
                                       initializer='zero',
                                       name='{}_b_f'.format(self.name),
                                       regularizer=self.b_regularizer)
            self.W_c = self.add_weight((self.input_dim, self.output_dim),
                                       initializer=self.init,
                                       name='{}_W_c'.format(self.name),
                                       regularizer=self.W_regularizer)
            self.W = K.concatenate([self.W_c, self.W_i, self.W_f])
            self.U = K.concatenate([self.U_i, self.U_f])
            self.b = K.concatenate([self.b_i, self.b_f])

        if self.initial_weights is not None:
            self.set_weights(self.initial_weights)
            del self.initial_weights
        self.built = True


    def reset_states(self):
        assert self.stateful, 'Layer must be stateful.'
        input_shape = self.input_spec[0].shape
        if not input_shape[0]:
            raise ValueError('If a RNN is stateful, a complete ' +
                             'input_shape must be provided (including batch size).')
        if hasattr(self, 'states'):
            K.set_value(self.states[0],
                        np.zeros((input_shape[0], self.output_dim)))
            K.set_value(self.states[1],
                        np.zeros((input_shape[0], self.output_dim)))
        else:
            self.states = [K.zeros((input_shape[0], self.output_dim)),
                           K.zeros((input_shape[0], self.output_dim))]

    def preprocess_input(self, x):
        if self.consume_less == 'cpu':
            input_shape = K.int_shape(x)
            input_dim = input_shape[2]
            timesteps = input_shape[1]

            x_c = time_distributed_dense(x, self.W_c, None, self.dropout_W,
                                         input_dim, self.output_dim, timesteps)
            x_i = time_distributed_dense(x, self.W_i, self.b_i, self.dropout_W,
                                         input_dim, self.output_dim, timesteps)
            x_f = time_distributed_dense(x, self.W_f, self.b_f, self.dropout_W,
                                         input_dim, self.output_dim, timesteps)
            return K.concatenate([x_c, x_i, x_f], axis=2)
        else:
            return x

    def step(self, x, states):
        h_tm1 = states[0]
        c_tm1 = states[1]
        B_U = states[2]
        B_W = states[3]

        if self.consume_less == 'gpu':
            matrix_x = K.dot(x * B_W[0], self.W)
            x_c = matrix_x[:, :self.output_dim]
            x_r = matrix_x[:, self.output_dim:]

            z = x_r + K.dot(h_tm1 * B_U[0], self.U) + self.b
            z0 = z[:, :self.output_dim]
            z1 = z[:, self.output_dim:]

            i = self.inner_activation(z0)
            f = self.inner_activation(z1)
        else:
            if self.consume_less == 'cpu':
                x_c = x[:, :self.output_dim]
                x_i = x[:, self.output_dim: 2 * self.output_dim]
                x_f = x[:, 2 * self.output_dim:]
            elif self.consume_less == 'mem':
                x_c = K.dot(x * B_W[0], self.W_c)
                x_i = K.dot(x * B_W[1], self.W_i) + self.b_i
                x_f = K.dot(x * B_W[2], self.W_f) + self.b_f
            else:
                raise ValueError('Unknown `consume_less` mode.')
            i = self.inner_activation(x_i + K.dot(h_tm1 * B_U[0], self.U_i))
            f = self.inner_activation(x_f + K.dot(h_tm1 * B_U[1], self.U_f))

        c = f * c_tm1 + i * x_c
        h = self.activation(c)
        return h, [h, c]

    def get_constants(self, x):
        constants = []
        if 0 < self.dropout_U < 1:
            ones = K.ones_like(K.reshape(x[:, 0, 0], (-1, 1)))
            ones = K.tile(ones, (1, self.output_dim))
            B_U = [K.in_train_phase(K.dropout(ones, self.dropout_U), ones) for _ in range(2)]
            constants.append(B_U)
        else:
            constants.append([K.cast_to_floatx(1.) for _ in range(2)])

        if 0 < self.dropout_W < 1:
            input_shape = K.int_shape(x)
            input_dim = input_shape[-1]
            ones = K.ones_like(K.reshape(x[:, 0, 0], (-1, 1)))
            ones = K.tile(ones, (1, int(input_dim)))
            B_W = [K.in_train_phase(K.dropout(ones, self.dropout_W), ones) for _ in range(3)]
            constants.append(B_W)
        else:
            constants.append([K.cast_to_floatx(1.) for _ in range(3)])
        return constants

    def get_config(self):
        config = {'output_dim': self.output_dim,
                  'init': self.init.__name__,
                  'inner_init': self.inner_init.__name__,
                  'activation': self.activation.__name__,
                  'inner_activation': self.inner_activation.__name__,
                  'W_regularizer': self.W_regularizer.get_config() if self.W_regularizer else None,
                  'U_regularizer': self.U_regularizer.get_config() if self.U_regularizer else None,
                  'b_regularizer': self.b_regularizer.get_config() if self.b_regularizer else None,
                  'dropout_W': self.dropout_W,
                  'dropout_U': self.dropout_U}
        base_config = super(RAN, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

HiddenStateRAN = keras.custom_object(get_hidden_state_rnn(RAN))
