from keras import backend as K
from keras.engine.topology import Layer
from ..serialization import keras

@keras.custom_object
class TimeCutoff(Layer):
    """Layer that returns only the last n timesteps from its input."""
    def __init__(self, timesteps, **kwargs):
        self.cutoff_timesteps = int(timesteps)
        super().__init__(**kwargs)

    def call(self, x, mask=None):
        return x[:, self.cutoff_timesteps:]

    def get_output_shape_for(self, input_shape):
        new_timesteps = input_shape[1] - self.cutoff_timesteps
        return (input_shape[0], new_timesteps) + input_shape[2:]

    def compute_mask(self, inputs, input_mask=None):
        if input_mask is None:
            return None
        return input_mask[:, self.cutoff_timesteps:]

    def get_config(self):
        return {'name': self.name,
                'timesteps': self.cutoff_timesteps}

    @classmethod
    def from_config(cls, config):
        return super(TimeCutoff, cls).from_config(config)
