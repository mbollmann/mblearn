from keras import backend as K
from keras.layers import Masking
from ..serialization import keras

@keras.custom_object
class MaskingFrom(Masking):
    """Layer that passes through its first input, taking the mask from the second input."""

    def build(self, input_shape):
        assert isinstance(input_shape, list) and len(input_shape) == 2
        assert input_shape[0][:-1] == input_shape[1][:-1]
        super().build(input_shape)

    def compute_mask(self, x, input_mask=None):
        return K.any(K.not_equal(x[1], self.mask_value), axis=-1)

    def call(self, x, mask=None):
        boolean_mask = K.any(K.not_equal(x[1], self.mask_value),
                             axis=-1, keepdims=True)
        return x[0] * K.cast(boolean_mask, K.floatx())

    def get_output_shape_for(self, input_shape):
        return input_shape[0]
