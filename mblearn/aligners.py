from keras import backend as K
from keras.layers import time_distributed_dense

class AlignmentModel:
    """Abstract class for alignment models for attentional RNNs.

    These classes are not intended to be used as Keras layers, but as arguments
    for layers that support attentional components, such as AttentionLSTM.
    """

    def __init__(self, parent):
        self.parent = parent
        self.states = 0

    def __call__(self, parent):
        self.parent = parent

    @property
    def name(self):
        return self.__class__.__name__

    def align(self, h_tm1, x_attn, attn_shape, mask_attn=None, states=None):
        """Calculate alignment, return attention weights."""
        raise NotImplementedError()

    def build(self, input_shape, attn_input_shape):
        """Adds trainable weights to the parent layer and returns a list of them."""
        return []

    def get_initial_states(self, x_input, x_attn, mask_attn):
        # should return a number of list items equal to self.states
        return []

    def get_config(self):
        return {'name': self.name,
                'kwargs': {}}


class GlobalAlignment(AlignmentModel):
    """Global alignment model.

    Arguments:
        * score_fn: the scoring function; can be:
              * 'dot', simple dot product of input and hidden state
              * 'concat', output of MLP on the concatenation of input and
                hidden state
        * position_bias: if true, uses a position bias as described in Sec. 3.1
          of Cohn et al. (2017)

    References:
        * Cohn et al. (2017), "Incorporating Structural Alignment Biases into
          an Attentional Neural Translation Model",
          <http://www.aclweb.org/anthology/N16-1102>
    """
    def __init__(self, parent, *args, score_fn='concat',
                 position_bias=False, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.score_fn = score_fn
        self.position_bias = position_bias
        if self.position_bias:
            self.states += 1
            if self.score_fn != "concat":
                raise NotImplementedError("position_bias requires score_fn='concat'")

    def build(self, input_shape, attn_input_shape):
        parent = self.parent
        if self.score_fn == 'dot':
            if parent.output_dim != attn_input_shape[-1]:
                raise Exception(("Dot product requires matching dimensions "
                                 "of attention layer and feature dimension "
                                 "of the attended input ({} != {})").format(
                                     parent.output_dim, attn_input_shape[-1]))
            return []
        elif self.score_fn == 'concat':
            parent.U_att = parent.inner_init((parent.output_dim, parent.output_dim),
                                             name='{}_U_att'.format(parent.name))
            parent.W_att = parent.attn_init((attn_input_shape[-1], parent.output_dim),
                                            name='{}_W_att'.format(parent.name))
            parent.v_att = parent.init((parent.output_dim, 1),
                                       name='{}_v_att'.format(parent.name))
            parent.b_att = K.zeros((parent.output_dim,), name='{}_b_att'.format(parent.name))
            weights = [parent.U_att, parent.W_att, parent.v_att, parent.b_att]
            if self.position_bias:
                parent.W_posbias = parent.attn_init((3, parent.output_dim),
                                                    name='{}_W_posbias'.format(parent.name))
                weights += [parent.W_posbias]
            return weights
        else:
            raise NotImplementedError("Unknown score_fn: {}".format(self.score_fn))

    def get_initial_states(self, x_input, x_attn, mask_attn):
        if not self.position_bias:
            return []
        initial_state = K.zeros_like(x_attn)  # (samples, timesteps, input_dim)
        initial_state = K.sum(initial_state, axis=(1, 2))  # (samples,)
        initial_state = K.expand_dims(initial_state)  # (samples, 1)
        # Without the following line, Theano makes the variable broadcastable as
        # "fcol" since the second dimension is 1, which causes cryptic errors in
        # the step function due to the updated state being (non-broadcastable)
        # "fmatrix".
        initial_state = K.pattern_broadcast(initial_state, (False, False))
        return [initial_state]

    def align(self, h_tm1, x_attn, attn_shape, mask_attn=None, states=None):
        parent = self.parent
        new_states = []
        #### attentional component
        # alignment model
        if self.score_fn == "dot":
            energy = K.batch_dot(x_attn, h_tm1, axes=(2, 1))
        elif self.score_fn == "concat":
            # -- keeping weight matrices for x_attn and h_s separate has the advantage
            # that the feature dimensions of the vectors can be different
            h_att = K.repeat(h_tm1, attn_shape[1])
            att = time_distributed_dense(x_attn, parent.W_att, parent.b_att)
            energy = K.dot(h_att, parent.U_att) + att

            if self.position_bias:
                p_tm1 = states[0]
                pos_j = K.repeat(p_tm1, attn_shape[1])
                pos_i = K.zeros_like(pos_j) + K.expand_dims(K.arange(0, attn_shape[1], dtype=K.floatx()))
                if mask_attn is None:
                    pos_I = K.ones_like(pos_j) * attn_shape[1]
                else:
                    pos_I = K.cast(K.sum(mask_attn, axis=1, keepdims=True), K.floatx())
                    pos_I = K.repeat(pos_I, attn_shape[1])
                v_posbias = K.concatenate([
                    K.log(1.0 + pos_i),
                    K.log(1.0 + pos_j),
                    K.log(1.0 + pos_I)])
                energy = energy + K.dot(v_posbias, parent.W_posbias)
                new_states = [p_tm1 + K.ones_like(p_tm1)]

            energy = parent.attn_activation(energy)
            energy = K.squeeze(K.dot(energy, parent.v_att), 2)

        # make probability tensor
        alpha = K.exp(energy)
        if mask_attn is not None:
            alpha *= mask_attn
        alpha /= K.sum(alpha, axis=1, keepdims=True)
        return alpha, new_states

    def get_config(self):
        config = super().get_config()
        config['kwargs']['score_fn'] = self.score_fn
        config['kwargs']['position_bias'] = self.position_bias
        return config


class PredictiveLocalAlignment(GlobalAlignment):
    """Predictive local alignment model after Luong et al. (2015), Sec. 3.2.

    Calculates alpha just as in the global alignment, but additionally predicts
    a position to focus on, then applies a Gaussian distribution around that
    predicted position to encourage the model to focus on it.

    References:
        * Luong, Pham & Manning (2015), "Effective Approaches to Attention-based
          Neural Machine Translation",
          <https://nlp.stanford.edu/pubs/emnlp15_attn.pdf>
    """
    def __init__(self, parent, *args,
                 D=10, scale=False, use_global=True, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.D = D
        self.variance = 2 * ((D / 2) ** 2)
        self.use_scaling = scale
        self.use_global = use_global

    def get_config(self):
        config = super().get_config()
        config['kwargs']['D'] = self.D
        config['kwargs']['scale'] = self.use_scaling
        config['kwargs']['use_global'] = self.use_global
        return config

    def build(self, input_shape, attn_input_shape):
        parent = self.parent
        weights = []
        if self.use_global:
            weights += super().build(input_shape, attn_input_shape)
        parent.W_p = parent.init((parent.output_dim, parent.output_dim),
                                 name='{}_W_p'.format(parent.name))
        parent.v_p = parent.init((parent.output_dim, 1),
                                 name='{}_v_p'.format(parent.name))
        parent.b_p = K.zeros((parent.output_dim,), name='{}_b_p'.format(parent.name))
        weights += [parent.W_p, parent.v_p, parent.b_p]
        if self.use_scaling:
            parent.v_lambda = parent.init((parent.output_dim, 1),
                                          name='{}_v_lambda'.format(parent.name))
            weights += [parent.v_lambda]
        return weights

    def align(self, h_tm1, x_attn, attn_shape, mask_attn=None, states=None):
        parent = self.parent

        # add locality constraint
        p_inner = K.tanh(K.dot(h_tm1, parent.W_p) + parent.b_p)
        if mask_attn is not None:
            p_t = K.sigmoid(K.dot(p_inner, parent.v_p)) * \
                K.cast(K.sum(mask_attn, axis=1, keepdims=True) - 1, K.floatx())
        else:
            p_t = K.sigmoid(K.dot(p_inner, parent.v_p)) * attn_shape[1]
        p_t = K.squeeze(K.repeat(p_t, attn_shape[1]), -1)
        s_t = K.arange(0, attn_shape[1], dtype=K.floatx())

        a_local = K.exp(-K.square(s_t - p_t) / self.variance)
        if self.use_scaling:
            lambda_t = K.exp(K.dot(p_inner, parent.v_lambda))
            a_local *= lambda_t

        if self.use_global:
            alpha, _ = super().align(h_tm1, x_attn, attn_shape, mask_attn=mask_attn)
            a_local = alpha * a_local

        # re-normalize
        if mask_attn is not None:
            a_local *= mask_attn
        a_local /= K.sum(a_local, axis=1, keepdims=True)

        return a_local, []

class DiagonalAlignment(GlobalAlignment):
    """Diagonal, a.k.a. monotonic local alignment after Luong et al. (2015)

    Calculates alpha just as in the global alignment, but applies a Gaussian
    distribution around the diagonal of the alignment matrix to encourage the
    model to focus on the same timestep of the input as in the output.

    References:
        * Luong, Pham & Manning (2015), "Effective Approaches to Attention-based
          Neural Machine Translation",
          <https://nlp.stanford.edu/pubs/emnlp15_attn.pdf>
    """
    def __init__(self, parent, *args,
                 D=10, use_global=True, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.D = D
        self.variance = 2 * ((D / 2) ** 2)
        self.use_global = use_global
        self.states = 1

    def get_initial_states(self, x_input, x_attn, mask_attn):
        initial_state = K.zeros_like(x_attn)  # (samples, timesteps, input_dim)
        initial_state = K.sum(initial_state, axis=(1, 2))  # (samples,)
        initial_state = K.expand_dims(initial_state)  # (samples, 1)
        # Without the following line, Theano makes the variable broadcastable as
        # "fcol" since the second dimension is 1, which causes cryptic errors in
        # the step function due to the updated state being (non-broadcastable)
        # "fmatrix".
        initial_state = K.pattern_broadcast(initial_state, (False, False))
        return [initial_state]

    def get_config(self):
        config = super().get_config()
        config['kwargs']['D'] = self.D
        config['kwargs']['use_global'] = self.use_global
        return config

    def build(self, input_shape, attn_input_shape):
        parent = self.parent
        weights = []
        if self.use_global:
            weights += super().build(input_shape, attn_input_shape)
        return weights

    def align(self, h_tm1, x_attn, attn_shape, mask_attn=None, states=None):
        parent = self.parent

        # add locality constraint
        p_tm1 = states[0]
        P_t = K.squeeze(K.repeat(p_tm1, attn_shape[1]), -1)
        s_t = K.arange(0, attn_shape[1], dtype=K.floatx())
        a_local = K.exp(-K.square(s_t - P_t) / self.variance)

        if self.use_global:
            alpha, _ = GlobalAlignment.align(self, h_tm1, x_attn, attn_shape, mask_attn=mask_attn)
            a_local = alpha * a_local

        # re-normalize
        if mask_attn is not None:
            a_local *= mask_attn
        a_local /= K.sum(a_local, axis=1, keepdims=True)

        p_t = p_tm1 + K.ones_like(p_tm1)
        return a_local, [p_t]


class MonotonousLocalAlignment(PredictiveLocalAlignment):
    """Monotonous local alignment after Tjandra et al. (2017)

    Like predictive local alignment, but instead of predicting an absolute
    position, predicts a (positive) delta of the new position, thereby
    guaranteeing monotonicity.

    References:
        * Tjandra, Sakti & Nakamura (2017), "Local Monotonic Attention
          Mechanism for End-to-End Speech Recognition",
          <https://arxiv.org/pdf/1705.08091.pdf>
    """
    def __init__(self, parent, *args, max_step=None, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.states = 1
        self.max_step = max_step

    def get_config(self):
        config = super().get_config()
        config['kwargs']['max_step'] = self.max_step
        return config

    def get_initial_states(self, x_input, x_attn, mask_attn):
        initial_state = K.zeros_like(x_attn)  # (samples, timesteps, input_dim)
        initial_state = K.sum(initial_state, axis=(1, 2))  # (samples,)
        initial_state = K.expand_dims(initial_state)  # (samples, 1)
        # Without the following line, Theano makes the variable broadcastable as
        # "fcol" since the second dimension is 1, which causes cryptic errors in
        # the step function due to the updated state being (non-broadcastable)
        # "fmatrix".
        initial_state = K.pattern_broadcast(initial_state, (False, False))
        return [initial_state]

    def align(self, h_tm1, x_attn, attn_shape, mask_attn=None, states=None):
        parent = self.parent

        # add locality constraint
        p_tm1 = states[0]
        p_inner = K.tanh(K.dot(h_tm1, parent.W_p) + parent.b_p)
        if self.max_step is not None:
            delta_p = K.sigmoid(K.dot(p_inner, parent.v_p)) * self.max_step
        elif mask_attn is not None:
            delta_p = K.sigmoid(K.dot(p_inner, parent.v_p)) * \
                (K.cast(K.sum(mask_attn, axis=1, keepdims=True) - 1, K.floatx()) \
                 - p_tm1)
        else:
            delta_p = K.exp(K.dot(p_inner, parent.v_p))
        p_t = p_tm1 + delta_p
        P_t = K.squeeze(K.repeat(p_t, attn_shape[1]), -1)
        s_t = K.arange(0, attn_shape[1], dtype=K.floatx())

        a_local = K.exp(-K.square(s_t - P_t) / self.variance)
        if self.use_scaling:
            lambda_t = K.exp(K.dot(p_inner, parent.v_lambda))
            a_local = a_local * lambda_t

        if self.use_global:
            alpha, _ = GlobalAlignment.align(self, h_tm1, x_attn, attn_shape, mask_attn=mask_attn)
            a_local = alpha * a_local

        # re-normalize
        if mask_attn is not None:
            a_local *= mask_attn
        a_local /= K.sum(a_local, axis=1, keepdims=True)

        return a_local, [p_t]


def from_config(config, parent):
    Aligner = get(config['name'])
    return Aligner(parent, **config['kwargs'])

def get(class_):
    if isinstance(class_, str):
        class_ = __ALIGNERS.get(class_)
        if class_ is not None:
            return class_
    elif isinstance(class_, dict):
        return from_config(class_, None)
    elif isinstance(class_, AlignmentModel):
        return class_
    raise Exception("Invalid or unknown aligner: {}".format(str(class_)))

__ALIGNERS = {
    'global': GlobalAlignment,
    'local': PredictiveLocalAlignment,
    'mono': MonotonousLocalAlignment,
    'diag': DiagonalAlignment,
    'GlobalAlignment': GlobalAlignment,
    'PredictiveLocalAlignment': PredictiveLocalAlignment,
    'MonotonousLocalAlignment': MonotonousLocalAlignment,
    'DiagonalAlignment': DiagonalAlignment
    }
